<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("О регионе");
?>
<?php
$APPLICATION->IncludeComponent("fbit:header.hero", "", [
    "BREADCRUMBS" => "Y",
    "TYPE" => "image",
    "SIZE" => "",
    "IMAGE" => [
        "JPEG" => "/img/__content/bg-hero.jpg",
        "WEBP" => "/img/__content/bg-hero.webp",
    ]
]);
?>

<?php $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "about-page",
    [
        "INCLUDE_SUBSECTIONS" => "Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "SET_TITLE" => "N",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "ID",
        "SORT_ORDER2" => "DESC",
        "IBLOCK_ID" => \FBit\Conf::ID_IBLOCK_MAIN_ABOUT,
        "PARENT_SECTION" => 0,
        "NEWS_COUNT" => 10,
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "86400",
        "CACHE_GROUPS" => "N",
        "CACHE_FILTER" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "FILTER_NAME" => '',
        "PROPERTY_CODE" => [
            "MORE_PHOTO",
            "TEXT_UNDER_GALLERY",
            "TITLE_UNDER_GALLERY",
        ],
        "FIELD_CODE" => ["PREVIEW_PICTURE", "DETAIL_PICTURE"],
    ]
); ?>


    <div class="container">
        <hr class="hr">
    </div>

    <section class="section">
        <div class="container mb-55">
            <h2 class="fz-20 fz-md-32 text-center text-lg-left">
                Вас может заинтересовать
            </h2>
        </div>

        <?php $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "may-be-interested-slider",
            [
                "INCLUDE_SUBSECTIONS" => "Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "SET_TITLE" => "N",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "ID",
                "SORT_ORDER2" => "DESC",
                "IBLOCK_ID" => \FBit\Conf::ID_IBLOCK_MAY_BE_INTERESTED,
                "PARENT_SECTION" => 0,
                "NEWS_COUNT" => 20,
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "86400",
                "CACHE_GROUPS" => "N",
                "CACHE_FILTER" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "FILTER_NAME" => '',
                "PROPERTY_CODE" => [
                    "URL",
                ],
                "FIELD_CODE" => ["PREVIEW_PICTURE"],
            ]
        ); ?>
    </section>

    <div class="py-10 py-md-60"></div>

<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>