<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Туроператоры и турагенства");
?><?php $APPLICATION->IncludeComponent(
    "fbit:header.hero",
    ".default",
    array(
        "BREADCRUMBS" => "Y",
        "CACHE_TIME" => 0,
        "CACHE_TYPE" => "N",
        "DISABLE_DIVIDER" => "Y",
        "IMAGE" => ["WEBP" => "/img/404/bg-404.webp", "JPEG" => "/img/404/bg-404.jpg",],
        "SIZE" => "hero_small",
        "TYPE" => "image"
    )
); ?>
<?php $APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "artoro",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "RESTART" => "N",
        "NO_WORD_LOGIC" => "N",
        "CHECK_DATES" => "N",
        "USE_TITLE_RANK" => "N",
        "DEFAULT_SORT" => "rank",
        "FILTER_NAME" => "",
        "arrFILTER" => array(
            0 => "main",
            1 => "iblock_activity",
            2 => "iblock_content",
        ),
        "arrFILTER_main" => array(),
        "arrFILTER_iblock_activity" => array(
            0 => "all",
        ),
        "arrFILTER_iblock_content" => array(
            0 => "5",
            1 => "10",
            2 => "12",
        ),
        "SHOW_WHERE" => "N",
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => "50",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "USE_LANGUAGE_GUESS" => "Y",
        "USE_SUGGEST" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Результаты поиска",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => ""
    ),
    false
); ?>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>