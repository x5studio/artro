<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Где купить донские сувениры");
?>

<?php
$APPLICATION->IncludeComponent("fbit:header.hero", ".default", [
    "BREADCRUMBS" => "Y",// "Y"/"N"
    "TYPE" => "image",// "image" / "video"
    "SIZE" => "hero_small",// "hero_medium" / "", "hero_small"
    "IMAGE" => [
        "WEBP" => "/img/404/bg-404.webp",
        "JPEG" => "/img/404/bg-404.jpg",
    ],
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => 0,
//    "ADD_TITLE_CLASS" => "d-none",// Так можно скрыть заголовок
    "DISABLE_DIVIDER" => "Y",//Y / N включить выключить разделитель
]);
?>

    <div class="container mt-30">
        <div class="typography">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci architecto consequatur distinctio
                earum excepturi fuga fugiat hic impedit, labore laudantium molestias nobis non pariatur, quaerat
                sapiente sequi tempora tempore velit!
            </p>
            <p>Error fugit illum in magnam non quibusdam repellat rerum? Aperiam asperiores commodi culpa dolor dolore
                explicabo facilis id ipsam modi nemo, non provident ratione suscipit, velit, vitae? Aliquid cum,
                rem?
            </p>
            <p>Debitis distinctio doloremque ipsum iste, necessitatibus quidem veritatis. Blanditiis consequatur
                consequuntur distinctio ducimus eos ex explicabo hic magnam nam nemo nobis possimus, quia quibusdam,
                repudiandae, tenetur! Commodi tempora tempore vel.
            </p>
        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>