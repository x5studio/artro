<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Где поесть");
?>
<?php $APPLICATION->IncludeComponent("fbit:catalog", "activity-without-sections", array(
    "HERO_IMG" => [
        "WEBP" => "/img/where-to-eat/bg-where-to-eat.webp",
        "JPEG" => "/img/where-to-eat/bg-where-to-eat.jpg",
    ],
    "ARTICLES_SHOW_PROPERTY" => "SHOW_ON_WHERE_TO_EAT",
    "COMPONENT_TEMPLATE" => ".default",
    "IBLOCK_TYPE" => "activity",    // Тип инфоблока
    "IBLOCK_ID" => \FBit\Conf::ID_IBLOCK_WHERE_TO_EAT,    // Инфоблок
    "TEMPLATE_THEME" => "",    // Цветовая тема
    "ADD_PICT_PROP" => "-",    // Дополнительная картинка основного товара
    "LABEL_PROP" => "",    // Свойство меток товара
    "MESS_BTN_BUY" => "Купить",    // Текст кнопки "Купить"
    "MESS_BTN_ADD_TO_BASKET" => "В корзину",    // Текст кнопки "Добавить в корзину"
    "MESS_BTN_COMPARE" => "Сравнение",    // Текст кнопки "Сравнение"
    "MESS_BTN_DETAIL" => "Подробнее",    // Текст кнопки "Подробнее"
    "MESS_NOT_AVAILABLE" => "Нет в наличии",    // Сообщение об отсутствии товара
    "MESS_BTN_SUBSCRIBE" => "Подписаться",    // Текст кнопки "Уведомить о поступлении"
    "SIDEBAR_SECTION_SHOW" => "N",    // Показывать правый блок в списке товаров
    "SIDEBAR_DETAIL_SHOW" => "N",    // Показывать правый блок на детальной странице
    "SIDEBAR_PATH" => "",    // Путь к включаемой области для вывода информации в правом блоке
    "USER_CONSENT" => "N",    // Запрашивать согласие
    "USER_CONSENT_ID" => "0",    // Соглашение
    "USER_CONSENT_IS_CHECKED" => "N",    // Галка по умолчанию проставлена
    "USER_CONSENT_IS_LOADED" => "N",    // Загружать текст сразу
    "SEF_MODE" => "Y",    // Включить поддержку ЧПУ
    "SEF_FOLDER" => "/where-to-eat/",    // Каталог ЧПУ (относительно корня сайта)
    "AJAX_MODE" => "N",    // Включить режим AJAX
    "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
    "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
    "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
    "CACHE_TYPE" => "A",    // Тип кеширования
    "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
    "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
    "CACHE_GROUPS" => "Y",    // Учитывать права доступа
    "USE_MAIN_ELEMENT_SECTION" => "N",    // Использовать основной раздел для показа элемента
    "DETAIL_STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для детального показа элемента
    "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
    "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
    "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
    "ADD_ELEMENT_CHAIN" => "N",    // Включать название элемента в цепочку навигации
    "USE_FILTER" => "Y",    // Показывать фильтр
    "FILTER_NAME" => "arFilter",    // Фильтр
    "FILTER_VIEW_MODE" => "VERTICAL",    // Вид отображения умного фильтра
    "FILTER_HIDE_ON_MOBILE" => "N",    // Скрывать умный фильтр на мобильных устройствах
    "INSTANT_RELOAD" => "N",    // Мгновенная фильтрация при включенном AJAX
    "ACTION_VARIABLE" => "action",    // Название переменной, в которой передается действие
    "PRODUCT_ID_VARIABLE" => "id",    // Название переменной, в которой передается код товара для покупки
    "USE_COMPARE" => "N",    // Разрешить сравнение товаров
    "PRICE_CODE" => "",    // Тип цены
    "USE_PRICE_COUNT" => "N",    // Использовать вывод цен с диапазонами
    "SHOW_PRICE_COUNT" => "1",    // Выводить цены для количества
    "PRICE_VAT_INCLUDE" => "N",    // Включать НДС в цену
    "PRICE_VAT_SHOW_VALUE" => "N",    // Отображать значение НДС
    "BASKET_URL" => "/personal/basket.php",    // URL, ведущий на страницу с корзиной покупателя
    "USE_PRODUCT_QUANTITY" => "N",    // Разрешить указание количества товара
    "PRODUCT_QUANTITY_VARIABLE" => "quantity",    // Название переменной, в которой передается количество товара
    "ADD_PROPERTIES_TO_BASKET" => "N",    // Добавлять в корзину свойства товаров и предложений
    "PRODUCT_PROPS_VARIABLE" => "prop",    // Название переменной, в которой передаются характеристики товара
    "PARTIAL_PRODUCT_PROPERTIES" => "N",    // Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
    "SEARCH_PAGE_RESULT_COUNT" => "50",    // Количество результатов на странице
    "SEARCH_RESTART" => "N",    // Искать без учета морфологии (при отсутствии результата поиска)
    "SEARCH_NO_WORD_LOGIC" => "Y",    // Отключить обработку слов как логических операторов
    "SEARCH_USE_LANGUAGE_GUESS" => "Y",    // Включить автоопределение раскладки клавиатуры
    "SEARCH_CHECK_DATES" => "Y",    // Искать только в активных по дате документах
    "SEARCH_USE_SEARCH_RESULT_ORDER" => "N",    // Использовать сортировку результатов по релевантности
    "SHOW_TOP_ELEMENTS" => "Y",    // Выводить топ элементов
    "TOP_ELEMENT_COUNT" => "9",    // Количество выводимых элементов
    "TOP_LINE_ELEMENT_COUNT" => "3",    // Количество элементов, выводимых в одной строке таблицы
    "TOP_ELEMENT_SORT_FIELD" => "sort",    // По какому полю сортируем товары в разделе
    "TOP_ELEMENT_SORT_ORDER" => "asc",    // Порядок сортировки товаров в разделе
    "TOP_ELEMENT_SORT_FIELD2" => "id",    // Поле для второй сортировки товаров в разделе
    "TOP_ELEMENT_SORT_ORDER2" => "desc",    // Порядок второй сортировки товаров в разделе
    "TOP_VIEW_MODE" => "SECTION",    // Показ элементов top'а
    "TOP_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",    // Порядок отображения блоков товара
    "TOP_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",    // Вариант отображения товаров
    "TOP_ENLARGE_PRODUCT" => "STRICT",    // Выделять товары в списке
    "TOP_SHOW_SLIDER" => "N",    // Показывать слайдер для товаров
    "SECTION_COUNT_ELEMENTS" => "Y",    // Показывать количество элементов в разделе
    "SECTION_TOP_DEPTH" => "2",    // Максимальная отображаемая глубина разделов
    "SECTIONS_VIEW_MODE" => "LIST",    // Вид списка подразделов
    "SECTIONS_SHOW_PARENT_NAME" => "Y",    // Показывать название раздела
    "PAGE_ELEMENT_COUNT" => "9",    // Количество элементов на странице
    "LINE_ELEMENT_COUNT" => "3",    // Количество элементов, выводимых в одной строке таблицы
    "ELEMENT_SORT_FIELD" => "sort",    // По какому полю сортируем товары в разделе
    "ELEMENT_SORT_ORDER" => "asc",    // Порядок сортировки товаров в разделе
    "ELEMENT_SORT_FIELD2" => "id",    // Поле для второй сортировки товаров в разделе
    "ELEMENT_SORT_ORDER2" => "desc",    // Порядок второй сортировки товаров в разделе
    "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
    "LIST_META_KEYWORDS" => "-",    // Установить ключевые слова страницы из свойства раздела
    "LIST_META_DESCRIPTION" => "-",    // Установить описание страницы из свойства раздела
    "LIST_BROWSER_TITLE" => "-",    // Установить заголовок окна браузера из свойства раздела
    "SECTION_BACKGROUND_IMAGE" => "-",    // Установить фоновую картинку для шаблона из свойства
    "LIST_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",    // Порядок отображения блоков товара
    "LIST_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",    // Вариант отображения товаров
    "LIST_ENLARGE_PRODUCT" => "STRICT",    // Выделять товары в списке
    "LIST_SHOW_SLIDER" => "Y",    // Показывать слайдер для товаров
    "LIST_SLIDER_INTERVAL" => "3000",    // Интервал смены слайдов, мс
    "LIST_SLIDER_PROGRESS" => "N",    // Показывать полосу прогресса
    "DETAIL_META_KEYWORDS" => "-",    // Установить ключевые слова страницы из свойства
    "DETAIL_META_DESCRIPTION" => "-",    // Установить описание страницы из свойства
    "DETAIL_BROWSER_TITLE" => "-",    // Установить заголовок окна браузера из свойства
    "DETAIL_SET_CANONICAL_URL" => "N",    // Устанавливать канонический URL
    "SECTION_ID_VARIABLE" => "SECTION_ID",    // Название переменной, в которой передается код группы
    "DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",    // Использовать код группы из переменной, если не задан раздел элемента
    "DETAIL_BACKGROUND_IMAGE" => "-",    // Установить фоновую картинку для шаблона из свойства
    "SHOW_DEACTIVATED" => "N",    // Показывать деактивированные товары
    "SHOW_SKU_DESCRIPTION" => "N",    // Отображать описание для каждого торгового предложения
    "DETAIL_USE_VOTE_RATING" => "N",    // Включить рейтинг товара
    "DETAIL_USE_COMMENTS" => "N",    // Включить отзывы о товаре
    "DETAIL_BRAND_USE" => "N",    // Использовать компонент "Бренды"
    "DETAIL_DISPLAY_NAME" => "Y",    // Выводить название элемента
    "DETAIL_IMAGE_RESOLUTION" => "16by9",    // Соотношение сторон изображения товара
    "DETAIL_PRODUCT_INFO_BLOCK_ORDER" => "sku,props",    // Порядок отображения блоков информации о товаре
    "DETAIL_PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",    // Порядок отображения блоков покупки товара
    "DETAIL_SHOW_SLIDER" => "N",    // Показывать слайдер для товаров
    "DETAIL_DETAIL_PICTURE_MODE" => array(    // Режим показа детальной картинки
        0 => "POPUP",
        1 => "MAGNIFIER",
    ),
    "DETAIL_ADD_DETAIL_TO_SLIDER" => "N",    // Добавлять детальную картинку в слайдер
    "DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",    // Показ описания для анонса на детальной странице
    "DETAIL_SHOW_POPULAR" => "Y",    // Показывать блок "Популярное в разделе"
    "DETAIL_SHOW_VIEWED" => "Y",    // Показывать блок "Просматривали"
    "LINK_IBLOCK_TYPE" => "",    // Тип инфоблока, элементы которого связаны с текущим элементом
    "LINK_IBLOCK_ID" => "",    // ID инфоблока, элементы которого связаны с текущим элементом
    "LINK_PROPERTY_SID" => "",    // Свойство, в котором хранится связь
    "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",    // URL на страницу, где будет показан список связанных элементов
    "USE_STORE" => "N",    // Показывать блок "Количество товара на складе"
    "USE_ENHANCED_ECOMMERCE" => "N",    // Включить отправку данных в электронную торговлю
    "PAGER_TEMPLATE" => "catalog-ajax",    // Шаблон постраничной навигации
    "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
    "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
    "PAGER_TITLE" => "Товары",    // Название категорий
    "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
    "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
    "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
    "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
    "LAZY_LOAD" => "N",    // Показать кнопку ленивой загрузки Lazy Load
    "MESS_BTN_LAZY_LOAD" => "Показать ещё",    // Текст кнопки "Показать ещё"
    "LOAD_ON_SCROLL" => "N",    // Подгружать товары при прокрутке до конца
    "SET_STATUS_404" => "Y",    // Устанавливать статус 404
    "SHOW_404" => "Y",    // Показ специальной страницы
    "FILE_404" => "/404.php",    // Страница для показа (по умолчанию /404.php)
    "COMPATIBLE_MODE" => "N",    // Включить режим совместимости
    "USE_ELEMENT_COUNTER" => "N",    // Использовать счетчик просмотров
    "DISABLE_INIT_JS_IN_COMPONENT" => "N",    // Не подключать js-библиотеки в компоненте
    "LIST_PROPERTY_CODE" => [
        "VIDEO",
        "CATEGORY",
    ],
    "DETAIL_PROPERTY_CODE" => [
        "RATING",
        "CATEGORY",
    ],
    "DETAIL_FIELD_CODE" => [
        "PROPERTY_VIDEO.PROPERTY_URL_YOUTUBE",
        "PROPERTY_VIDEO.PREVIEW_PICTURE",
    ],
    "SEF_URL_TEMPLATES" => array(
        "sections" => "",
        "section" => "",
        "element" => "#ELEMENT_CODE#/",
        "smart_filter" => "filter/#SMART_FILTER_PATH#/",
    )
),
    false
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>