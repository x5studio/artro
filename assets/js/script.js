
(function () {
    var ajaxPagerLoadingClass = 'ajax-pager-loading',
        ajaxPagerWrapClass = 'ajax-pager-wrap',
        ajaxPagerLinkClass = 'ajax-pager-link',
        ajaxWrapAttribute = 'wrapper-class',
        ajaxPagerLoadingTpl = ['<span class="' + ajaxPagerLoadingClass + '">',
            'Загрузка…',
            '</span>'].join(''),
        busy = false,

        ajaxPagination = function (e) {
            e.preventDefault();

            busy = true;
            var wrapperClass = $('.' + ajaxPagerLinkClass).data(ajaxWrapAttribute),
                $wrapper = $('.' + wrapperClass),
                $paginationWrapper = $('.' + wrapperClass),
                $link = $(this);

            if ($wrapper.length) {
                $('.' + ajaxPagerWrapClass).append(ajaxPagerLoadingTpl);
                $.get($link.attr('href'), {'AJAX_PAGE': 'Y'}, function (data) {
                    data = JSON.parse(data);
                    $('.' + ajaxPagerWrapClass).html(data.paginationHtml);
                    $wrapper.append(data.productsHtml);
                    busy = false;
                });
            }
        };

    $(function () {
        var $ajaxPager = $('.' + ajaxPagerLinkClass);
        if ($ajaxPager.length && $ajaxPager.data(ajaxWrapAttribute).length) {
            $(document).on('click', '.' + ajaxPagerLinkClass, ajaxPagination);
        }
    });

})();

(function ($) {
    $(function () {
        /**/
        /**/
        /*PAGE ROUTES DETAIL*/
// before document ready
        var element = document.getElementById('route-scrollspy');
        if (typeof (element) != 'undefined' && element != null) {
            document.body.setAttribute('data-spy', 'scroll');
            document.body.setAttribute('target', 'route-scrollspy');
            document.body.setAttribute('offset', '180');
        }
        /*PAGE ROUTES DETAIL*/
        /* */
        /* */
        $(document).on('change', '.js-order-control', function (event) {
            event.preventDefault();
            location.href = $(this).find("option[value='" + $(this).val() + "']").attr("data-href");
        });


        /**/
        /**/
        /*HEADER*/

        /*collapse header by scroll*/
        let pageScroll = $(window).scrollTop();
        if (pageScroll > 0) {
            $('.header').addClass('header_inverse header_collapse');
        }

        window.addEventListener('scroll', function () {
            let st = window.pageYOffset || document.documentElement.scrollTop;
            let header = $('.header');

            if (st > 0) {
                // console.log('scroll');
                header.addClass('header_inverse header_collapse');
                header.removeClass('active');
                closeAllHeaderNavs();

            } else {
                // console.log('pagetop');
                setTimeout(function () {
                    if (!header.hasClass('header_inverse-lock')) {
                        header.removeClass('header_inverse header_collapse');
                    } else {
                        header.removeClass('header_collapse');
                    }
                }, 200);
                closeAllHeaderNavs();
                $('#collapse-duplicate-menu').collapse('hide');
                $('#site-search').blur();
            }

            // show/hide scroll-top 
            if (st > 400) {
                $('.scroll-top').fadeIn();
            } else {
                $('.scroll-top').fadeOut();
            }


        });
        /*end collapse header by scroll*/


        /*collapse-secondary-menu*/
        $('#collapse-secondary-menu').on('show.bs.collapse', function () {
            closeAllHeaderNavs();
            setTimeout(function () {
                $('.header').addClass('active');
            }, 10);
        });
        $('#collapse-secondary-menu').on('hide.bs.collapse', function () {
            $('.header').removeClass('active');
        });
        /*end collapse-secondary-menu*/

        /*collapse-submenu*/
        $('div[id^="collapse-submenu_"]').on('show.bs.collapse', function () {
            setTimeout(function () {
                $('.header').addClass('active');
            }, 10);
        });
        $('div[id^="collapse-submenu_"]').on('hide.bs.collapse', function () {
            $('.header').removeClass('active');
        });

        $('div[id^="collapse-duplicate-submenu_"]').on('show.bs.collapse', function (event) {
            $('.header').addClass('active');
            event.stopPropagation();
        });
        $('div[id^="collapse-duplicate-submenu_"]').on('hide.bs.collapse', function (event) {
            event.stopPropagation();
            $('.header').removeClass('active');
        });
        /*end collapse-submenu*/


        /*collapse-duplicate-menu*/
        $('#collapse-duplicate-menu').on('show.bs.collapse', function () {
            closeAllHeaderNavs();
            $('.header').addClass('active');
        });
        $('#collapse-duplicate-menu').on('hide.bs.collapse', function () {
            $('.header').removeClass('active');
        });
        /*end collapse-duplicate-menu*/

        /*collapse-header-search*/
        $('#collapse-header-search').on('show.bs.collapse', function () {
            closeAllHeaderNavs();
            $('.breadcrumb').fadeOut();
            $('.header__top').addClass('border-0');
        });
        $('#collapse-header-search').on('hide.bs.collapse', function () {
            closeAllHeaderNavs();
            $('.breadcrumb').fadeIn();
            $('.header__top').removeClass('border-0');
        });
        /*end collapse-header-search*/

        /*site-search focus*/
        $('#site-search').focus(function () {
            closeAllHeaderNavs();
            let header = $('.header');
            let headerBlur = header.find('.header__blur');
            let searchInner = header.find('.header-search__inner');
            // if (!header.hasClass('header_collapse')) {
            header.addClass('active');
            // }
            headerBlur.addClass('header__blur_search');
            searchInner.addClass('header-search__inner_blur');
            searchInner.find('.header-search__results').slideDown();

            $('#collapse-secondary-menu').removeClass('show');

        });
        $('#site-search').focusout(function () {
            let header = $('.header');
            let headerBlur = header.find('.header__blur');
            let searchInner = header.find('.header-search__inner');
            // if (!header.hasClass('header_collapse')) {
            header.removeClass('active');
            // }
            headerBlur.removeClass('header__blur_search');
            searchInner.removeClass('header-search__inner_blur');
            searchInner.find('.header-search__results').hide();
        });

        /*end site-search focus*/


        function closeAllHeaderNavs() {
            $('#collapse-secondary-menu').collapse('hide');
            // $('#collapse-duplicate-menu').collapse('hide');
            $('div[id^="collapse-submenu_"]').collapse('hide');
            $('div[id^="collapse-duplicate-submenu_"]').collapse('hide');
            $('#collapse-lang').collapse('hide');
            // $('#site-search').blur();
        }

        /*END HEADER*/
        /**/
        /**/


        /**/
        /**/
        /*PAGE MAIN*/
        if ($('.slider-main-page').length) {
            let sliderSelector = '.slider-main-page';
            let options = {
                preloadImages: false,
                lazy: {
                    loadPrevNext: true,
                },
                init: false,
                loop: true,
                // autoplay: true,
                speed: 800,
                slidesPerView: 1, // or 'auto'
                centeredSlides: true,
                effect: 'coverflow', // 'cube', 'fade', 'coverflow',
                coverflowEffect: {
                    stretch: -112, // Stretch space between slides (in px)
                    depth: 2, // Depth offset in px (slides translate in Z axis)
                    rotate: 26, // Slide rotate in degrees
                    modifier: 1, // Effect multipler
                    slideShadows: true, // Enables slides shadows
                },
                grabCursor: true,
                parallax: true,
                pagination: {
                    el: '.main-slider-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.main-slider-next',
                    prevEl: '.main-slider-prev',
                },
                breakpoints: {
                    992: {
                        slidesPerView: 2,
                    },
                }
            };
            let swiper = new Swiper(sliderSelector, options);
            // Initialize slider
            swiper.init();
        }


        /*avia map*/
        if ($('#js-avia-map').length) {
            const mapData = '[{"id":"region_25621","city":"Астрахань","x":"106.55288696289062","y":"359.5867614746094","time":"1:20"},{"id":"region_23969","city":"Брянск","x":"98.54852294921875","y":"205.50271606445312","time":"3:20"},{"id":"region_21237","city":"Волгоград","x":"132.56707763671875","y":"267.53656005859375","time":"1:00"},{"id":"region_29961","city":"Воронеж","x":"105.5523452758789","y":"260.5327453613281","time":"1:20"},{"id":"region_25860","city":"Грозный","x":"63.5294189453125","y":"371.59332275390625","time":"1:15"},{"id":"region_27801","city":"Екатеринбург","x":"269.80859375","y":"305.395751953125","time":"2:45"},{"id":"region_22872","city":"Казань","x":"193.60037231445312","y":"275.5409240722656","time":"1:50"},{"id":"region_23389","city":"Калуга","x":"125.5632553100586","y":"209.50489807128906","time":"4:05"},{"id":"region_29739","city":"Махачкала","x":"74.5354232788086","y":"381.5987854003906","time":"1:30"},{"id":"region_24569","city":"Минеральные Воды","x":"62.528873443603516","y":"348.5807800292969","time":"1:05"},{"id":"region_24733","city":"Москва","x":"141.57199096679688","y":"212.5065460205078","time":"2:05"},{"id":"region_20318","city":"Мурманск","x":"267.6407470703125","y":"93.44159698486328","time":"5:40"},{"id":"region_20990","city":"Нижневартовск","x":"392.708984375","y":"315.562744140625","time":"5:20"},{"id":"region_22872","city":"Нижнекамск","x":"210.60964965820312","y":"290.5491027832031","time":"1:55"},{"id":"region_20812","city":"Нижний Новгород","x":"175.59054565429688","y":"244.5240020751953","time":"2:00"},{"id":"region_23163","city":"Новосибирск","x":"410.71881103515625","y":"402.6102294921875","time":"4:15"},{"id":"region_26830","city":"Норильск","x":"476.75482177734375","y":"210.5054473876953","time":"7:50"},{"id":"region_21860","city":"Омск","x":"346.6838684082031","y":"375.5954895019531","time":"3:30"},{"id":"region_20359","city":"Пермь","x":"262.6380310058594","y":"281.544189453125","time":"4:30"},{"id":"region_25315","city":"Псков","x":"125.5632553100586","y":"147.4710693359375","time":"4:20"},{"id":"region_26050","city":"Самара","x":"185.59600830078125","y":"305.55731201171875","time":"1:45"},{"id":"region_27457","city":"Санкт-Петербург","x":"160.58236694335938","y":"143.46888732910156","time":"2:40"},{"id":"region_24975","city":"Саратов","x":"146.57472229003906","y":"300.5545654296875","time":"1:25"},{"id":"region_26589","city":"Симферополь","x":"11.501038551330566","y":"270.5382080078125","time":"1:15"},{"id":"region_22582","city":"Сочи","x":"29.510862350463867","y":"324.5676574707031","time":"1:15"},{"id":"region_29277","city":"Череповец","x":"182.5943603515625","y":"187.49290466308594","time":"2:00"},{"id":"region_26575","city":"Элиста","x":"84.54087829589844","y":"333.57257080078125","time":"1:10"}]';

            function mapInit(data) {

                data = JSON.parse(data);

                let svg = d3.select('#js-avia-map');

                let plane = 'M15.4088 0.0390391L13.7157 0.809688C13.6073 0.85973 13.5072 0.928121 13.4222 1.01319L10.6632 3.77385L1.16185 2.63456C0.990037 2.61454 0.816558 2.67292 0.694789 2.79636L0.16601 3.32514C-0.114225 3.60537 -0.0274858 4.07911 0.334486 4.24091L7.14355 7.29348L5.18857 9.24846H1.77403C1.6239 9.24846 1.48045 9.30851 1.37369 9.41358L1.08845 9.70049C0.804881 9.98406 0.898293 10.4628 1.26527 10.6196L4.14603 11.854L5.3804 14.7347C5.5372 15.1017 6.0176 15.1951 6.29952 14.9116L6.58641 14.6246C6.69317 14.5179 6.75155 14.3744 6.75155 14.2243V10.8098L8.70653 8.85479L11.7591 15.6655C11.9209 16.0275 12.3946 16.1142 12.6749 15.834L13.2036 15.3052C13.3271 15.1818 13.3855 15.01 13.3654 14.8382L12.2245 5.33685L14.9851 2.57619C15.0702 2.49112 15.1386 2.3927 15.1886 2.28261L15.9593 0.589519C16.1211 0.240876 15.7608 -0.121096 15.4088 0.0390391Z';
                for (let i = 0; i < data.length; i++) {
                    let checkGroup = d3.select('#group_' + data[i].id);
                    let group = [];
                    if (checkGroup.node()) {
                        group = checkGroup;
                    } else {
                        group = svg.append('g')
                            .attr('class', 'region-group region-group-hidden')
                            .attr('id', 'group_' + data[i].id);
                    }

                    let groupInner = group.append('g')
                        .attr('class', 'region-group-inner');

                    groupInner.append('circle')
                        .attr('cx', parseInt(data[i].x))
                        .attr('cy', parseInt(data[i].y))
                        .attr('r', 3)
                        .attr('class', 'region-point');

                    groupInner.append('text')
                        .attr('x', parseInt(data[i].x) + 10)
                        .attr('y', parseInt(data[i].y) + 2)
                        .attr('class', 'region-text')
                        .text(data[i].city);

                    groupInner.append('rect')
                        .attr('x', parseInt(data[i].x) - 3)
                        .attr('y', parseInt(data[i].y) + 10)
                        .attr('width', 79)
                        .attr('height', 28)
                        .attr('rx', 5)
                        .attr('ry', 5)
                        .attr('filter', 'url(#dropshadow)')
                        .attr('class', 'region-rect');

                    let planeX = parseInt(data[i].x) + 7;
                    let planeY = parseInt(data[i].y) + 16;
                    groupInner.append('path')
                        .attr('d', plane)
                        .attr('transform', 'translate(' + planeX + ', ' + planeY + ')')
                        .attr('class', 'region-plane');

                    groupInner.append('text')
                        .attr('x', parseInt(data[i].x) + 35)
                        .attr('y', parseInt(data[i].y) + 29)
                        .attr('class', 'region-time')
                        .text(data[i].time);

                    let path = d3.select('path#' + data[i].id);
                    path.node().classList.add('region-target');
                    path.raise();
                }
            }

            mapInit(mapData);


            d3.selectAll('#js-avia-map path.region-target').on('mouseover', function () {
                let region = d3.select(this).node();
                let idNumber = region.id.replace('region_', '');
                let group = d3.select('#group_region_' + idNumber).node();
                group.classList.remove('region-group-hidden');
                region.classList.add('region-active');
            });
            d3.selectAll('#js-avia-map path.region-target').on('mouseleave', function () {
                let region = d3.select(this).node();
                let idNumber = region.id.replace('region_', '');
                let group = d3.select('#group_region_' + idNumber).node();
                group.classList.add('region-group-hidden');
                region.classList.remove('region-active');
            });
            d3.selectAll('#js-avia-map g.region-group').on('mouseover', function () {
                let group = d3.select(this).node();
                let idNumber = group.id.replace('group_region_', '');
                let region = d3.select('#region_' + idNumber).node();
                group.classList.remove('region-group-hidden');
                region.classList.add('region-active');
            });
            d3.selectAll('#js-avia-map g.region-group').on('mouseleave', function () {
                let group = d3.select(this).node();
                let idNumber = group.id.replace('group_region_', '');
                let region = d3.select('#region_' + idNumber).node();
                group.classList.add('region-group-hidden');
                region.classList.remove('region-active');
            });
            d3.selectAll('#js-avia-map g.region-group-inner').on('mouseover', function () {
                let group = d3.select(this);
                group.raise();
            });


            /*разблокируйте режим редактирования, для добавления новых данных в json*/
            const editMode = false;

            if (editMode == true) {
                d3.selectAll('#js-avia-map path').attr('class', 'region-edit');

                let map = document.getElementById('js-avia-map');

                let inputCity = document.createElement('input');
                inputCity.id = 'edit-mode-city';
                inputCity.type = 'text';
                inputCity.placeholder = 'Город';
                inputCity.className = 'mr-10';

                let inputTime = document.createElement('input');
                inputTime.id = 'edit-mode-time';
                inputTime.type = 'text';
                inputTime.value = '0:00';

                let hint = document.createElement('div');
                hint.innerHTML = 'Все просто: <br> Вводим название города и время продолжительности перелета в соответсвующие поля, затем ставим точку на карте. <br> Результат будет виден в консоли разработчика в виде json строки.';

                map.parentNode.insertBefore(hint, map.nextSibling);
                map.parentNode.insertBefore(inputTime, map.nextSibling);
                map.parentNode.insertBefore(inputCity, map.nextSibling);

                let svg = d3.select('#js-avia-map');
                svg.on('click', mapClick);

                let data = JSON.parse(mapData);

                function mapClick(event) {
                    let result = data;
                    let object = {
                        'id': '',
                        'city': '',
                        'x': '',
                        'y': '',
                        'time': ''
                    }
                    let pathId = event.path[0].id;
                    let xy = d3.pointer(event, this);
                    let inputCity = document.getElementById('edit-mode-city');
                    let inputTime = document.getElementById('edit-mode-time');

                    object.id = pathId;
                    object.city = inputCity.value;
                    object.x = xy[0].toString();
                    object.y = xy[1].toString();
                    object.time = inputTime.value;

                    let group = svg.append('g').attr('id', 'group_' + pathId);
                    group.append('circle')
                        .attr('cx', xy[0])
                        .attr('cy', xy[1])
                        .attr('r', 3)
                        .attr('class', 'region-point');

                    result.push(object);
                    result = JSON.stringify(result);

                    inputCity.value = '';
                    inputTime.value = '0:00';

                    console.log(result);
                }

            }

        }
        /*end avia map*/

        /*END PAGE MAIN*/
        /**/
        /**/


        /**/
        /**/
        /*PAGE ABOUT*/
        if ($('.slider-about-page').length) {
            let sliderSelector = '.slider-about-page';
            let menu = $('.js-about-pagination-list').text().split(',');
            let options = {
                loop: true,
                autoplay: false,
                speed: 800,
                direction: 'vertical',
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                    renderBullet: function (index, className) {
                        return '<span class="' + className + '">' + (menu[index]) + '</span>';
                    },
                },
            }
            let aboutSlider = new Swiper(sliderSelector, options);
            aboutSlider.on('slideChange', function () {
                let content = $('#about-slider-content');
                let index = parseInt(aboutSlider.realIndex) + 1;
                let currentItemClassName = '.js-about-content-item-' + index;
                content.find('.about-content-item').hide();
                $(currentItemClassName).fadeIn();
            });
        }

        if ($('.slider-about-page-second').length) {
            $('.slider-about-page-second').each(function (i) {
                i = i + 1;
                let this_ID = $(this).attr('id');
                if ($('#' + this_ID).length) {
                    let sliderSelectorSecond = '#' + this_ID;
                    let optionsSecond = {
                        preloadImages: false,
                        lazy: {
                            loadPrevNext: true,
                        },
                        loop: true,
                        autoplay: true,
                        speed: 800,
                        navigation: {
                            nextEl: '.slider-about-page-next-' + i,
                            prevEl: '.slider-about-page-prev-' + i,
                        },
                        pagination: {
                            el: '.slider-about-page-pagination-' + i,
                            clickable: true,
                        },
                    };
                    new Swiper(sliderSelectorSecond, optionsSecond);
                }

            });
        }

        if ($('.slider-interests').length) {
            let sliderSelector = '.slider-interests';
            let options = {
                preloadImages: false,
                lazy: {
                    loadPrevNext: true,
                },
                loop: true,
                autoplay: true,
                speed: 800,
                slidesPerView: 1,
                spaceBetween: 30,
                navigation: {
                    nextEl: '.slider-interests-next',
                    prevEl: '.slider-interests-prev',
                },
                pagination: {
                    el: '.slider-interests-pagination',
                    clickable: true,
                },
                breakpoints: {
                    800: {
                        slidesPerView: 2,
                    },
                    992: {
                        slidesPerView: 4,
                    },
                }
            };
            new Swiper(sliderSelector, options);
        }
        /*END PAGE ABOUT*/
        /**/
        /**/

        /**/
        /**/
        /*PAGE EVENTS*/
        if ($('#js-daterangepicker').length) {

            $('#js-daterangepicker-action').click(function () {
                $('#js-daterangepicker').data('daterangepicker').toggle();
            });

            var $minDate = $("#js-event-min-date"),
                $maxDate = $("#js-event-max-date"),
                $clearBtn = $("#js-clear-date"),
                $todayBtn = $("#js-set-date-today"),
                $tomorrowBtn = $("#js-set-date-tomorrow"),
                $weekendCheckbox = $(".js-weekend-checkbox"),
                today = moment(),
                tomorrow = moment().add(1, "days"),
                minDate, maxDate;

            if ($weekendCheckbox.prop("checked")) {
                $clearBtn.removeClass("text-red");
            }

            if ($minDate.length > 0 && $minDate.val() !== "") {
                minDate = moment($minDate.val(), "DD.MM.YYYY");
            } else {
                minDate = undefined;
            }

            if ($maxDate.length > 0 && $maxDate.val() !== "") {
                maxDate = moment($maxDate.val(), "DD.MM.YYYY");
            } else {
                maxDate = undefined
            }

            if (minDate !== undefined && maxDate !== undefined) {
                if (minDate.format('DD.MM.YYYY') === maxDate.format('DD.MM.YYYY')) {
                    if (minDate.format('DD.MM.YYYY') === today.format('DD.MM.YYYY')) {
                        $todayBtn.addClass("text-red");
                    } else if (minDate.format('DD.MM.YYYY') === tomorrow.format('DD.MM.YYYY')) {
                        $tomorrowBtn.addClass("text-red");
                    }
                }
                $clearBtn.removeClass("text-red");
            }

            $(document).on('click', '#js-set-date-today', function (event) {
                event.preventDefault();
                var today = moment();

                $minDate.val(today.format('DD.MM.YYYY'));
                $maxDate.val(today.format('DD.MM.YYYY'));

                $weekendCheckbox.prop("checked", false);
                smartFilter.keyup($maxDate[0]);
            });

            $(document).on('click', '#js-clear-date', function (event) {
                event.preventDefault();
                $minDate.val("");
                $maxDate.val("");
                $weekendCheckbox.prop("checked", false);
                smartFilter.keyup($maxDate[0]);
            });

            $(document).on('click', '#js-set-date-tomorrow', function (event) {
                event.preventDefault();
                var tomorrow = moment().add(1, "days")

                $minDate.val(tomorrow.format('DD.MM.YYYY'));
                $maxDate.val(tomorrow.format('DD.MM.YYYY'));

                $weekendCheckbox.prop("checked", false);
                smartFilter.keyup($maxDate[0]);
            });

            $('#js-daterangepicker').daterangepicker(
                {
                    linkedCalendars: false,
                    startDate: minDate,
                    endDate: maxDate,
                    applyButtonClasses: 'btn btn-sm btn-outline-red my-5',
                    cancelClass: 'btn btn-sm btn-outline-secondary my-5',
                    'locale': {
                        'format': 'DD.MM.YYYY',
                        'separator': ' - ',
                        'applyLabel': 'Применить',
                        'cancelLabel': 'Отменить',
                        'fromLabel': 'От',
                        'toLabel': 'До',
                        'customRangeLabel': 'Выбрать даты',
                        'daysOfWeek': [
                            'Вс',
                            'Пн',
                            'Вт',
                            'Ср',
                            'Чт',
                            'Пт',
                            'Сб'
                        ],
                        'monthNames': [
                            'Январь',
                            'Февраль',
                            'Март',
                            'Апрель',
                            'Май',
                            'Июнь',
                            'Июль',
                            'Август',
                            'Сентябрь',
                            'Октябрь',
                            'Декабрь',
                            'Январь'
                        ],
                        'firstDay': 1
                    }
                },
                function (start, end) {
                    $("#js-event-min-date").val(start.format('DD.MM.YYYY'));
                    $("#js-event-max-date").val(end.format('DD.MM.YYYY'));
                    $weekendCheckbox.prop("checked", false);
                    smartFilter.keyup($("#js-event-max-date")[0]);
                }
            );

        }
        /*END PAGE EVENTS*/
        /**/
        /**/


        /**/
        /**/
        /*ANY PAGE*/
        if ($('.slider-simple').length) {
            let sliderSelector = '.slider-simple';
            let options = {
                preloadImages: false,
                lazy: {
                    loadPrevNext: true,
                },
                loop: true,
                autoplay: true,
                speed: 800,
                navigation: {
                    nextEl: '.slider-simple-next',
                    prevEl: '.slider-simple-prev',
                },
                pagination: {
                    el: '.slider-simple-pagination',
                    clickable: true,
                },
            };
            new Swiper(sliderSelector, options);
        }

        /*jQuery Custom Forms (jcf)*/
        if (typeof jcf !== 'undefined') {
            jcf.setOptions('Select', {
                wrapNative: false
            });
            jcf.replaceAll();
        }
        /*end jQuery Custom Forms (jcf)*/


        /*text-gradient*/
        $('#collapse-text-gradient').on('show.bs.collapse', function () {
            $(this).parent().find('.text-gradient__backdrop').fadeOut();
        });
        $('#collapse-text-gradient').on('hide.bs.collapse', function () {
            $(this).parent().find('.text-gradient__backdrop').fadeIn();
        });
        /*end text-gradient*/


        /*scroll to something*/
        if ($('.js-scroll-to').length) {
            $('.js-scroll-to').click(function () {
                let target = $(this).data('target');
                let offset = $(this).data('offset');
                $('html, body').animate({
                    scrollTop: $(target).offset().top - offset
                }, 500);
            })
        }
        /*end scroll to something*/


        /*share button*/
        const shareButton = document.querySelector("#js-share");
        const shareResult = document.querySelector("#js-share-result");
        if (shareButton) {
            shareButton.addEventListener("click", function (event) {
                if (navigator.share) {
                    navigator
                        .share({
                            title: shareButton.getAttribute("data-title"),
                            url: window.location.href,
                        })
                        .then(function () {
                            shareResult.innerHTML = shareButton.getAttribute("data-result");
                        })
                        ["catch"](console.error);
                }
            });
        }

        /*end share button*/


        /*END ANY PAGE*/
        /**/
        /**/

    });
})(jQuery);
