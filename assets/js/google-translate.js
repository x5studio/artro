const googleTranslateConfig = {
    lang: "ru",
    langPath: "/ru/",
};

function TranslateInit() {
    let code = TranslateGetCode();

    $('[data-google-lang="' + code + '"]').addClass('hidden');
    $(".js-active-lang").html(code);
    console.log(code);

    if (code === googleTranslateConfig.lang) {
        TranslateClearCookie();
    }

    new google.translate.TranslateElement({
        pageLanguage: googleTranslateConfig.lang,
    });

    $('[data-google-lang]').click(function () {
        debugger
        let code = $(this).attr("data-google-lang");

        if (code === googleTranslateConfig.lang) {
            TranslateClearCookie();
        } else {
            TranslateSetCookie($(this).attr("data-google-lang"))
        }
        window.location.reload();
    });
}

function TranslateGetCode() {
    let lang = ($.cookie('googtrans') != undefined && $.cookie('googtrans') != "null") ? $.cookie('googtrans') : googleTranslateConfig.lang;
    return lang.substr(-2);
}

function TranslateClearCookie() {
    $.cookie('googtrans', null);
    $.cookie("googtrans", null, {
        domain: "." + document.domain,
    });
}

function TranslateSetCookie(code) {
    // Записываем куки /язык_который_переводим/язык_на_который_переводим
    $.cookie('googtrans', "/auto/" + code);
    $.cookie("googtrans", "/auto/" + code, {
        domain: "." + document.domain,
    });
}