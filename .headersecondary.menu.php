<?
$aMenuLinks = array(
    array(
        "Памятка туристу",
        "/tourist-memo/",
        array(),
        array(),
        ""
    ),
    array(
        "Визовые центры",
        "/visa-centers/",
        array(),
        array(),
        ""
    ),
    array(
        "Новости",
        "/news/",
        array(),
        array(),
        ""
    ),
    array(
        "Туроператоры и турагенства",
        "/travel-agencies/",
        array(),
        array(),
        ""
    ),
    array(
        "Виртуальные туры (3d экскурсии по РО)",
        "/virtual-tours/",
        array(),
        array(),
        ""
    ),
    array(
        "Аудиогид по РО",
        "/audio-guide/",
        array(),
        array(),
        ""
    ),
    array(
        "Где купить донские сувениры",
        "/where-to-buy-souvenirs/",
        array(),
        array(),
        ""
    ),
    array(
        "Сервисы и услуги",
        "/services/",
        array(),
        array(),
        ""
    ),
    array(
        "Как добраться до РО",
        "/how-to-get-there/",
        array(),
        array(),
        ""
    ),
    array(
        "Видео о регионе",
        "/video/",
        array(),
        array(),
        ""
    ),
);
?>