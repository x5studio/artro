<?
Bitrix\Main\Loader::registerAutoloadClasses(
    "delight.webpconverter",
    array("WebPConvertLib\WebPConvert" => "lib/webp-convert-concat-master/build/latest/webp-convert-delight.inc",
        "DelightWebpConverter" => "classes/general/DelightWebpConverter_class.php",
    )
);

$extensions = array(
    "delight_webp" => array("js" => "/bitrix/js/delight.webpconverter/delight.webp.js",)
);
foreach ($extensions as $code => $extension) {
    \CJSCore::RegisterExt($code, $extension);
};
