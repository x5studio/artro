<?php
/*ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);*/
use WebPConvertLib\WebPConvert as WebPConvertLib;
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class DelightWebpConverter{
	const MODULE_ID = "delight.webpconverter";
	const DOCUMENT_ROOT_PATH = __DIR__ . "/../../../../..";
	const FILE_EXTENSIONS = array("png", "jpg", "jpeg");
	const CONVERTED_IMAGES_FOLDER = "/upload/delight.webpconverter";													// ���� ��� ���������� ���������������� �����������
	const CONVERTERS = array('cwebp', 'vips', 'imagick', 'gmagick', 'imagemagick', 'graphicsmagick', 'wpc', 'gd');		// �������� �����������
	const TEST_IMAGE_PATH = "/bitrix/modules/delight.webpconverter/images/test.jpg";									// ���� � ��������� �����������
	const CACHE_CONVERTED_IMAGE = 604800;																				// ����� ����������� ��� ��������� �������� ������� ����������������� ����������� (604800 = ������)
	const STOP_TAGS = array("script", "style");																			// ����, ������� �� ����� ��������������. ������� ��-�� �������������� ����������� �������� ���������� �������� ��-�� ������� �� ������� �������� �� ��������� ����������.
	const IMAGES_LIMITATION_CACHE_ID = "delight_webp_images_in_conversion";												// Cache ID ��� ���� ����������� �������������� ���������������
	const IMAGES_LIMITATION_CACHE_TIME = 600;																			// ����� ����������� ��� ���� ����������� �������������� ���������������
	
	// ��� ��������� background � background-image
    public static $originUrl = false;
    public static $originCssKey = false;
	
	public static $BxCDNActive = true;																					// ���� ���������� ������������ CDN ��������
	public static $BXCloudConfig = false;																				// ������ ������ CBitrixCloudCDNConfig
	
	// ������� ��������� ��� ������ ������� ��� ������� ����������� � �������� URL
	function get_http_response_code($url) {
		$headers = get_headers($url);
		return substr($headers[0], 9, 3);
	}
	
	// ������� �������� ���� � �����
	static function GetRoot(){
		if((isset($_SERVER["DOCUMENT_ROOT"])) AND (!empty($_SERVER["DOCUMENT_ROOT"]))){
			return $_SERVER["DOCUMENT_ROOT"];
		} else {
			return static::DOCUMENT_ROOT_PATH;
		}
	}
	
	// ������� ���������� ������� URL ��� ���
	static function isInternal($image_url) {
		$components = parse_url($image_url);    
		return empty($components['host']);
	}
	
	// ������� �������� ������� URL
	static function GetCurrentUrl(){
		return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")."://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
	}
	
	// ������� ���������� URL ����������� (����������� ���������� ������ � �������������)
	static function BuildLink($link){
		$Buffer = explode(":",$_SERVER["HTTP_HOST"]);
		$HttpHostWithoutPort = $Buffer[0];
		$components = parse_url($link);
		if((!empty($components['host'])) AND (($components['host'] == $HttpHostWithoutPort) OR (stripos($components["host"], "1c-bitrix-cdn.ru") !== false))){
			$link = $components["path"];
		} elseif((!empty($components['host'])) AND (!isset($components["scheme"])) AND (strripos($link, "//") !== false) AND (strripos($link, "//") == 0)){
			// ��������� �������� � ������� ���� //example.com/image.png
			$Protocol = (CMain::IsHTTPS()) ? "https" : "http";
			$link = urldecode($Protocol.":".$link);
		}
		// ��������� ���������� ������
		if(((!empty($components['host'])) AND (($components['host'] == $HttpHostWithoutPort) OR (stripos($components["host"], "1c-bitrix-cdn.ru") !== false))) OR (empty($components['host']))){
			if(!file_exists(self::GetRoot().$link)){
				$link = urldecode($link);
				if(!file_exists(self::GetRoot().$link)){
					if(!file_exists(self::GetRoot().$link)){
						// ��������� ���������� ������
						if(!file_exists(self::GetRoot().$link)){
							$buffer_link = $GLOBALS["APPLICATION"]->GetCurDir().$link;
							if(file_exists(self::GetRoot().$buffer_link)){
								$link = $buffer_link;
							} elseif(strripos($link, " ") !== false){
								// ��������� ������ � ���������
								$link = str_replace(" ","+",$link);
							}
						}
					}
				}
			}
		}
		return $link;
	}
	
	// ������� �������� ���� � ����� (������������ � CDN)
	// ����������� ������� CFile:GetPath() � CFile::GetFileArray() ��������� ������ � AJAX-������
	static function GetFilePath($arFile){
		if(array_key_exists("~src", $arFile)){
			if($arFile["~src"]){
				$arFile["SRC"] = $arFile["~src"];
			} else {
				$arFile["SRC"] = CFile::GetFileSRC($arFile, false, false/*It is known file is local*/);
			}
		} else {
			$arFile["SRC"] = CFile::GetFileSRC($arFile, false);
		}
		return $arFile["SRC"];
	}
	
	// ������� ����������, ��������� �� �������������� �����������
	static function NeedConvertation($image_url) {
		if((\Bitrix\Main\Config\Option::get(static::MODULE_ID, "cdn_enabled") == "Y") AND (!empty(\Bitrix\Main\Config\Option::get(static::MODULE_ID, "cdn_domains")))){
			// �������� � �������������� CDN
			$cdn_domains = explode("|",str_replace(array("\r\n", "\r", "\n"),"|",\Bitrix\Main\Config\Option::get(static::MODULE_ID, "cdn_domains")));
			$components = parse_url($image_url);
			if(in_array($components["host"], $cdn_domains)){
				return true;
			}
			return false;
		} elseif((is_file(self::GetRoot().$image_url)) AND (self::isInternal($image_url)) AND (file_exists(self::GetRoot().$image_url))){
			// �������� ��� ������������� CDN
			if (function_exists("exif_imagetype")) {
				if((exif_imagetype(self::GetRoot().$image_url) == IMAGETYPE_JPEG) OR (exif_imagetype(self::GetRoot().$image_url) == IMAGETYPE_PNG)){
					return true;
				}
			} elseif(function_exists("mime_content_type")) {
				$MimeType = mime_content_type(self::GetRoot().$image_url);
				if(($MimeType == "image/png") OR ($MimeType == "image/jpeg")){
					return true;
				}
			}
		}
		return false;
	}
	
	// ������� ��������� ��������������� �����������
	static function ConvertImage($RealFilePath, $destination_path){
		if(self::NeedConvertation($RealFilePath)){
			$cache_file_id = self::BuildOriginalFileName($RealFilePath);
			if(self::$BxCDNActive){
				// ��������� ��� � �������������� ������������ CDN � ��� �������������
				$cache_file_id .= "bxcdn";
			} elseif((\Bitrix\Main\Config\Option::get(static::MODULE_ID, "cdn_enabled") == "Y") AND (!empty(\Bitrix\Main\Config\Option::get(static::MODULE_ID, "cdn_domains")))){
				// ��������� ��� � �������������� �������� CDN � ��� �������������
				$cache_file_id .= "cdn";
			}
			// ������ ������ �� ����������� �� ����
			$obCache = \Bitrix\Main\Application::getInstance()->getManagedCache();
			if($obCache->read(static::CACHE_CONVERTED_IMAGE, $cache_file_id)){
				$WebpfilePath = $obCache->get($cache_file_id);
			} else {
				if((\Bitrix\Main\Config\Option::get(static::MODULE_ID, "cdn_enabled") == "Y") AND (!empty(\Bitrix\Main\Config\Option::get(static::MODULE_ID, "cdn_domains")))){
					// ��������� � �������������� CDN
					$url_array = parse_url($RealFilePath);
					$host_str = (!empty($url_array["host"])) ? $url_array["host"] : $_SERVER["HTTP_HOST"];
					$FileArr = explode("/",$url_array["path"]);
					$FileName = $FileArr[count($FileArr)-1];
					unset($FileArr[count($FileArr)-1]);
					$FileRes = CFile::GetList(array(), array("MODULE_ID"=>static::MODULE_ID, "ORIGINAL_NAME"=>self::BuildOriginalFileName($RealFilePath).".webp"));
					if($arFile = $FileRes->GetNext()){
						$WebpfilePath = CHTTP::URN2URI(self::GetFilePath($arFile));
					} else {
						if(!empty($url_array["host"])){
							if(self::get_http_response_code($RealFilePath) != 200){
								return false;
							}
							// ��������� ��������� ���� ��� ���������
							$TmpFilePath = static::DOCUMENT_ROOT_PATH.DelightWebpConverter::CONVERTED_IMAGES_FOLDER."/tmp/".$host_str.$url_array["path"];
							mkdir(static::DOCUMENT_ROOT_PATH.DelightWebpConverter::CONVERTED_IMAGES_FOLDER."/tmp/".$host_str.implode("/",$FileArr), 0777, true);
							$image = file_get_contents($RealFilePath);
							file_put_contents($TmpFilePath, $image);
						} else {
							// ��������� ������������� ���������� �����
							$TmpFilePath = static::DOCUMENT_ROOT_PATH.$RealFilePath;
							if(!file_exists(realpath($TmpFilePath))){
								return false;
							}
						}
						// ������ �������� �����, �� �������� ����� ������ ��� �����
						$destination_path_arr = explode("/",$destination_path);
						unset($destination_path_arr[count($destination_path_arr)-1]);
						$destination_path_str = implode("/",$destination_path_arr)."/".self::BuildOriginalFileName($RealFilePath).".webp";
						$ImgLimitaion = 0;
						if((int)\Bitrix\Main\Config\Option::get(static::MODULE_ID, "images_limit", 0) > 0){
							if($obCache->read(static::IMAGES_LIMITATION_CACHE_TIME, static::IMAGES_LIMITATION_CACHE_ID)){
								$ImgLimitaion = $obCache->get(static::IMAGES_LIMITATION_CACHE_ID);
								if($ImgLimitaion >= (int)\Bitrix\Main\Config\Option::get(static::MODULE_ID, "images_limit", 0)){
									return false;
								}
							}
							$ImgLimitaion++;
							$obCache->set(static::IMAGES_LIMITATION_CACHE_ID, $ImgLimitaion);
						}
						$result_conversion = WebPConvertLib::convert(realpath($TmpFilePath), realpath(static::DOCUMENT_ROOT_PATH).$destination_path_str, array("converters"=>static::CONVERTERS));
						if((int)\Bitrix\Main\Config\Option::get(static::MODULE_ID, "images_limit", 0) > 0){
							$ImgLimitaion = $obCache->get(static::IMAGES_LIMITATION_CACHE_ID);
							$obCache->set(static::IMAGES_LIMITATION_CACHE_ID, $ImgLimitaion);
						}
						if((isset($result_conversion["success"])) AND ($result_conversion["success"] === false)){
							return false;
						}
						$arIMAGE = CFile::MakeFileArray($destination_path_str);
						$arIMAGE["del"] = "Y";
						$arIMAGE["MODULE_ID"] = "delight.webpconverter";
						$arIMAGE["description"] = $RealFilePath;
						$file_id = CFile::SaveFile($arIMAGE, static::MODULE_ID."/cdn/".$host_str.implode("/",$FileArr));
						if(!empty($url_array["host"])){
							// ������� ��������� ����, ���� �� ��� ������ � �������� �������
							unlink($TmpFilePath);
						}
						// CFile::GetByID() � CFile::GetPath() �� �������� � ������ AJAX, ������� ���������� CFile::GetList
						$FileRes = CFile::GetList(array(), array("MODULE_ID"=>static::MODULE_ID, "ORIGINAL_NAME"=>self::BuildOriginalFileName($RealFilePath).".webp"));
						if($arFile = $FileRes->GetNext()){
							$WebpfilePath = CHTTP::URN2URI(self::GetFilePath($arFile));
						} else {
							return false;
						}
					}
				} else {
					// ��������� ��� ������������� ������� CDN
					if(!file_exists(realpath(static::DOCUMENT_ROOT_PATH).$destination_path)){
						// ����������� �� ���������� ������������ �������������� ������
						$ImgLimitaion = 0;
						if((int)\Bitrix\Main\Config\Option::get(static::MODULE_ID, "images_limit", 0) > 0){
							if($obCache->read(static::IMAGES_LIMITATION_CACHE_TIME, static::IMAGES_LIMITATION_CACHE_ID)){
								$ImgLimitaion = $obCache->get(static::IMAGES_LIMITATION_CACHE_ID);
								if($ImgLimitaion >= (int)\Bitrix\Main\Config\Option::get(static::MODULE_ID, "images_limit", 0)){
									return false;
								}
							}
							$ImgLimitaion++;
							$obCache->set(static::IMAGES_LIMITATION_CACHE_ID, $ImgLimitaion);
						}
						$result_conversion = WebPConvertLib::convert(realpath(self::GetRoot().$RealFilePath), realpath(static::DOCUMENT_ROOT_PATH).$destination_path, array("converters"=>static::CONVERTERS));
						if((int)\Bitrix\Main\Config\Option::get(static::MODULE_ID, "images_limit", 0) > 0){
							$ImgLimitaion = $obCache->get(static::IMAGES_LIMITATION_CACHE_ID);
							$obCache->set(static::IMAGES_LIMITATION_CACHE_ID, $ImgLimitaion);
						}
						if((isset($result_conversion["success"])) AND ($result_conversion["success"] === false)){
							return false;
						}
					}
					$WebpfilePath = str_replace(" ","%20",CUtil::GetAdditionalFileURL($destination_path));
					$BxCDNServer = self::GetCDNServer($WebpfilePath);
					if($BxCDNServer !== false){
						$WebpfilePath = "//".$BxCDNServer.$WebpfilePath;
					}
				}
				$obCache->set($cache_file_id, $WebpfilePath);
			}
			return $WebpfilePath;
		}
		return false;
	}
	
	// ������� ���������, ������ ����� ��� ���
	static function dir_is_empty($dir) {
		$handle = opendir($dir);
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				closedir($handle);
				return false;
			}
		}
		closedir($handle);
		return true;
	}
	
	// ������� ����������� ������ � ��� ����� (������������ ��� CDN � ����)
	static function BuildOriginalFileName($RealFilePath){
		return str_replace(array(":","/","."),"_",$RealFilePath);
	}
	
	// ������� ������� ���������� ����� (�����)
	static function AgentRemoveOldFiles(){
		$obCache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$dir = realpath(static::DOCUMENT_ROOT_PATH) . DelightWebpConverter::CONVERTED_IMAGES_FOLDER;
		$files = new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS),
			RecursiveIteratorIterator::CHILD_FIRST
		);

		foreach ($files as $fileinfo) {
			if($fileinfo->isFile()){
				// ����� CDN �� ������� �� ������ �����
				if(strripos($fileinfo->getRealPath(), "/delight.webpconverter/cdn/") !== false){
					continue;
				}
				// ������� ����������������� ����, ���� �������� �����������
				$original_file = preg_replace('/(upload\/delight.webpconverter\/)(.*).webp$/', '$2', $fileinfo->getRealPath());
				if(!file_exists($original_file)){
					$obCache->clean(self::BuildOriginalFileName($original_file));
					unlink($fileinfo->getRealPath());
				}
			} elseif($dir != $fileinfo->getRealPath()) {
				// ������� �����, ���� ��� ������
				if(self::dir_is_empty($fileinfo->getRealPath())){
					rmdir($fileinfo->getRealPath());
				}
			}
		}
		
		// ������� �����, ������� ����������� ����� API ������� (CDN)
		self::RemoveAllCDNFiles();
		return "DelightWebpConverter::AgentRemoveOldFiles();";
	}
	
	// ������� ���������, ��������� �� ��� ������� ��� ������ ������
	static function CanProcess(){
		if(\Bitrix\Main\Config\Option::get(static::MODULE_ID, "enabled") != "Y"){
			return false;
		}
		// ��������� ����������� �� URL
		$LimitationUrlParamsStr = \Bitrix\Main\Config\Option::get(static::MODULE_ID, "limitation_url");
		if(!empty($LimitationUrlParamsStr)){
			$LimitationUrlParamsArr = explode("|",str_replace(array("\r\n", "\r", "\n"),"|",$LimitationUrlParamsStr));
			$CurrentUrl = self::GetCurrentUrl();
			foreach($LimitationUrlParamsArr as $LimitationUrlParam){
				if($LimitationUrlParam == "/"){
					if($GLOBALS['APPLICATION']->GetCurPage(false) == SITE_DIR){
						return false;
					}
				} else {
					if(mb_stripos($CurrentUrl, $LimitationUrlParam) !== false){
						return false;
					}
				}
			}
		}
		return true;
	}
	
	// ������� �������� ����������� ��� ���������
	public static function DelightWebpOnEndBufferContentHandler(&$content)
	{
		$start = microtime(true);
		if (isset($_GET["nowebp"]))
			return;
		if ((strpos($_SERVER["REQUEST_URI"], '/bitrix/admin/') !== 0) AND (DelightWebpConverter::CanProcess())){

			$content = preg_replace_callback("#(?<full_tag><(?<tag_name>[^/\s>]+)\s*[^>]*>)#is", function ($full_tag) {
				// ��������� ��������� ��������� �����
				foreach(static::STOP_TAGS as $stop_tag){
					if (strcasecmp($full_tag['tag_name'], $stop_tag) === 0) {
						return $full_tag['full_tag'];
					}
				}
				
				// ������������ ���������:
				//preg_match_all('/((?i:<[a-z]+\b[\s]*[^>]*?[\s]+))(src)=("|\')([^?\'\"]+\.)((?i:jpg|jpeg|png))(|\?\d+|\\?v=\\d+)("|\')/x', $content, $output_array);
				//Replacement: $1$2=$3$4$5.webp$6$7
				$img_attrs = explode("|",str_replace(array("\r\n", "\r", "\n"),"|",\Bitrix\Main\Config\Option::get(static::MODULE_ID, "attributes")));
				foreach($img_attrs as $img_attr){
					if($img_attr == "srcset"){
						$extension_regex = ".*";
					} else {
						$extension_regex = "(?i:".implode("|", static::FILE_EXTENSIONS).")";
					}
					$regex = "/
						((?i:<[a-z]+\b[\s]*[^>]*?[\s]+))
						(".trim($img_attr).")[\s]*=[\s]*									#attribute
						(\"|\')																#open_quote
						([^?'\"]+\.)(".$extension_regex.")									#href body
						(|\?\d+|\\?v=\\d+)													#params
						(\\3)																#close_quote
					/x";
					$full_tag['full_tag'] = preg_replace_callback($regex, array(
						"DelightWebpConverter",
						"filter_img",
					), $full_tag['full_tag']);
				}
				return $full_tag['full_tag'];
			}, $content);
			unset($full_tag);
			// ��������� background � background-image
			$content = preg_replace_callback("#(?<full_tag><[^>]+>)#is", function ($full_tag) {
				return preg_replace_callback("#<(?<tag>\S+)(?<before_style>[^>]*\s)(?<style_key>style\s*=\s*)(?<style_val>.+?)>#is", array('DelightWebpConverter', 'style_replace_callback'), $full_tag['full_tag']);
			}, $content);
		}
		$time = microtime(true) - $start;
		//echo "Time: ".$time."<br/>";
	}
	
	// ������� �������� ������ �� ����������� � background � background-image
	private static function style_replace_callback($match){
        /*
            C��� ��������, ��� ���� ������� style. ���������:
            $match['tag'] - ������������ ����
            $match['before_style'] - ���� �����, ������� ��� �� style
            $match['style'] - ���������� style(������� �������) + ���� �������, ������� ����� ���� � ���� ���

            ����� ������� ���� ������� ��� ���������:  return "<{$match['tag']}{$match['before_style']}{$match['style_key']}{$match['style_val']}>";
        */

        // ����� �� ��� ��� ������� $match['style'] �� ������� ����������� � ��� background ��� background-image
        $new_style_val = preg_replace_callback("#(?<css_key>background(-image)?)(?<between>\s*:\s*)url\((?<url>.+?)\)#is", function ($match2) {
            // ���������� ������������ Url, �������������� ����� �������, ���� ������� ����
            $url = str_replace(['"', "'", '\"', "&quot;", "&#039;"], '', $match2['url']);
            preg_match("#^(?<file_name>[^\.]+\.)(?<extension>[^?]+?)(?<params>.*)?$#iU", $url, $origin_file_name_match);
			if (preg_match("#\.((".implode("|", static::FILE_EXTENSIONS).")$|(".implode("\?|", static::FILE_EXTENSIONS)."\?))#i", $url)) {
				$RealFilePath = self::BuildLink($origin_file_name_match["file_name"].$origin_file_name_match["extension"]);
				$destination_path = DelightWebpConverter::CONVERTED_IMAGES_FOLDER.str_replace(array("http://","https://","//"),"/",$RealFilePath).".webp";
				$WebpfilePath = self::ConvertImage($RealFilePath, $destination_path);
				if($WebpfilePath !== false){
					self::$originUrl = $url;
					self::$originCssKey = $match2['css_key'];
					return "{$match2['css_key']}{$match2['between']}url(" . str_replace($url, $WebpfilePath, $match2['url']) . ")";
				}
            }
			return $match2[0];
        }, $match['style_val']);

        // � ������, ���� ������� $originUrl - ��������� data-background ��� data-background-image
        $add_to_tag = (self::$originUrl === FALSE ? '' : "data-bgr-webp=" . $match['style_val'][0] . self::$originUrl . $match['style_val'][0] . ' ');
        self::$originUrl = FALSE;
        return "<{$match['tag']}{$match['before_style']}{$add_to_tag}{$match['style_key']}{$new_style_val}>";
	}
	
	// ������� ����������� �������� �����������
	private static function filter_img($match)
	{
		$element = $match[1];
		$attribute = $match[2];
		$open_quote = $match[3];
		$link = $match[4];
		$extension = $match[5];
		$params = $match[6];
		$close_quote = $match[7];
		
		if($attribute == "srcset"){
			$SuccessFl = true;
			$arSrcsetLink = explode(",",$link.$extension.$params);
			
			foreach($arSrcsetLink as $key_with_size => &$LinkWithSize){
				/*
					1. �������� �������� ����� �� �������
					2. ��������� ������ �� ������ ���� [0] => /file/path/image.png, [1] => 640w
					3. �������� ������ �������� ������� � ��������������� ������
				*/
				$arLinkWithSize = array_values(array_filter(explode(" ",trim(preg_replace('/\s\s+/', ' ', $LinkWithSize))), function($value) { return !is_null($value) && $value !== ''; }));
				$RealFilePath = self::BuildLink($arLinkWithSize[0]);
				$destination_path = DelightWebpConverter::CONVERTED_IMAGES_FOLDER.str_replace(array("http://","https://","//"),"/",$RealFilePath).".webp";
				$WebpfilePath = self::ConvertImage($RealFilePath, $destination_path);
				if($WebpfilePath !== false){
					$arLinkWithSize[0] = str_replace(" ", "%20", $WebpfilePath);
					$LinkWithSize = implode(" ", $arLinkWithSize);
				} else {
					$SuccessFl = false;
					break;
				}
			}
			
			if($SuccessFl){
				$WebpSrcsetLink = implode(",",$arSrcsetLink);
				return $element.$attribute."=".$open_quote.$WebpSrcsetLink.$close_quote." data-webp-".trim($attribute)."=".$open_quote.$link.$extension.$close_quote;
			}
		} else {		
			$RealFilePath = self::BuildLink($link.$extension.$params);
			$destination_path = DelightWebpConverter::CONVERTED_IMAGES_FOLDER.str_replace(array("http://","https://","//"),"/",$RealFilePath).".webp";
			$WebpfilePath = self::ConvertImage($RealFilePath, $destination_path);
			if($WebpfilePath !== false){
				return $element.$attribute."=".$open_quote.$WebpfilePath.$close_quote." data-webp-".trim($attribute)."=".$open_quote.$link.$extension.$close_quote;
			}
		}
		return $match[0];
	}

	/*
		������� ��������� ����������� ������� ���������������
		���������� ������ ����:
		array(
			"converters" => array(
				0 => array(
					"name" => "cweb",
					"success" => false
				),
				...
				7 => array(
					"name" => "gd",
					"success" => true
				)
			),
			"success" => true
		)
	*/
	static function CheckConvertionMethods(){
		return WebPConvertLib::convert(realpath(static::DOCUMENT_ROOT_PATH . static::TEST_IMAGE_PATH), realpath(static::DOCUMENT_ROOT_PATH).static::TEST_IMAGE_PATH.".webp", array("converters"=>static::CONVERTERS));
	}
	
	// ��������� CDN
	static function CheckCDN(){
		if(self::$BXCloudConfig === false){
			if (\Bitrix\Main\Loader::includeModule('bitrixcloud')){
				// �������, ������� �� CDN ��������
				self::$BXCloudConfig = CBitrixCloudCDNConfig::getInstance()->loadFromOptions();
				if (self::$BXCloudConfig->isExpired()){
					self::$BxCDNActive = false;
				}
				$CDNActive = false;
				foreach (GetModuleEvents("main", "OnEndBufferContent", true) as $arEvent){
					if ($arEvent["TO_MODULE_ID"] === "bitrixcloud" && $arEvent["TO_CLASS"] === "CBitrixCloudCDN"){
						$CDNActive = true;
					}
				}
				if(!$CDNActive){
					self::$BxCDNActive = false;
				}
				if((!self::$BXCloudConfig->isKernelRewriteEnabled()) OR (!self::$BXCloudConfig->isContentRewriteEnabled())){
					self::$BxCDNActive = false;
				}
			} else {
				self::$BxCDNActive = false;
			}
		}
	}
	
	// ������� ���������� CDN ������. ��� ������� CDN �� �����
	static function GetCDNServer($RealFilePath){
		self::CheckCDN();
		if(self::$BxCDNActive){
			$proto = CMain::IsHTTPS() ? "https" : "http";
			foreach (self::$BXCloudConfig->getLocations() as $location){
				if ($location->getProto() === $proto){
					return $location->getServerNameByPrefixAndExtension("/upload/", "webp", $RealFilePath);
				}
			}
		}
		return false;
	}
	
	// ������� ��������� ������ � ������� ����������� � ������ ������� � ������ ������
	static function HandlingCheckConvertionMethods($check_conversion){
		if ((!function_exists("exif_imagetype")) AND (!function_exists("mime_content_type"))) {
			CAdminNotify::Add(array(          
				'MESSAGE' => Loc::getMessage('DELIGHT_WEBP_EXIF_ERROR'),
				'TAG' => 'delight_php_exif_version_error',          
				'MODULE_ID' => static::MODULE_ID,          
				'ENABLE_CLOSE' => 'Y',));
			\Bitrix\Main\Config\Option::set(static::MODULE_ID, "enabled", "N");
		}
		if(isset($check_conversion["success"])){
			if($check_conversion["success"] === true){
				foreach($check_conversion["converters"] as $converter){
					if(($converter["success"]) AND ($converter["name"] == "gd")){
						$gd_version = mb_substr(filter_var(gd_info()["GD Version"], FILTER_SANITIZE_NUMBER_INT), 0, 2);
						if($gd_version < 22){
							// ��������� ���������� ������ ���������� GD
							CAdminNotify::Add(array(          
								'MESSAGE' => Loc::getMessage('DELIGHT_WEBP_GD_LIB_VERSION_ERROR'),
								'TAG' => 'delight_php_gd_lib_version_error',          
								'MODULE_ID' => static::MODULE_ID,          
								'ENABLE_CLOSE' => 'Y',));
							\Bitrix\Main\Config\Option::set(static::MODULE_ID, "enabled", "N");
						}
					}
				}
			} else {
				// ��������� ������ ���������������
				CAdminNotify::Add(array(          
					'MESSAGE' => Loc::getMessage('DELIGHT_WEBP_CONVERTATION_ERROR'),
					'TAG' => 'delight_convertation_error',          
					'MODULE_ID' => static::MODULE_ID,          
					'ENABLE_CLOSE' => 'Y',));
				\Bitrix\Main\Config\Option::set(static::MODULE_ID, "enabled", "N");
			}
		}
	}
	
	static function RemoveAllCDNFiles(){
		$obCache = \Bitrix\Main\Application::getInstance()->getManagedCache();
		$FileRes = CFile::GetList(array(), array("MODULE_ID"=>static::MODULE_ID));
		while($arFile = $FileRes->GetNext()){
			if(!empty($arFile["DESCRIPTION"])){
				if(self::isInternal($arFile["DESCRIPTION"])){
					// ��������� ���������� �����
					if (file_exists(self::GetRoot().$arFile["DESCRIPTION"])) {
						continue;
					}
				} else {
					// ��������� ������� �����
					if (file_get_contents($arFile["DESCRIPTION"],0,null,0,1) !== false) {
						continue;
					}
				}
			}
			$obCache->clean(self::BuildOriginalFileName($arFile["DESCRIPTION"]));
			CFile::Delete($arFile["ID"]);
		}
		return true;
	}
	
	// ������� ���������� JS � ��������� ����� �����
	static function DelightWebpOnPageStartHandler(){
		if ((strpos($_SERVER["REQUEST_URI"], '/bitrix/admin/') !== 0) AND (DelightWebpConverter::CanProcess())){
			CUtil::InitJSCore(array('delight_webp'));
			$attrs = str_replace(array("\r\n", "\r", "\n"),"|",\Bitrix\Main\Config\Option::get(static::MODULE_ID, "attributes"));
			\Bitrix\Main\Page\Asset::getInstance()->addString('<meta name="delight_webpconverter_attr" content="'.$attrs.'">');
		}
	}
}