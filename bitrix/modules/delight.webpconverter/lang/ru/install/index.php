<?
$MESS["DELIGHT_WEBP_INSTALL_MODULE_NAME"] = 'Webp - Конвертер изображений в современный формат «на лету»';
$MESS["DELIGHT_WEBP_INSTALL_MODULE_DESCRIPTION"] = 'Модуль конвертирует изображения из .jpg/.png в .webp в момент загрузки страницы';
$MESS["DELIGHT_WEBP_INSTALL_PARTNER_NAME"] = 'IT Angels';
$MESS["DELIGHT_WEBP_INSTALL_NOTICE"] = 'Модуль <strong>Webp - Конвертер изображений в современный формат «на лету»</strong> успешно установлен! Теперь вам нужно активировать его в <a href="/bitrix/admin/settings.php?mid=delight.webpconverter">настройках модуля</a>';