<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Такой страницы не существует 404");
$APPLICATION->SetPageProperty("TITLE", "Такой страницы не существует 404");
$APPLICATION->SetPageProperty("PAGE_DESCRIPTION", "");
?>
<?php
$APPLICATION->IncludeComponent("fbit:header.hero", ".default", [
    "BREADCRUMBS" => "Y",
    "TYPE" => "image",
    "SIZE" => "hero_small",
    "IMAGE" => [
        "WEBP" => "/img/404/bg-404.webp",
        "JPEG" => "/img/404/bg-404.jpg",
    ],
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => 0,
    "ADD_TITLE_CLASS" => "d-none",
    "DISABLE_DIVIDER" => "Y",
]);
?>
    <div class="page-404 d-flex flex-column overflow-hidden">
        <div class="flex-grow-1">
            <div class="page-404__horse-wrap">
                <img class="page-404__horse lazyloaded" src="/img/404/horse.png" data-src="/img/404/horse.png" alt="">
            </div>
        </div>
        <div class="text-center">
            <div class="container">
                <div class="page-404__code">
                    <span class="text-title">4</span>
                    <span class="text-red">0</span>
                    <span class="text-title">4</span>
                </div>
                <div class="page-404__title">
                    Такой страницы не существует
                </div>
                <div class="page-404__text mx-xl-n20">
                    Начните поиск с
                    <a class="text-yellow text-underline" href="/">главной страницы</a>
                    или выберите <br class="d-none d-md-inline-block d-lg-none"> нужный раздел в меню
                </div>
            </div>
        </div>
    </div>
<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>