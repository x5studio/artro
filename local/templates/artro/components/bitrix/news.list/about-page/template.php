<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="about-reasons lazyload" data-bg="/img/about/bg-about.jpg">
    <div class="about-reasons__title text-center">
        <div class="container">
            <h2 class="text-white fz-20 fz-md-32 mb-60">
                Причины посетить <br class="d-inline-block d-lg-none"> Ростовскую область
            </h2>
        </div>
    </div>

    <div class="about-reasons__body pt-10 pt-lg-25">
        <div class="d-none js-about-pagination-list">
            <?= implode(",", array_column($arResult["ITEMS"], "NAME")) ?>
        </div>
        <div class="container">
            <!-- Slider main container -->
            <div class="slider-about-page">
                <div class="swiper-wrapper">
                    <?php foreach ($arResult["ITEMS"] as $arItem): ?>
                        <div class="swiper-slide overflow-auto scrollbar">
                            <?= $arItem["PREVIEW_TEXT"] ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
</div>

<div id="about-slider-content">
    <?php foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        <div class="about-content-item js-about-content-item-<?= $key + 1 ?>">
            <section class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2">
                            <div class="mx-lg-n30">
                                <div class="fz-14">
                                    <h2 class="fz-20 fz-md-32 mb-30">
                                        <?= $arItem["NAME"] ?>
                                    </h2>
                                    <div class="fz-14 text-justify typography">
                                        <?= $arItem["DETAIL_TEXT"] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php if (!empty($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"])): ?>
                <div class="section">
                    <div class="container">
                        <div class="mx-n15 mx-lg-0">
                            <div class="swiper slider-about-page-second mb-60"
                                 id="slider-about-page-second-<?= $arItem["ID"] ?>"
                            >
                                <div class="swiper-wrapper">
                                    <?php foreach ($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $key => $photoId): ?>
                                        <?php
                                        $picture = CFile::ResizeImageGet(
                                            $photoId,
                                            [
                                                'width' => 1218,
                                                'height' => 445
                                            ],
                                            BX_RESIZE_IMAGE_PROPORTIONAL,
                                            true
                                        );
                                        ?>
                                        <div class="swiper-slide swiper-lazy"
                                             data-background="<?= $picture["src"] ?>"
                                        >
                                            <div class="slider-content">
                                                <div class="ff-montserrat fz-20 fz-lg-32 fw-700 text-white">
                                                    <?= $arItem["PROPERTIES"]["MORE_PHOTO"]["DESCRIPTION"][$key] ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 d-none d-md-block"></div>
                            <div class="col-lg-6 col-md-12">
                                <div class="d-flex justify-content-between align-items-center py-5">
                                    <div class="d-none d-md-block text-left">
                                        <a href="#"
                                           class="slider-about-page-prev-1 btn btn-slider btn-lg btn-outline-red fw-400"
                                        ><span class="icon-arrow fz-20 align-text-top rotate-180"></span></a>
                                    </div>
                                    <div class="flex-grow-1 text-center">
                                        <div class="slider-about-page-pagination-1"></div>
                                    </div>
                                    <div class="d-none d-md-block text-right">
                                        <a href="#"
                                           class="slider-about-page-next-1 btn btn-slider btn-lg btn-outline-red fw-400"
                                        ><span class="icon-arrow fz-20 align-text-top"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 d-none d-md-block"></div>
                        </div>
                    </div>
                </div>
            <?php endif ?>


            <section class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2">
                            <div class="mx-lg-n30">
                                <h2 class="fz-20 fz-md-32 mb-30">
                                    <?= $arItem["PROPERTIES"]["TITLE_UNDER_GALLERY"]["VALUE"] ?>
                                </h2>
                                <div class="fz-14 text-justify typography">
                                    <?= $arItem["PROPERTIES"]["TEXT_UNDER_GALLERY"]["~VALUE"]["TEXT"] ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    <?php endforeach; ?>
</div>
