<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (isset($_GET['AJAX_PAGE'])) {
    $content = ob_get_contents();
    ob_end_clean();
    $APPLICATION->RestartBuffer();

    list(, $productsHtml) = explode('<!--RestartBufferProducts-->', $content);
    list(, $paginationHtml) = explode('<!--RestartBufferPagination-->', $APPLICATION->GetViewContent("things-to-do-pagination"));
    echo json_encode([
        "productsHtml" => $productsHtml,
        "paginationHtml" => $paginationHtml,
    ]);
    die();
}?>