<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row mb-30 js-items-wrapper">
    <!--RestartBufferProducts-->
    <?php foreach ($arResult["ITEMS"] as $arItem): ?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $picture = CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            [
                'width' => 510,
                'height' => 393
            ],
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );
        ?>
        <div class="col-md-4 col-lg-3">
            <article class="text-title mb-35 mb-md-30" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <a class="d-block" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                   aria-label="<?= htmlspecialchars($arItem["NAME"]) ?>"
                >
                    <div class="aspect-ratio aspect-ratio_77 mb-20 mb-xl-25">
                        <img class="rounded lazyload"
                             src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                             data-src="<?= $picture["src"] ?>" alt="<?= htmlspecialchars($arItem["NAME"]) ?>"
                        >
                    </div>
                    <h2 class="ff-montserrat fz-18 fz-md-16 fz-xl-20 fw-700 mb-10 text-reset text-truncate-3">
                        <?= $arItem["NAME"] ?>
                    </h2>
                    <div class="text-basic">
                        <?= $arItem["DISPLAY_ACTIVE_FROM"] ?>
                    </div>
                </a>
            </article>
        </div>
    <?php endforeach; ?>
    <!--RestartBufferProducts-->
</div>

<?php $this->SetViewTarget("pagination"); ?>
<?= $arResult['NAV_STRING'] ?>
<?php $this->EndViewTarget(); ?>

<?php $this->SetViewTarget("total-news"); ?>
<?= \FBit\Helper::numWord($arResult["NAV_RESULT"]->NavRecordCount, [
    "новость",
    "новости",
    "новостей",
]) ?>
<?php $this->EndViewTarget(); ?>
