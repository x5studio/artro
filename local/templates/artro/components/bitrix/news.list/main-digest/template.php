<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="slider-main-page mb-55">
    <div class="swiper-wrapper">
        <?php foreach ($arResult["ITEMS"] as $arItem): ?>
            <?php
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            $picture = CFile::ResizeImageGet(
                $arItem["PREVIEW_PICTURE"],
                array(
                    'width' => 1217,
                    'height' => 445
                ),
                BX_RESIZE_IMAGE_PROPORTIONAL,
                true
            );
            ?>
            <div class="swiper-slide swiper-lazy"
                 id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
                 data-background="<?= $picture["src"] ?>"
            >
                <div class="slider-content">
                    <a class="ff-montserrat fz-20 fz-xl-32 fw-700 text-white"
                       href="<?= $arItem["PROPERTIES"]["LINK"]["VALUE"] ?>"
                    >
                        <?= $arItem["NAME"] ?>
                    </a>
                </div>
            </div>

        <?php endforeach; ?>

    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-3 d-none d-md-block"></div>
        <div class="col-lg-6 col-md-12">
            <div class="d-flex justify-content-between align-items-center py-5">
                <div class="d-none d-md-block text-left">
                    <a href="#" class="main-slider-prev btn btn-slider btn-lg btn-outline-red fw-400">
                        <span class="icon-arrow fz-20 align-text-top rotate-180"></span>
                    </a>
                </div>

                <div class="flex-grow-1 text-center">
                    <div class="main-slider-pagination"></div>
                </div>

                <div class="d-none d-md-block text-right">
                    <a href="#" class="main-slider-next btn btn-slider btn-lg btn-outline-red fw-400">
                        <span class="icon-arrow fz-20 align-text-top"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 d-none d-md-block"></div>
    </div>
</div>