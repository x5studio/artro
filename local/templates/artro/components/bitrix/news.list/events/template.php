<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="events__list events-list js-items-wrapper">
    <!--RestartBufferProducts-->
    <?php foreach ($arResult["ITEMS"] as $arItem): ?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $picture = CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            [
                'width' => 510,
                'height' => 393
            ],
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );
        $date = new \Bitrix\Main\Type\DateTime($arItem["PROPERTIES"]["DATE"]["VALUE"]);
        ?>
        <article class="events-list__item py-15 py-md-35" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-xl-1 col-lg-2">
                        <div class="d-flex flex-row flex-md-column align-items-end align-items-md-stretch h-100 text-title text-md-center">
                            <div class="mb-md-5 mr-10 mr-md-0 pt-md-5 fz-14 fz-md-16">
                                <?= mb_convert_case(FormatDate("D", $date->getTimestamp()), MB_CASE_UPPER) ?>
                            </div>
                            <div class="mb-md-5 mr-10 mr-md-0 ff-montserrat fz-32 fz-md-45 fw-700 lh-1">
                                <?= $date->format("d") ?>
                            </div>
                            <div class="mb-md-20 fz-14 fz-md-22 fw-600">
                                <?= $date->format("H:i") ?>
                                <?php if ($arItem["PROPERTIES"]["PRICE"]["VALUE"]): ?>
                                    <span class="d-md-none"> / <?= $arItem["PROPERTIES"]["PRICE"]["VALUE"] ?></span>
                                <?php endif ?>
                            </div>
                            <div class="flex-md-grow-1 d-none d-md-block  fz-14 text-nowrap">
                                <?php if ($arItem["PROPERTIES"]["PRICE"]["VALUE"]): ?>
                                    <?= $arItem["PROPERTIES"]["PRICE"]["VALUE"] ?>
                                <?php endif ?>
                            </div>

                            <?php if ($arItem["PROPERTIES"]["AGE_RESTRICTIONS"]["VALUE"]): ?>
                                <div class="flex-grow-1 flex-md-grow-0 text-right text-md-center mb-n10 mb-md-0 pt-md-30">
                                    <div class="btn btn-md-lg btn-outline-secondary fz-16 fw-400 text-red bg-transparent no-hover">
                                        <span class="fz-14 fz-md-16"><?= $arItem["PROPERTIES"]["AGE_RESTRICTIONS"]["VALUE"] ?></span>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="col-md-10 col-xl-7 col-lg-6">
                        <div class="h-100 d-flex flex-column pt-15 pt-md-0">
                            <h2 class="mt-5 mb-15 ff-montserrat text-title fz-20 fz-md-22 fz-xl-32 fw-700">
                                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                    <?= $arItem["NAME"] ?>
                                </a>
                            </h2>
                            <div class="mb-25 mb-md-20 fz-14 fz-md-16">
                                <?= $arItem["PROPERTIES"]["ADDRESS"]["VALUE"] ?>
                            </div>
                            <div class="row flex-grow-1">
                                <div class="col-md-6 col-lg-12 col-xl-12 order-1 order-md-0">
                                    <div class="d-flex flex-column h-100">
                                        <div class="mb-30 mb-md-20 flex-grow-1">
                                            <?= $arItem["PREVIEW_TEXT"] ?>
                                        </div>
                                        <div class="text-title mb-10 mb-md-0">
                                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                                <span class="text-underline mr-20">Подробнее</span>
                                                <span class="icon-arrow-short align-text-top fz-20"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 d-lg-none order-0 order-md-1 mb-25 mb-md-0">
                                    <a href="#" class="d-block aspect-ratio aspect-ratio_65">
                                        <img class="rounded lazyload"
                                             src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                             data-src="<?= $picture["src"] ?>"
                                             alt="<?= htmlspecialchars($arItem["NAME"]) ?>"
                                        >
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-none d-lg-block col-lg-4 col-xl-4">
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="d-block aspect-ratio aspect-ratio_65">
                            <img class="rounded lazyload"
                                 src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                 data-src="<?= $picture["src"] ?>" alt="<?= htmlspecialchars($arItem["NAME"]) ?>"
                            >
                        </a>
                    </div>
                </div>
            </div>
        </article>

    <?php endforeach; ?>
    <!--RestartBufferProducts-->
</div>

<?php $this->SetViewTarget("pagination"); ?>
<?= $arResult['NAV_STRING'] ?>
<?php $this->EndViewTarget(); ?>



