<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$itemSizeClass = "col-md-6 col-lg-4";

if ($arParams["LINE_COUNT"] == 4) {
    $itemSizeClass = "col-md-6 col-lg-3";
}
?>
<div class="row mb-60 js-items-wrapper">
    <!--RestartBufferProducts-->
    <?php foreach ($arResult["ITEMS"] as $arItem): ?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $picture = CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            [
                'width' => 510,
                'height' => 408
            ],
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );
        ?>
        <div class="<?= $itemSizeClass ?> mb-30 text-title">
            <article class="d-flex flex-column h-100" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <a class="d-block mb-lg-20"
                   href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                   aria-label="<?= htmlspecialchars($arItem["NAME"]) ?>"
                >
                    <div class="aspect-ratio aspect-ratio_77 mb-20 mb-md-15 mb-lg-30">
                        <img class="rounded lazyload"
                             src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                             data-src="<?= $picture["src"] ?>" alt="<?= htmlspecialchars($arItem["NAME"]) ?>"
                        >
                    </div>
                    <h2 class="ff-montserrat text-reset fz-16 fz-lg-20 fw-700 text-truncate-2 mb-0">
                        <?= $arItem["NAME"] ?>
                    </h2>
                </a>
                <div class="mb-15 mb-lg-30 text-truncate-3">
                    <?= $arItem["PREVIEW_TEXT"] ?>
                </div>
                <?php if ($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]): ?>
                    <div class="mb-20 mb-lg-35 flex-grow-1">
                        <?= $arItem["PROPERTIES"]["ADDRESS"]["VALUE"] ?>
                    </div>
                <?php endif ?>

                <div>
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                       class="btn-lg-to-md d-block btn btn-outline-red fz-16 fz-lg-18 fw-700"
                    >
                        Подробнее
                    </a>
                </div>
            </article>
        </div>
    <?php endforeach; ?>
    <!--RestartBufferProducts-->
</div>

<?php $this->SetViewTarget("things-to-do-pagination"); ?>
<?= $arResult['NAV_STRING'] ?>
<?php $this->EndViewTarget(); ?>

<?php $this->SetViewTarget("total-things1"); ?>
<?= \FBit\Helper::numWord($arResult["NAV_RESULT"]->NavRecordCount, [
    "результат",
    "результата",
    "результатов",
]) ?>
<?php $this->EndViewTarget(); ?>

<?php $this->SetViewTarget("total-things2"); ?>
<?= \FBit\Helper::numWord($arResult["NAV_RESULT"]->NavRecordCount, [
    "результат",
    "результата",
    "результатов",
]) ?>
<?php $this->EndViewTarget(); ?>
