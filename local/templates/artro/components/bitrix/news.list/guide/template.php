<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$classes = [
    "routes-item_big col-md-6",
    "routes-item_big col-md-6",
    "col-md-6 col-xl-4",
    "col-md-6 col-xl-4",
    "col-md-6 col-xl-4",
];
$size1 = [
    "width" => 594,
    "height" => 326,
];

$size2 = [
    "width" => 386,
    "height" => 297,
];

$sizes = [
    $size1,
    $size1,
    $size2,
    $size2,
    $size2,
];
?>

<div class="row mb-30 js-items-wrapper">
    <!--RestartBufferProducts-->
    <?php foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $picture = CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            $sizes[$key % 5],
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );
        ?>
        <div class="routes-item <?= $classes[$key % 5] ?>">
            <article class="text-title mb-35 mb-md-60 mb-xl-60 pb-xl-5"
                     id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
            >
                <a class="d-block" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                   aria-label="<?= htmlspecialchars($arItem["NAME"]) ?>"
                >
                    <div class="routes-item__aspect-ratio aspect-ratio mb-20 mb-md-20 mb-xl-20">
                        <img class="rounded lazyload"
                             src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                             data-src="<?= $picture["src"] ?>" alt="<?= htmlspecialchars($arItem["NAME"]) ?>"
                        >
                    </div>
                    <h2 class="ff-montserrat fz-16 fz-md-18 fz-xl-20 fz-xl-20 fw-700 mb-0 text-reset text-truncate-2 text-truncate-autoheight">
                        <?= $arItem["NAME"] ?>
                    </h2>
                </a>
            </article>
        </div>
    <?php endforeach; ?>
    <!--RestartBufferProducts-->
</div>

<?php $this->SetViewTarget("pagination"); ?>
<?= $arResult['NAV_STRING'] ?>
<?php $this->EndViewTarget(); ?>
