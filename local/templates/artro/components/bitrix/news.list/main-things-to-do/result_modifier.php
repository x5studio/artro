<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var CBitrixComponent $component */

$arFilter = array(
    "IBLOCK_ID" => \FBit\Conf::ID_IBLOCK_MAIN_THINGS_TO_DO,
    "ACTIVE" => "Y"
);

$res = CIblockSection::GetList(["SORT" => "ASC"], $arFilter, false, ["ID", "NAME", "CODE"]);
$arResult["SECTIONS"] = [];
while ($arItem = $res->Fetch()) {
    $arItem["ITEMS"] = [];
    $arResult["SECTIONS"][$arItem["ID"]] = $arItem;
}

foreach ($arResult["ITEMS"] as $arItem) {
    if (isset($arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]])) {
        $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["ITEMS"][] = $arItem;
    }
}
unset($arResult["ITEMS"]);