<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$classList = [
    "mb-25 mb-md-55 text-title",
    "mb-25 mb-md-55 text-title",
    "mb-25 mb-md-55 text-title",

    "d-none d-md-block mb-55 text-title",
    "d-none d-md-block mb-55 text-title",
    "d-none d-md-block mb-55 text-title",
];

$season = new \FBit\Seasons();
$currentSeason = $season->get();
?>
<div class="tabs">
    <ul class="nav nav-tabs mb-15" id="seasons" role="tablist">
        <?php foreach ($arResult["SECTIONS"] as $arSection): ?>
            <li class="nav-item">
                <a class="nav-link <?= $currentSeason == $arSection["CODE"] ? "active" : '' ?>"
                   id="<?= $arSection["CODE"] ?>-tab"
                   data-toggle="tab"
                   href="#<?= $arSection["CODE"] ?>"
                   role="tab"
                   aria-controls="<?= $arSection["CODE"] ?>"
                   aria-selected="true"
                >
                    <?= $arSection["NAME"] ?>
                </a>
            </li>
        <?php endforeach ?>
    </ul>
    <div class="tab-content" id="seasons-content">
        <?php foreach ($arResult["SECTIONS"] as $arSection): ?>
            <?php
            $counter = 0;
            ?>
            <div class="tab-pane fade show <?= $currentSeason == $arSection["CODE"] ? "active" : '' ?>"
                 id="<?= $arSection["CODE"] ?>"
                 role="tabpanel"
                 aria-labelledby="<?= $arSection["CODE"] ?>-tab"
            >
                <div class="pt-15 pt-md-50">
                    <div class="row">
                        <?php foreach ($arSection["ITEMS"] as $arItem): ?>
                            <?php
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            $picture = CFile::ResizeImageGet(
                                $arItem["PREVIEW_PICTURE"],
                                array(
                                    'width' => 386,
                                    'height' => 297
                                ),
                                BX_RESIZE_IMAGE_PROPORTIONAL,
                                true
                            );
                            ?>

                            <div class="col-12 col-md-6 col-lg-4 <?= $classList[$counter] ?>"
                                 id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
                            >
                                <a class="d-block" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                                   aria-label="<?= htmlspecialchars($arItem["NAME"]) ?>"
                                >
                                    <div class="aspect-ratio aspect-ratio_77 mb-20 mb-md-30">
                                        <img class="rounded lazyload"
                                             src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                             data-src="<?= $picture["src"] ?>"
                                             alt="<?= htmlspecialchars($arItem["NAME"]) ?>"
                                        >
                                    </div>
                                    <div class="ff-montserrat fz-20 fw-700">
                                        <?= $arItem["NAME"] ?>
                                    </div>
                                </a>
                            </div>
                            <?php
                            $counter++;
                            if($counter > (count($classList) - 1)){
                                $counter = 0;
                            }

                            ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>