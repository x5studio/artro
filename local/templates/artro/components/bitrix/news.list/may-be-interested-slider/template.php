<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
    <div class="swiper slider-interests mb-60">
        <div class="swiper-wrapper text-title">
            <?php foreach ($arResult["ITEMS"] as $arItem): ?>
                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                $picture = CFile::ResizeImageGet(
                    $arItem["PREVIEW_PICTURE"],
                    array(
                        'width' => 282,
                        'height' => 225
                    ),
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    true
                );
                ?>
                <a class="d-block swiper-slide"
                   href="<?= $arItem["PROPERTIES"]["URL"]["VALUE"] ?>"
                   aria-label="<?= htmlspecialchars($arItem["NAME"]) ?>"
                >
                    <div class="aspect-ratio aspect-ratio_80 mb-20 mb-md-25">
                        <img class="rounded swiper-lazy"
                             src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                             data-src="<?= $picture["src"] ?>" alt="<?= htmlspecialchars($arItem["NAME"]) ?>"
                        >
                    </div>
                    <div class="ff-montserrat fz-22 fz-md-20 fw-700">
                        <?= $arItem["NAME"] ?>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3 d-none d-md-block"></div>
        <div class="col-lg-6 col-md-12">
            <div class="d-flex justify-content-between align-items-center py-5">
                <div class="d-none d-md-block text-left">
                    <a href="#" class="slider-interests-prev btn btn-slider btn-lg btn-outline-red fw-400">
                        <span class="icon-arrow fz-20 align-text-top rotate-180"></span>
                    </a>
                </div>
                <div class="flex-grow-1 text-center">
                    <div class="slider-interests-pagination"></div>
                </div>
                <div class="d-none d-md-block text-right">
                    <a href="#" class="slider-interests-next btn btn-slider btn-lg btn-outline-red fw-400">
                        <span class="icon-arrow fz-20 align-text-top"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 d-none d-md-block"></div>
    </div>
</div>