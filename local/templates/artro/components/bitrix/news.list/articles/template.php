<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row mb-30 js-items-wrapper">
    <!--RestartBufferProducts-->
    <?php foreach ($arResult["ITEMS"] as $arItem): ?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $picture = CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            [
                'width' => 510,
                'height' => 393
            ],
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );
        ?>
        <div class="col-lg-6">
            <article class="mb-20 mb-md-30" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="d-flex">
                    <div class="news-micra-preview mr-20 mr-md-30">
                        <a class="d-block w-100" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                           aria-label="<?= htmlspecialchars($arItem["NAME"]) ?>"
                        >
                            <div class="aspect-ratio aspect-ratio_80">
                                <img class="rounded lazyload"
                                     src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                     data-src="<?= $picture["src"] ?>" alt="<?= htmlspecialchars($arItem["NAME"]) ?>"
                                >
                            </div>
                        </a>
                    </div>
                    <div class="flex-1">
                        <div class="d-flex justify-content-between align-items-center mb-10 mb-md-15">
                            <div class="fz-14">
                                <?= $arItem["DISPLAY_ACTIVE_FROM"] ?>
                            </div>
                            <?php if ($arItem["SHOW_COUNTER"] > 0): ?>
                                <div>
                                    <span class="icon-eye fz-18 align-middle mr-10"></span><span><?= $arItem["SHOW_COUNTER"] ?></span>
                                </div>
                            <?php endif ?>
                        </div>
                        <h3 class="text-title text-truncate-2 ff-montserrat mb-0 fz-16 fz-md-18">
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
                        </h3>
                    </div>
                </div>
            </article>
        </div>
    <?php endforeach; ?>
    <!--RestartBufferProducts-->
</div>

<?php $this->SetViewTarget("pagination"); ?>
<?= $arResult['NAV_STRING'] ?>
<?php $this->EndViewTarget(); ?>

<?php $this->SetViewTarget("total-news"); ?>
<?= \FBit\Helper::numWord($arResult["NAV_RESULT"]->NavRecordCount, [
    "статья",
    "статьи",
    "статей",
]) ?>
<?php $this->EndViewTarget(); ?>
