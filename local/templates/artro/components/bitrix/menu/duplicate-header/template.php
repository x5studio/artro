<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
    return;
?>
<!--duplicate-header-menu-->
<ul class="duplicate-menu__list ul-clean">
    <?php foreach ($arResult["LVL1"] as $lvl1Key => $lvl1Item): ?>
        <?php if (isset($arResult["LVL2"][$lvl1Key]) && !empty($arResult["LVL2"][$lvl1Key])): ?>
            <li class="duplicate-menu__item">
                <a class="duplicate-menu__link"
                   data-toggle="collapse"
                   href="#collapse-duplicate-submenu_<?= $lvl1Key ?>"
                   role="button"
                   aria-expanded="false"
                   aria-controls="collapse-duplicate-submenu_<?= $lvl1Key ?>"
                >
                    <?= $lvl1Item["TEXT"] ?>
                    <span class="duplicate-menu__link-icon">
                        <span class="icon-chevron rotate-90 rotate-transition"></span>
                    </span>
                </a>

                <div class="duplicate-submenu collapse"
                     id="collapse-duplicate-submenu_<?= $lvl1Key ?>"
                >
                    <ul class="duplicate-submenu__list ul-clean">
                        <?php foreach ($arResult["LVL2"][$lvl1Key] as $lvl2Key => $lvl2Item): ?>
                            <li class="duplicate-submenu__item">
                                <a href="<?= $lvl2Item["LINK"] ?>" class="duplicate-submenu__link">
                                    <span class="duplicate-submenu__link-icon icon-arrow-long mr-20"></span><?= $lvl2Item["TEXT"] ?>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </li>
        <?php else: ?>
            <li class="duplicate-menu__item <?= $lvl1Item["SELECTED"] ? "active" : '' ?>">
                <a href="<?= $lvl1Item["LINK"] ?>" class="duplicate-menu__link"><?= $lvl1Item["TEXT"] ?></a>
            </li>
        <?php endif ?>
    <?php endforeach ?>
</ul>
<!--end-duplicate-header-menu-->