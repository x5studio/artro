<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var CBitrixComponent $component */

$lvl1 = [];
$lvl2 = [];
$prevLvl1 = [];

foreach ($arResult as $key => $arItem) {
    if ($arItem["DEPTH_LEVEL"] == 1) {
        $prevLvl1 = $key;
        $lvl1[$key] = $arItem;
    } else {
        $lvl2[$prevLvl1][$key] = $arItem;
    }
}

$arResult = [
    "LVL1" => $lvl1,
    "LVL2" => $lvl2,
];