<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
    return;

?>
<ul class="ul-clean fz-12 fz-md-14 ml-md-n25">
    <?php foreach ($arResult as $menuItem): ?>
        <li class="mb-10">
            <span class="icon-arrow d-inline-block align-middle mr-10"></span>
            <a href="<?= $menuItem["LINK"] ?>">
                <?= $menuItem["TEXT"] ?>
            </a>
        </li>
    <?php endforeach ?>
</ul>
