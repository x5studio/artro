<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
    return;
?>
<ul class="duplicate-menu__list ul-clean">
    <?php foreach ($arResult as $menuItem): ?>
        <li class="duplicate-menu__item">
            <a href="<?= $menuItem["LINK"] ?>" class="duplicate-menu__link fz-14">
                <span class="duplicate-menu__link-icon duplicate-menu__link-icon_before icon-arrow-long mr-20"></span><?= $menuItem["TEXT"] ?>
            </a>
        </li>
    <?php endforeach ?>
</ul>