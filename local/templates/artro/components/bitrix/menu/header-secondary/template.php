<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
    return;
$menuRows = [];
for ($i = 0; $count = count($arResult), $i < $count; $i += 10) {
    $menuRows[] = array_slice($arResult, $i, 10);
}
?>
<nav class="header__menu-secondary header-menu-secondary collapse" id="collapse-secondary-menu"
     data-parent="#header-collapse-parent"
>
    <div class="row">
        <? foreach ($menuRows as $menuRow): ?>
            <div class="col-md-6">
                <ul class="header-menu-secondary__list ul-clean">
                    <?php foreach ($menuRow as $menuItem): ?>
                        <li class="header-menu-secondary__item">
                            <a href="<?= $menuItem["LINK"] ?>" class="header-menu-secondary__link">
                                <span class="header-menu-secondary__link-icon icon-arrow-long mr-20"></span><?= $menuItem["TEXT"] ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        <? endforeach ?>
    </div>
</nav>
