<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php $APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "things-to-do",
    array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "CURRENT_SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        "SECTION_ID" => 0,
        "COUNT_ELEMENTS" => "N",
        "TOP_DEPTH" => "1",
        "SECTION_FIELDS" => [],
        "SECTION_USER_FIELDS" => ["UF_SVG"],
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
    ),
    $component
);
?>

<div class="container">
    <div class="row mb-15 mb-lg-35 align-items-center text-title">
        <div class="col-6 col-md-4 col-lg-3 pr-5 pr-md-15">
            <div class="d-none d-md-block fw-700 fz-16">Фильтр</div>
            <div class="d-block d-md-none">
                <a class="d-block btn btn-outline-secondary" data-toggle="collapse" href="#collapse-sidebar-filter"
                   role="button" aria-expanded="false" aria-controls="collapse-sidebar-filter"
                >
                    Фильтр <span class="icon-filter align-middle fz-14 ml-10"></span>
                </a>
            </div>
        </div>
        <div class="col-6 col-md-3 col-lg-3 d-none d-md-block">
            <div class="fw-700 fz-16"><?php $APPLICATION->ShowViewContent("total-things1") ?></div>
        </div>
        <div class="col-6 col-md-5 col-lg-6 pl-5 pl-md-15">
            <div class="d-flex align-items-center justify-content-end">

                <span class="d-none d-md-inline-block fw-700 fz-16 mr-10 mr-lg-15">Сортировать по:</span>
                <div class="flex-grow-1 flex-md-grow-0 text-center text-md-left">
                    <select class="form-control" name="order-by" aria-label="Сортировать по">
                        <option value="1" selected>По популярности</option>
                        <option value="2">По алфавиту</option>
                        <option value="3">По дате</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-lg-3 mb-15 mb-md-40">
            <div class="sidebar-filter d-md-block text-title collapse" id="collapse-sidebar-filter">
                <a class="sidebar-filter__close d-inline-block d-md-none" data-toggle="collapse"
                   href="#collapse-sidebar-filter" role="button" aria-expanded="true"
                   aria-controls="collapse-sidebar-filter" aria-label="Скрыть фильтр"
                >
                    <span class="icon-close"></span>
                </a>
                <div class="mb-15">
                    <div class="sidebar-filter__section filter-section">
                        <a class="filter-section__title" data-toggle="collapse" href="#filter-section-1"
                           role="button" aria-expanded="true" aria-controls="filter-section-1"
                        >
                            Время года <span class="filter-section__title-icon icon-chevron"></span>
                        </a>
                        <div class="filter-section__body collapse show" id="filter-section-1">
                            <div class="filter-section__inner overflow-auto scrollbar">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="season_1">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="season_1"
                                    >Лето</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="season_2">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="season_2"
                                    >Осень</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="season_3">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="season_3"
                                    >Зима</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="season_4">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="season_4"
                                    >Весна</label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="sidebar-filter__section filter-section">
                        <a class="filter-section__title" data-toggle="collapse" href="#filter-section-2"
                           role="button" aria-expanded="true" aria-controls="filter-section-2"
                        >
                            Город <span class="filter-section__title-icon icon-chevron"></span>
                        </a>
                        <div class="filter-section__body collapse show" id="filter-section-2">
                            <div class="filter-section__inner overflow-auto scrollbar">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="city_1">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="city_1">Ростов-на-Дону</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="city_2">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="city_2">Новочеркаск</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="city_3">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="city_3"
                                    >Таганрог</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="city_4">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="city_4"
                                    >Танаис</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="city_5">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="city_5"
                                    >Азов</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="city_6">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="city_6"
                                    >Аксай</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="city_7">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="city_7"
                                    >Батайск</label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="sidebar-filter__section filter-section">
                        <a class="filter-section__title" data-toggle="collapse" href="#filter-section-3"
                           role="button" aria-expanded="true" aria-controls="filter-section-3"
                        >
                            Категории <span class="filter-section__title-icon icon-chevron"></span>
                        </a>
                        <div class="filter-section__body collapse show" id="filter-section-3">
                            <div class="filter-section__inner overflow-auto scrollbar">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="category_1">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="category_1">Музеи</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="category_2">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="category_2">Театры</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="category_3">
                                    <label class="custom-control-label pl-15 fz-14 fz-md-16 mb-10" for="category_3">Архитектура</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-md-column">
                    <div class="flex-grow-1 mb-10">
                        <a href="#" class="d-block btn btn-lg btn-outline-red fz-18 fw-600">Показать</a>
                    </div>
                    <div class="flex-grow-1 text-title">
                        <a href="#" class="d-block btn btn-lg btn-link text-underline  fz-16">Сбросить</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-lg-9">
            <div class="d-block d-md-none fz-16 fw-600 text-title mb-20"><?php $APPLICATION->ShowViewContent("total-things2") ?></div>
            <?php $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "things-to-do",
                [
                    "PARENT_SECTION" => $arResult["VARIABLES"]["SECTION_ID"],
                    "PARENT_SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "NEWS_COUNT" => $arParams["NEWS_COUNT"],
                    "SORT_BY1" => $arParams["SORT_BY1"],
                    "SORT_ORDER1" => $arParams["SORT_ORDER1"],
                    "SORT_BY2" => $arParams["SORT_BY2"],
                    "SORT_ORDER2" => $arParams["SORT_ORDER2"],
                    "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
                    "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                    "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
                    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                    "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
                    "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                    "SET_TITLE" => $arParams["SET_TITLE"],
                    "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                    "MESSAGE_404" => $arParams["MESSAGE_404"],
                    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                    "SHOW_404" => $arParams["SHOW_404"],
                    "FILE_404" => $arParams["FILE_404"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                    "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                    "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                    "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                    "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                    "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                    "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                    "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                    "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                    "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                    "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                    "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                    "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                    "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                    "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                    "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                    "FILTER_NAME" => $arParams["FILTER_NAME"],
                    "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
                    "CHECK_DATES" => $arParams["CHECK_DATES"],
                ],
                $component
            ); ?>
        </div>
    </div>
    <?php $APPLICATION->ShowViewContent("things-to-do-pagination") ?>
</div>


<div class="container">
    <div class="mt-60 pt-30 mb-60">
        <h2 class="fz-20 fz-md-32">
            <?php $APPLICATION->IncludeFile("/include/things-to-do/seo_title.php"); ?>
        </h2>
        <div class="text-gradient mb-15">
            <div class="text-gradient__backdrop"></div>
            <?php $APPLICATION->IncludeFile("/include/things-to-do/seo_text1.php"); ?>
            <div class="collapse" id="collapse-text-gradient">
                <?php $APPLICATION->IncludeFile("/include/things-to-do/seo_text2.php"); ?>
            </div>
        </div>
        <div class="text-title">
            <a href="#collapse-text-gradient" data-toggle="collapse" role="button" aria-expanded="false"
               aria-controls="collapse-text-gradient"
            >
                <span class="text-underline fz-14">Читать полностью</span>
                <span class="icon-chevron ml-10 fz-12 align-middle"></span>
            </a>
        </div>
    </div>

    <div class="pt-xl-60">
        <h2 class="fz-20 fz-md-32 text-lg-center mb-45 mb-md-60 mt-60">Подборка статей</h2>
        <div class="row">
            <div class="col-lg-6">
                <article>
                    <div class="row">
                        <div class="col-3 pr-0 mb-30">
                            <a class="d-block" href="#"
                               aria-label="Объявлен конкурс на лучший слоган &laquo;Столицы Донского туризма&raquo;"
                            >
                                <div class="aspect-ratio aspect-ratio_80">
                                    <img class="rounded lazyload"
                                         src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                         data-src="/img/__content/what-to-do/4.jpg"
                                         alt="Объявлен конкурс на лучший слоган &laquo;Столицы Донского туризма&raquo;"
                                    >
                                </div>
                            </a>
                        </div>
                        <div class="col-9 pl-30">
                            <div class="d-flex justify-content-between align-items-center mb-15">
                                <div class="fz-14">
                                    20.08.2021
                                </div>
                                <div>
                                    <span class="icon-eye fz-18 align-middle mr-10"></span><span>1 237</span>
                                </div>
                            </div>
                            <h3 class="text-title mb-0 fz-12 fz-md-18">
                                <a href="#">Объявлен конкурс на лучший слоган &laquo;Столицы Донского
                                    туризма&raquo;
                                </a>
                            </h3>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-lg-6">
                <article>
                    <div class="row">
                        <div class="col-3 pr-0 mb-30">
                            <a class="d-block" href="#"
                               aria-label="Объявлен конкурс на лучший слоган &laquo;Столицы Донского туризма&raquo;"
                            >
                                <div class="aspect-ratio aspect-ratio_80">
                                    <img class="rounded lazyload"
                                         src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                         data-src="/img/__content/what-to-do/4.jpg"
                                         alt="Объявлен конкурс на лучший слоган &laquo;Столицы Донского туризма&raquo;"
                                    >
                                </div>
                            </a>
                        </div>
                        <div class="col-9 pl-30">
                            <div class="d-flex justify-content-between align-items-center mb-15">
                                <div class="fz-14">
                                    20.08.2021
                                </div>
                                <div>
                                    <span class="icon-eye fz-18 align-middle mr-10"></span><span>1 237</span>
                                </div>
                            </div>
                            <h3 class="text-title mb-0 fz-12 fz-md-18">
                                <a href="#">Объявлен конкурс на лучший слоган &laquo;Столицы Донского
                                    туризма&raquo;
                                </a>
                            </h3>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-lg-6">
                <article>
                    <div class="row">
                        <div class="col-3 pr-0 mb-30">
                            <a class="d-block" href="#"
                               aria-label="Ростовская область стала частью первого в России винного трэвл-гида"
                            >
                                <div class="aspect-ratio aspect-ratio_80">
                                    <img class="rounded lazyload"
                                         src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                         data-src="/img/__content/what-to-do/5.jpg"
                                         alt="Ростовская область стала частью первого в России винного трэвл-гида"
                                    >
                                </div>
                            </a>
                        </div>
                        <div class="col-9 pl-30">
                            <div class="d-flex justify-content-between align-items-center mb-15">
                                <div class="fz-14">
                                    20.08.2021
                                </div>
                                <div>
                                    <span class="icon-eye fz-18 align-middle mr-10"></span><span>1 237</span>
                                </div>
                            </div>
                            <h3 class="text-title mb-0 fz-12 fz-md-18">
                                <a href="#">Ростовская область стала частью первого в России винного трэвл-гида</a>
                            </h3>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-lg-6 d-none d-md-block">
                <article>
                    <div class="row">
                        <div class="col-3 pr-0 mb-30">
                            <a class="d-block" href="#"
                               aria-label="Ростовская область стала частью первого в России винного трэвл-гида"
                            >
                                <div class="aspect-ratio aspect-ratio_80">
                                    <img class="rounded lazyload"
                                         src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                         data-src="/img/__content/what-to-do/5.jpg"
                                         alt="Ростовская область стала частью первого в России винного трэвл-гида"
                                    >
                                </div>
                            </a>
                        </div>
                        <div class="col-9 pl-30">
                            <div class="d-flex justify-content-between align-items-center mb-15">
                                <div class="fz-14">
                                    20.08.2021
                                </div>
                                <div>
                                    <span class="icon-eye fz-18 align-middle mr-10"></span><span>1 237</span>
                                </div>
                            </div>
                            <h3 class="text-title mb-0 fz-12 fz-md-18">
                                <a href="#">Ростовская область стала частью первого в России винного трэвл-гида</a>
                            </h3>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-lg-6 d-none d-md-block">
                <article>
                    <div class="row">
                        <div class="col-3 pr-0 mb-30">
                            <a class="d-block" href="#"
                               aria-label="Объявлен конкурс на лучший слоган &laquo;Столицы Донского туризма&raquo;"
                            >
                                <div class="aspect-ratio aspect-ratio_80">
                                    <img class="rounded lazyload"
                                         src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                         data-src="/img/__content/what-to-do/4.jpg"
                                         alt="Объявлен конкурс на лучший слоган &laquo;Столицы Донского туризма&raquo;"
                                    >
                                </div>
                            </a>
                        </div>
                        <div class="col-9 pl-30">
                            <div class="d-flex justify-content-between align-items-center mb-15">
                                <div class="fz-14">
                                    20.08.2021
                                </div>
                                <div>
                                    <span class="icon-eye fz-18 align-middle mr-10"></span><span>1 237</span>
                                </div>
                            </div>
                            <h3 class="text-title mb-0 fz-12 fz-md-18">
                                <a href="#">Объявлен конкурс на лучший слоган &laquo;Столицы Донского
                                    туризма&raquo;
                                </a>
                            </h3>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-lg-6 d-none d-md-block">
                <article>
                    <div class="row">
                        <div class="col-3 pr-0 mb-30">
                            <a class="d-block" href="#"
                               aria-label="Объявлен конкурс на лучший слоган &laquo;Столицы Донского туризма&raquo;"
                            >
                                <div class="aspect-ratio aspect-ratio_80">
                                    <img class="rounded lazyload"
                                         src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                         data-src="/img/__content/what-to-do/4.jpg"
                                         alt="Объявлен конкурс на лучший слоган &laquo;Столицы Донского туризма&raquo;"
                                    >
                                </div>
                            </a>
                        </div>
                        <div class="col-9 pl-30">
                            <div class="d-flex justify-content-between align-items-center mb-15">
                                <div class="fz-14">
                                    20.08.2021
                                </div>
                                <div>
                                    <span class="icon-eye fz-18 align-middle mr-10"></span><span>1 237</span>
                                </div>
                            </div>
                            <h3 class="text-title mb-0 fz-12 fz-md-18">
                                <a href="#">Объявлен конкурс на лучший слоган &laquo;Столицы Донского
                                    туризма&raquo;
                                </a>
                            </h3>
                        </div>
                    </div>
                </article>
            </div>
        </div>

        <div class="d-block d-md-none mt-10">
            <a href="#" class="show-more">
                <div class="show-more__line"></div>
                <div class="show-more__anchor">
                    <span class="show-more__text">Показать ещё</span><span class="show-more__icon icon-chevron"
                    ></span>
                </div>
                <div class="show-more__line"></div>
            </a>
        </div>
    </div>
</div>

<div class="py-60"></div>
