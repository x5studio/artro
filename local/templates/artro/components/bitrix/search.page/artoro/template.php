<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="container mt-30">
    <div class="row">
        <div class="col-12">
            <form action="" method="get">
                <div class="input-group mb-3">
                    <input class="form-control" type="text" name="q" value="<?= $arResult["REQUEST"]["QUERY"] ?>"
                           size="40"
                    />
                    <div class="input-group-append">
                        <input class="btn btn-red" type="submit" value="<?= GetMessage("SEARCH_GO") ?>"/>
                    </div>
                </div>

                <input type="hidden" name="how" value="<? echo $arResult["REQUEST"]["HOW"] == "d" ? "d" : "r" ?>"/>
            </form>
        </div>
    </div>
    <? if (isset($arResult["REQUEST"]["ORIGINAL_QUERY"])): ?>
        <div class="search-language-guess">
            <? echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#" => '<a href="' . $arResult["ORIGINAL_QUERY_URL"] . '">' . $arResult["REQUEST"]["ORIGINAL_QUERY"] . '</a>')) ?>
        </div>
    <? endif; ?>

    <div class="row">
        <div class="col-12">
            <? if ($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false): ?>
            <? elseif ($arResult["ERROR_CODE"] != 0): ?>
            <? elseif (count($arResult["SEARCH"]) > 0): ?>
                <? if ($arParams["DISPLAY_TOP_PAGER"] != "N") echo $arResult["NAV_STRING"] ?>

                <? foreach ($arResult["SEARCH"] as $arItem): ?>
                    <div class="card mb-10">
                        <div class="card-body typography">
                            <p>
                                <a href="<? echo $arItem["URL"] ?>"><? echo $arItem["TITLE_FORMATED"] ?></a>
                            </p>
                            <p>
                                <? echo $arItem["BODY_FORMATED"] ?>
                            </p>

                            <small><?= GetMessage("SEARCH_MODIFIED") ?> <?= $arItem["DATE_CHANGE"] ?></small>

                            <? if ($arItem["CHAIN_PATH"]): ?>
                                <small><?= GetMessage("SEARCH_PATH") ?>&nbsp;<?= $arItem["CHAIN_PATH"] ?></small>
                            <? endif; ?>
                        </div>
                    </div>
                <? endforeach; ?>

                <? if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"] ?>
            <? else: ?>
                <? ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND")); ?>
            <? endif; ?>
        </div>
    </div>
</div>
<div class="py-60"></div>
<div class="py-50 d-none d-lg-block"></div>