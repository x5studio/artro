<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>
<div class="what-to-do mb-60">
    <div class="container pt-30">
        <div class="what-to-do-icons-wrap">
            <div class="what-to-do__icons what-to-do-icons">
                <?php foreach ($arResult['SECTIONS'] as $arSection): ?>
                    <?php
                    if (!is_null($arSection["UF_SVG"])) {
                        $picture = CFile::GetFileArray($arSection["UF_SVG"]);
                    } elseif ($arSection["PICTURE"]) {
                        $picture = CFile::ResizeImageGet(
                            $arSection["PICTURE"],
                            [
                                'width' => 98,
                                'height' => 74
                            ],
                            BX_RESIZE_IMAGE_PROPORTIONAL,
                            true
                        );
                        $picture = array_change_key_case($picture, CASE_UPPER);
                    } else {
                        $picture = \FBit\Helper::getNoPhotoSvg();
                    }
                    ?>
                    <a href="<?= $arSection["SECTION_PAGE_URL"] ?>"
                       class="what-to-do-order what-to-do-icons__item what-to-do-icon <?= $arParams["CURRENT_SECTION_CODE"] == $arSection["CODE"] ? "active" : "" ?>"
                    >
                        <div class="what-to-do-icon__image">
                            <img class="lazyload"
                                 src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                 data-src="<?= $picture["SRC"] ?>"
                                 alt="<?= htmlspecialchars($arSection["NAME"]) ?>"
                            >
                        </div>
                        <div class="what-to-do-icon__text"><?= $arSection["NAME"] ?></div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>