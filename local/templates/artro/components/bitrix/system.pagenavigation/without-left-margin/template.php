<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}
$arResult["NavQueryString"] = str_replace("AJAX_PAGE=Y", "", $arResult["NavQueryString"]);
$arResult["sUrlPathParams"] = str_replace("AJAX_PAGE=Y&", "", $arResult["sUrlPathParams"]);

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
$isContainerOpened = false;
?>

<div class="ajax-pager-wrap">
    <!--RestartBufferPagination-->
    <?php if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
        <div class="mb-50 mb-xl-40">
            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"
               data-wrapper-class="js-items-wrapper" class="show-more ajax-pager-link"
            >
                <div class="show-more__line"></div>
                <div class="show-more__anchor">
                    <span class="show-more__text">Показать ещё</span>
                    <span class="show-more__icon icon-chevron"></span>
                </div>
                <div class="show-more__line"></div>
            </a>
        </div>
    <?php endif ?>
    <div class="overflow-auto scrollbar pb-5">
        <nav class="pagination">
            <?php if ($arResult["NavPageNomer"] > 1): ?>
                <?php if ($arResult["bSavePage"]): ?>
                    <a class="pagination__direction"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"
                    >
                        <span class="icon-arrow rotate-180"></span>
                        <span><?= GetMessage("round_nav_back") ?></span>
                    </a>
                    <?= "<div>" ?>
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1"
                       class="pagination__item btn btn-outline-secondary"
                    >
                        1
                    </a>
                <?php else: ?>
                    <?php if ($arResult["NavPageNomer"] > 2): ?>
                        <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"
                           class="pagination__direction"
                        >
                            <span class="icon-arrow rotate-180"></span>
                            <span><?= GetMessage("round_nav_back") ?></span>
                        </a>
                    <?php else: ?>
                        <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"
                           class="pagination__direction"
                        >
                            <span class="icon-arrow rotate-180"></span>
                            <span><?= GetMessage("round_nav_back") ?></span>
                        </a>
                    <?php endif ?>
                    <?= "<div>" ?>
                    <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"
                       class="pagination__item btn btn-outline-secondary"
                    >
                        1
                    </a>
                <?php endif ?>
            <?php else: ?>
                <a href="javascript:void(0)"
                   class="pagination__direction no-hover"
                >
                    <span class="icon-arrow rotate-180"></span>
                    <span><?= GetMessage("round_nav_back") ?></span>
                </a>
                <?= "<div>" ?>
                <a href="javascript:void(0)"
                   class="pagination__item btn btn-outline-red"
                >
                    1
                </a>
            <?php endif ?>

            <?php
            $arResult["nStartPage"]++;
            while ($arResult["nStartPage"] <= $arResult["nEndPage"] - 1):
                ?>
                <?php if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                <a href="javascript:void(0)" class="pagination__item btn btn-outline-red"
                >
                    <?= $arResult["nStartPage"] ?>
                </a>
            <?php else: ?>
                <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"
                   class="pagination__item btn btn-outline-secondary"
                >
                    <?= $arResult["nStartPage"] ?>
                </a>
            <?php endif ?>
                <?php $arResult["nStartPage"]++ ?>
            <?php endwhile ?>

            <?php if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
                <?php if ($arResult["NavPageCount"] > 1): ?>
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"
                       class="pagination__item btn btn-outline-secondary"
                    >
                        <?= $arResult["NavPageCount"] ?>
                    </a>
                    <?= "</div>" ?>
                <?php endif ?>
                <a class="pagination__direction pagination__direction_next"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"
                >
                    <span><?= GetMessage("round_nav_forward") ?></span>
                    <span class="icon-arrow"></span>
                </a>
            <?php else: ?>
                <?php if ($arResult["NavPageCount"] > 1): ?>
                    <a href="javascript:void(0);" class="pagination__item btn btn-outline-red">
                        <?= $arResult["NavPageCount"] ?>
                    </a>
                    <?= "</div>" ?>
                <?php endif ?>
                <a class="pagination__direction pagination__direction_next no-hover"
                   href="javascript:void(0);"
                >
                    <span><?= GetMessage("round_nav_forward") ?></span>
                    <span class="icon-arrow"></span>
                </a>
            <?php endif ?>
        </nav>
    </div>
    <!--RestartBufferPagination-->
</div>
