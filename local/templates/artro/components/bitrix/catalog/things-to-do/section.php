<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arFilter = array(
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "ACTIVE" => "Y",
    "GLOBAL_ACTIVE" => "Y",
);
if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
    $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
    $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog")) {
    $arCurSection = $obCache->GetVars();
} elseif ($obCache->StartDataCache()) {
    $arCurSection = [];
    if (\Bitrix\Main\Loader::includeModule("iblock")) {
        $dbRes = CIBlockSection::GetList([], $arFilter, false, ["ID", "UF_TITLE_DESCRIPTION", "UF_ARTICLES"]);

        if (defined("BX_COMP_MANAGED_CACHE")) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache("/iblock/catalog");

            if ($arCurSection = $dbRes->Fetch()) {
                $CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);
            }

            $CACHE_MANAGER->EndTagCache();
        } else {
            if (!$arCurSection = $dbRes->Fetch()) {
                $arCurSection = [];
            }
        }
    }
    $obCache->EndDataCache($arCurSection);
}
if (!isset($arCurSection)) {
    $arCurSection = [];
}

?>
<?php
$APPLICATION->IncludeComponent("fbit:header.hero", ".default", [
    "BREADCRUMBS" => "Y",
    "TYPE" => "image",
    "SIZE" => "hero_medium",
    "IMAGE" => [
        "WEBP" => "/img/what-to-do/bg-what-to-do.webp",
        "JPEG" => "/img/what-to-do/bg-what-to-do.jpg",
    ],
    "DISABLE_DIVIDER" => "Y",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => 0,
]);
?>
<?php $APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "things-to-do",
    [
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "CURRENT_SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        "SECTION_ID" => 0,
        "COUNT_ELEMENTS" => "N",
        "TOP_DEPTH" => "1",
        "SECTION_FIELDS" => [],
        "SECTION_USER_FIELDS" => ["UF_SVG"],
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
    ],
    $component
);
?>

<div class="container">
    <div class="row mb-15 mb-lg-35 align-items-center text-title">
        <div class="col-6 col-md-4 col-lg-3 pr-5 pr-md-15">
            <div class="d-none d-md-block fw-700 fz-16">Фильтр</div>
            <div class="d-block d-md-none">
                <a class="d-block btn btn-outline-secondary" data-toggle="collapse" href="#collapse-sidebar-filter"
                   role="button" aria-expanded="false" aria-controls="collapse-sidebar-filter"
                >
                    Фильтр <span class="icon-filter align-middle fz-14 ml-10"></span>
                </a>
            </div>
        </div>
        <div class="col-6 col-md-3 col-lg-3 d-none d-md-block">
            <div class="fw-700 fz-16"><?php $APPLICATION->ShowViewContent("total-things1") ?></div>
        </div>
        <div class="col-6 col-md-5 col-lg-6 pl-5 pl-md-15">
            <?php $sort = $APPLICATION->IncludeComponent(
                "fbit:news.list.sort",
                "",
                [
                    "SORT" => [
                        "popular" => [
                            "NAME" => "По популярности",
                            "SORT_BY" => "SORT",
                            "SORT_ORDER" => "ASC",
                            "DEFAULT" => true
                        ],
                        "alphabet" => [
                            "NAME" => "По алфавиту",
                            "SORT_BY" => "NAME",
                            "SORT_ORDER" => "ASC",
                        ],
                        "date" => [
                            "NAME" => "По дате",
                            "SORT_BY" => "CREATED_DATE",
                            "SORT_ORDER" => "DESC",
                        ],
                    ]
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <?php
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter",
            "activity-filter",
            array(
                "DISPLAY_ELEMENT_COUNT" => "N",
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => $arCurSection['ID'],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "PRICE_CODE" => $arParams["~PRICE_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "SAVE_IN_SESSION" => "N",
                "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                "XML_EXPORT" => "N",
                "SECTION_TITLE" => "NAME",
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                "SEF_MODE" => $arParams["SEF_MODE"],
                "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
                "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
            ),
            false
        );
        ?>

        <div class="col-md-8 col-lg-9">
            <div class="d-block d-md-none fz-16 fw-600 text-title mb-20"><?php $APPLICATION->ShowViewContent("total-things2") ?></div>
            <?php $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "things-to-do",
                [
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "PARENT_SECTION" => $arResult["VARIABLES"]["SECTION_ID"],
                    "PARENT_SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                    "FIELD_CODE" => [
                        "PROPERTY_VIDEO.DETAIL_PAGE_URL"
                    ],
                    "SORT_BY1" => $sort["sort"]["SORT_BY"],
                    "SORT_ORDER1" => $sort["sort"]["SORT_ORDER"],

                    "SORT_BY2" => $arParams["ELEMENT_SORT_FIELD2"],
                    "SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],

                    "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                    "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                    "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                    "FILTER_NAME" => $arParams["FILTER_NAME"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "SET_TITLE" => "N",
                    "MESSAGE_404" => $arParams["~MESSAGE_404"],
                    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                    "SHOW_404" => $arParams["SHOW_404"],
                    "NEWS_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
                    "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                    "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                    "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                    "PAGER_SHOW_ALWAYS" => "N"
                ],
                $component
            ); ?>
        </div>
    </div>
    <?php $APPLICATION->ShowViewContent("things-to-do-pagination") ?>
</div>


<div class="container">
    <div class="mt-60 pt-30 mb-60">
        <h2 class="fz-20 fz-md-32">
            <?php $APPLICATION->IncludeFile("/include/things-to-do/seo_title.php"); ?>
        </h2>
        <div class="text-gradient mb-15">
            <div class="text-gradient__backdrop"></div>
            <?php $APPLICATION->IncludeFile("/include/things-to-do/seo_text1.php"); ?>
            <div class="collapse" id="collapse-text-gradient">
                <?php $APPLICATION->IncludeFile("/include/things-to-do/seo_text2.php"); ?>
            </div>
        </div>
        <div class="text-title">
            <a href="#collapse-text-gradient" data-toggle="collapse" role="button" aria-expanded="false"
               aria-controls="collapse-text-gradient"
            >
                <span class="text-underline fz-14">Читать полностью</span>
                <span class="icon-chevron ml-10 fz-12 align-middle"></span>
            </a>
        </div>
    </div>

    <?php if (!empty($arCurSection["UF_ARTICLES"])): ?>
        <div class="pt-xl-60">
            <h2 class="fz-20 fz-md-32 text-lg-center mb-45 mb-md-60 mt-60">Подборка статей</h2>
            <?
            global $filterSectionArticles;
            $filterSectionArticles = [
                "ID" => $arCurSection["UF_ARTICLES"]
            ];
            ?>
            <?php $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "section-articles",
                [
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "SET_TITLE" => "N",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "ID",
                    "SORT_ORDER2" => "DESC",
                    "IBLOCK_ID" => \FBit\Conf::ID_IBLOCK_ARTICLES,
                    "PARENT_SECTION" => 0,
                    "NEWS_COUNT" => 6,
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "86400",
                    "CACHE_GROUPS" => "N",
                    "CACHE_FILTER" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "FILTER_NAME" => 'filterSectionArticles',
                    "FIELD_CODE" => ["PREVIEW_PICTURE", "SHOW_COUNTER", "DATE_CREATE"],
                ]
            ); ?>


            <div class="d-block d-md-none mt-10">
                <a href="/articles/" class="show-more">
                    <div class="show-more__line"></div>
                    <div class="show-more__anchor">
                        <span class="show-more__text">Показать ещё</span><span class="show-more__icon icon-chevron"
                        ></span>
                    </div>
                    <div class="show-more__line"></div>
                </a>
            </div>
        </div>
    <?php endif ?>
</div>

<div class="py-60"></div>
