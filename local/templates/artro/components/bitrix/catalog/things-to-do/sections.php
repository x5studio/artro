<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<?php
$APPLICATION->IncludeComponent("fbit:header.hero", "", [
    "BREADCRUMBS" => "Y",
    "TYPE" => "image",
    "SIZE" => "hero_medium",
    "IMAGE" => [
        "WEBP" => "/img/what-to-do/bg-what-to-do.webp",
        "JPEG" => "/img/what-to-do/bg-what-to-do.jpg",
    ],
    "DISABLE_DIVIDER" => "Y",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => 0,
]);
?>

<?php $APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "things-to-do",
    array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "SECTION_ID" => 0,
        "COUNT_ELEMENTS" => "N",
        "TOP_DEPTH" => "1",
        "SECTION_FIELDS" => [],
        "SECTION_USER_FIELDS" => ["UF_SVG"],
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
    ),
    $component
);
$lineCount = 4;
?>

<div class="container">
    <div class="row mb-15 mb-lg-35 align-items-center text-title">
        <div class="col-6 col-md-4 col-lg-3 pr-5 pr-md-15">

        </div>
        <div class="col-6 col-md-3 col-lg-3 d-none d-md-block">
            <div class="fw-700 fz-16"><?php $APPLICATION->ShowViewContent("total-things1") ?></div>
        </div>
        <div class="col-6 col-md-5 col-lg-6 pl-5 pl-md-15">
            <?php $sort = $APPLICATION->IncludeComponent(
                "fbit:news.list.sort",
                "",
                [
                    "SORT" => [
                        "popular" => [
                            "NAME" => "По популярности",
                            "SORT_BY" => "SORT",
                            "SORT_ORDER" => "ASC",
                            "DEFAULT" => true
                        ],
                        "alphabet" => [
                            "NAME" => "По алфавиту",
                            "SORT_BY" => "NAME",
                            "SORT_ORDER" => "ASC",
                        ],
                        "date" => [
                            "NAME" => "По дате",
                            "SORT_BY" => "CREATED_DATE",
                            "SORT_ORDER" => "DESC",
                        ],
                    ]
                ]
            ); ?>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12 col-lg-12">
            <div class="d-block d-md-none fz-16 fw-600 text-title mb-20"><?php $APPLICATION->ShowViewContent("total-things2") ?></div>
            <?php $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "things-to-do",
                [
                    "LINE_COUNT" => $lineCount,
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "SECTION_ID" => 0,
                    "FIELD_CODE" => [
                        "PROPERTY_VIDEO.DETAIL_PAGE_URL"
                    ],
                    "SORT_BY1" => $sort["sort"]["SORT_BY"],
                    "SORT_ORDER1" => $sort["sort"]["SORT_ORDER"],

                    "SORT_BY2" => $arParams["ELEMENT_SORT_FIELD2"],
                    "SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],

                    "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                    "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                    "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                    "FILTER_NAME" => $arParams["FILTER_NAME"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "SET_TITLE" => $arParams["SET_TITLE"],
                    "MESSAGE_404" => $arParams["~MESSAGE_404"],
                    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                    "SHOW_404" => $arParams["SHOW_404"],

                    "NEWS_COUNT" => $arParams["PAGE_ELEMENT_COUNT"] + ($lineCount - ($arParams["PAGE_ELEMENT_COUNT"] % $lineCount)),

                    "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                    "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                    "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                    "PAGER_SHOW_ALWAYS" => "N",
                ],
                $component
            ); ?>
        </div>
    </div>
    <?php $APPLICATION->ShowViewContent("things-to-do-pagination") ?>
</div>


<div class="container">
    <div class="mt-60 pt-30 mb-60">
        <h2 class="fz-20 fz-md-32">
            <?php $APPLICATION->IncludeFile("/include/things-to-do/seo_title.php"); ?>
        </h2>
        <div class="text-gradient mb-15">
            <div class="text-gradient__backdrop"></div>
            <?php $APPLICATION->IncludeFile("/include/things-to-do/seo_text1.php"); ?>
            <div class="collapse" id="collapse-text-gradient">
                <?php $APPLICATION->IncludeFile("/include/things-to-do/seo_text2.php"); ?>
            </div>
        </div>
        <div class="text-title">
            <a href="#collapse-text-gradient" data-toggle="collapse" role="button" aria-expanded="false"
               aria-controls="collapse-text-gradient"
            >
                <span class="text-underline fz-14">Читать полностью</span>
                <span class="icon-chevron ml-10 fz-12 align-middle"></span>
            </a>
        </div>
    </div>

</div>

<div class="py-60"></div>
