<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$needLeftBorder = false;
$heroPicture = [
    "ORIGINAL" => $arResult["DETAIL_PICTURE"],
    "JPG" => CFile::ResizeImageGet(
        $arResult["DETAIL_PICTURE"],
        array(
            'width' => 1903,
            'height' => 646
        ),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    ),
    "WEBP" => NULL,
];


if (CModule::IncludeModule("delight.webpconverter")) {
    $heroPicture["WEBP"] = DelightWebpConverter::ConvertImage($heroPicture["JPG"]["src"], $heroPicture["JPG"]["src"] . ".webp");
    if ($heroPicture["WEBP"]) {
        $heroPicture["WEBP"] = str_replace(" ", "%20", $heroPicture["WEBP"]);
    }
}
?>

<?php $this->SetViewTarget("element-hero-content"); ?>
<div class="d-inline-flex align-items-center">
    <?php if ($arResult["PROPERTIES"]["RATING"]["VALUE"]): ?>
        <?php $needLeftBorder = true; ?>
        <div class="py-5 pl-lg-10 pr-15 pr-md-20 fz-14 text-white  border-white text-nowrap">
            <?= $arResult["PROPERTIES"]["RATING"]["VALUE"] ?> <?= \Bitrix\Main\Localization\Loc::getMessage("ELEMENT_RATING") ?>
        </div>
    <?php endif ?>

    <?php if ($arResult["PROPERTIES"]["CATEGORY"]["VALUE"]): ?>
        <?php $needLeftBorder = true; ?>
        <div class="py-5 pl-10 pr-20 fz-14 text-white <?= $needLeftBorder ? "border-left" : "" ?> border-left border-white text-nowrap">
            <?= $arResult["DISPLAY_PROPERTIES"]["CATEGORY"]["DISPLAY_VALUE"] ?>
        </div>
    <?php endif ?>

    <?php if ($arResult["PROPERTIES"]["WORKING_TIME"]["VALUE"]): ?>
        <div class="py-5 pl-10 fz-14 text-white <?= $needLeftBorder ? "border-left" : "" ?> border-white text-nowrap">
            <span class="icon-clock fz-18 mr-10 align-middle"></span>
            <?= $arResult["PROPERTIES"]["WORKING_TIME"]["VALUE"] ?>
        </div>
    <?php endif ?>
</div>
<?php $this->EndViewTarget(); ?>

<?php $this->SetViewTarget("element-hero-picture"); ?>
<picture class="hero__banner">
    <?php if ($heroPicture["WEBP"]): ?>
        <source srcset="<?= $heroPicture["WEBP"] ?>" type="image/webp">
    <?php endif ?>
    <source srcset="<?= $heroPicture["JPG"]["src"] ?>" type="image/jpg">
    <img src="<?= $heroPicture["JPG"]["src"] ?>" alt="">
</picture>
<?php $this->EndViewTarget(); ?>

<?php if (!empty($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"])): ?>
    <div class="slider-what-to-do-detail-wrap mb-40 mb-md-60">
        <div class="container">
            <div class="swiper slider-simple slider-what-to-do-detail mb-40 mb-md-60">
                <div class="swiper-wrapper">
                    <?php foreach ($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $pictureId): ?>
                        <?php
                        $picture = CFile::ResizeImageGet(
                            $pictureId,
                            array(
                                'width' => 1218,
                                'height' => 445
                            ),
                            BX_RESIZE_IMAGE_PROPORTIONAL,
                            true
                        );
                        ?>
                        <div class="swiper-slide swiper-lazy" data-background="<?= $picture["src"] ?>"></div>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 offset-md-1 col-lg-6 offset-lg-3">
                    <div class="d-flex justify-content-between align-items-center py-5">
                        <div class="d-none d-md-block text-left">
                            <a href="#" class="slider-simple-prev btn btn-slider btn-lg btn-outline-red fw-400">
                                <span class="icon-arrow fz-20 align-text-top rotate-180"></span>
                            </a>
                        </div>
                        <div class="flex-grow-1 text-center">
                            <div class="slider-simple-pagination"></div>
                        </div>
                        <div class="d-none d-md-block text-right">
                            <a href="#" class="slider-simple-next btn btn-slider btn-lg btn-outline-red fw-400">
                                <span class="icon-arrow fz-20 align-text-top"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>


<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <?php
            $counter = 0;
            ?>
            <?php foreach ($arResult["PROPERTIES"] as $property): ?>
                <?php if (($property["SORT"] >= 5000) && ($property["SORT"] < 10000) && ($property["VALUE"])): ?>
                    <div class="mb-45">
                        <?php if ($counter++ == 0): ?>
                            <h2 class="fz-20 fz-md-32 mb-40 mb-md-30"><?= $property["NAME"] ?></h2>
                        <?php else: ?>
                            <h3 class="fz-20 mb-20 mb-md-30"><?= $property["NAME"] ?></h3>
                        <?php endif ?>
                        <div class="typography">
                            <?php if ($property["USER_TYPE"] == "HTML"): ?>
                                <?= $property["~VALUE"]["TEXT"] ?>
                            <?php else: ?>
                                <?= $property["VALUE"] ?>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="line mb-25"></div>
                <?php endif ?>
            <?php endforeach ?>

            <?php if ($arResult["DETAIL_TEXT"]): ?>
                <div class="mb-60 pt-0 pt-md-25 typography">
                    <?= $arResult["DETAIL_TEXT"] ?>
                </div>
            <?php endif ?>
        </div>

        <div class="col-lg-3">
            <? if ($arResult["PROPERTIES"]["MAP"]["VALUE"]): ?>
                <div class="mb-15 mt-30">
                    <div class="map-sidebar">
                        <?php
                        $coordinates = explode(",", $arResult["PROPERTIES"]["MAP"]["VALUE"]);
                        $lon = $coordinates[1];
                        $lat = $coordinates[0];
                        ?>
                        <?php
                        $APPLICATION->IncludeComponent(
                            "bitrix:map.yandex.view",
                            "detail-page-map",
                            [
                                "MAP_DATA" => serialize([
                                    "yandex_lon" => $lon,
                                    "yandex_lat" => $lat,
                                    "yandex_scale" => 16,
                                    "PLACEMARKS" => [
                                        [
                                            "LON" => $lon,
                                            "LAT" => $lat,
                                            "TEXT" => $arResult["NAME"]
                                        ]
                                    ]
                                ]),
                                "MAP_WIDTH" => "100%",
                                "MAP_HEIGHT" => "100%",
                                "CONTROLS" => [
                                    "SMALLZOOM",
                                ],
                                "OPTIONS" => [
                                    "ENABLE_SCROLL_ZOOM",
                                    "ENABLE_DBLCLICK_ZOOM",
                                    "ENABLE_DRAGGING"
                                ],
                                "MAP_ID" => "map_detail_page",
                            ],
                            $component
                        ); ?>
                    </div>
                </div>
            <? endif ?>
            <div class="row">
                <div class="col-12 col-md-5 col-lg-12 mb-20">
                    <?php
                    $needLine = false;
                    ?>
                    <?php if ($arResult["PROPERTIES"]["ADDRESS"]["VALUE"]): ?>
                        <?php $needLine = true; ?>
                        <div class="d-flex align-items-center py-10">
                            <span class="icon-place-marker fz-22 mr-15"></span>
                            <span><?= $arResult["PROPERTIES"]["ADDRESS"]["VALUE"] ?></span>
                        </div>
                    <?php endif ?>

                    <?php if ($needLine): ?>
                        <div class="line"></div>
                    <?php endif ?>

                    <?php if ($arResult["PROPERTIES"]["PHONE"]["VALUE"]): ?>
                        <a href="tel:<?= $arResult["PROPERTIES"]["PHONE"]["VALUE"] ?>"
                           class="d-flex align-items-center py-10"
                        >
                            <span class="icon-phone fz-22 mr-15"></span>
                            <span><?= $arResult["PROPERTIES"]["PHONE"]["VALUE"] ?></span>
                        </a>
                    <?php endif ?>

                    <?php if ($needLine): ?>
                        <div class="line"></div>
                    <?php endif ?>

                    <?php if ($arResult["PROPERTIES"]["LINK"]["VALUE"]): ?>
                        <a href="<?= $arResult["PROPERTIES"]["LINK"]["VALUE"] ?>"
                           class="d-flex align-items-center py-10"
                        >
                            <span class="icon-globe fz-22 mr-15"></span>
                            <span>Сайт</span>
                        </a>
                    <?php endif ?>

                    <?php if ($needLine): ?>
                        <div class="line"></div>
                    <?php endif ?>

                    <?php if ($arResult["PROPERTIES"]["WORKING_TIME"]["VALUE"]): ?>
                        <div class="d-flex align-items-center py-10">
                            <span class="icon-clock fz-22 mr-15"></span>
                            <span><?= $arResult["PROPERTIES"]["WORKING_TIME"]["VALUE"] ?></span>
                        </div>
                    <?php endif ?>

                    <?php if ($needLine): ?>
                        <div class="line"></div>
                    <?php endif ?>

                    <a id="js-share"
                       href="javascript:void(0)"
                       data-title="<?= htmlspecialchars($arResult["NAME"]) ?>"
                       data-result="Спасибо, что поделились этой страницей!"
                       class="d-flex align-items-center py-10 ie-hidden"
                    >
                        <span class="icon-share fz-22 mr-15"></span>
                        <span>Поделиться</span>
                    </a>
                    <div id="js-share-result" class="text-title"></div>
                </div>
            </div>

        </div>
    </div>

    <?php if ($arResult["PROPERTIES"]["DETAIL_PICTURE_2"]["VALUE"] > 0): ?>
        <?php
        $picture = CFile::ResizeImageGet(
            $arResult["PROPERTIES"]["DETAIL_PICTURE_2"]["VALUE"],
            array(
                'width' => 1500,
                'height' => 500
            ),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );
        ?>
        <div class="mb-60 pt-10">
            <img class="img-fluid rounded lazyload w-100"
                 src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                 data-src="<?= $picture["src"] ?>"
                 alt="<?= htmlspecialchars($arResult["PROPERTIES"]["DETAIL_PICTURE_2"]["DESCRIPTION"]) ?>"
            >
        </div>
    <?php endif ?>

    <?php if ($arResult["PROPERTIES"]["DETAIL_TEXT_2"]["~VALUE"]["TEXT"]): ?>
        <div class="px-md-25 px-lg-0 typography">
            <?= $arResult["PROPERTIES"]["DETAIL_TEXT_2"]["~VALUE"]["TEXT"] ?>
        </div>
    <?php endif ?>

</div>

<div class="py-60 d-none d-md-block"></div>
<div class="py-60"></div>