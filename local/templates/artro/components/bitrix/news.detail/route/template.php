<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$needLeftBorder = false;
?>


<div class="container pt-60">
    <div class="route row pt-10">
        <div class="col-lg-4">
            <div class="route__sidebar route-sidebar mr-lg-30 pl-lg-10">

                <div class="route-sidebar__title d-none d-lg-flex align-items-center">
                    <div>
                        <span class="route-sidebar__title-icon icon-place-marker"></span>
                    </div>
                    <div>
                        <span class="text-title ff-montserrat fz-20 fw-700">Схема маршрута</span>
                    </div>
                </div>

                <div class="d-lg-none">
                    <a href="#collapse-route-nav" class="route-sidebar__btn btn btn-outline-secondary"
                       data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapse-route-nav"
                    >
                        <span class="icon-place-marker"></span>
                        <span>Схема маршрута</span>
                        <span class="icon-chevron"></span>
                    </a>
                </div>

                <div class="collapse hide d-lg-block" id="collapse-route-nav">
                    <nav class="route-sidebar__nav pt-md-30 pt-lg-0">
                        <div class="route-sidebar__scrollbar scrollbar my-15 my-md-0">
                            <ul class="ul-clean" id="route-scrollspy">
                                <?php foreach ($arResult["ROUTE"] as $key => $routeItem): ?>
                                    <li class="route-sidebar__item route-sidebar-item">
                                        <a class="route-sidebar-item__link list-group-item list-group-item-action js-scroll-to"
                                           href="javascript:void(0)"
                                           data-target="#route-item-<?= $key + 1 ?>"
                                           data-offset="144"
                                        >
                                            <div class="route-sidebar-item__dot"></div>
                                            <div class="route-sidebar-item__anchor"><?= $routeItem["NAME"] ?></div>
                                            <div class="route-sidebar-item__decor"></div>
                                        </a>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ml-lg-n30">
                <div>
                    <h2 class="ff-montserrat fw-700 fz-20 mb-20"><?= $arResult["NAME"] ?></h2>
                    <div class="routes-summary fz-14 mb-10 mb-md-30">
                        <div class="routes-summary__item">
                            <span class="icon-clock text-red fz-18 fz-xl-20 mr-15"></span>
                            <?= $arResult["PROPERTIES"]["TIME"]["VALUE"] ?>
                        </div>
                        <div class="routes-summary__item">
                            <?= $arResult["PROPERTIES"]["LENGTH"]["VALUE"] ?>
                        </div>
                        <div class="routes-summary__item">
                            <?= \FBit\Helper::numWord(count($arResult["ROUTE"]), [
                                "объект",
                                "объекта",
                                "объектов",
                            ]) ?>
                        </div>
                    </div>
                    <div>
                        <?php foreach ($arResult["ROUTE"] as $key => $routeItem): ?>
                            <article class="route-item" id="route-item-<?= $key + 1 ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="ff-montserrat fw-700 fz-16 fz-md-20 mb-15 mb-md-10 text-truncate-2">
                                            <a href="<?= $routeItem["DETAIL_PAGE_URL"] ?>"><?= $routeItem["NAME"] ?></a>
                                        </h3>
                                        <div class="routes-summary fz-14 mb-10 mb-md-30">
                                            <?php if ($routeItem["TIME"]): ?>
                                                <div class="routes-summary__item">
                                                    <span class="icon-clock text-red mr-15"></span>
                                                    <?= $routeItem["TIME"] ?>
                                                </div>
                                            <?php endif ?>

                                            <div class="routes-summary__item">
                                                <span class="icon-place-marker d-inline-block text-red mr-10"></span>
                                                <?= $routeItem["ADDRESS"] ?>
                                            </div>

                                        </div>
                                        <div class="text-truncate-4 mb-20 mb-md-0">
                                            <?= $routeItem["PREVIEW_TEXT"] ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="pl-xl-15">
                                            <a href="<?= $routeItem["DETAIL_PAGE_URL"] ?>"
                                               class="d-block aspect-ratio aspect-ratio_77"
                                            >
                                                <img class="rounded lazyload"
                                                     src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                     data-src="<?= $routeItem["PICTURE"]["src"] ?>"
                                                     alt="<?= htmlspecialchars($routeItem["NAME"]) ?>"
                                                >
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        <?php endforeach ?>
                    </div>

                    <div class="py-20 d-none d-lg-block"></div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="py-60"></div>

<div class="map-routes">
    <? $APPLICATION->IncludeComponent(
        "bitrix:map.yandex.view",
        "route",
        array(
//            "API_KEY" => "",
            "CONTROLS" => array(
                'ZOOM',
                'SMALLZOOM',
                'TYPECONTROL',
                'SCALELINE',
            ),
            "INIT_MAP_TYPE" => "MAP",
            "MAP_DATA" => serialize($arResult["MAP_DATA"]),
            "MAP_HEIGHT" => "100%",
            "MAP_ID" => "",
            "MAP_WIDTH" => "100%",
            "OPTIONS" => array("ENABLE_DBLCLICK_ZOOM", "ENABLE_DRAGGING")
        )
    ); ?>
</div>