<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var CBitrixComponent $component */

$arResult["ROUTE"] = [];
$arResult["MAP_DATA"] = [
    "yandex_lon" => 0,
    "yandex_lat" => 0,
    "yandex_scale" => 13,
    "PLACEMARKS" => []
];

if (!empty($arResult["PROPERTIES"]["ACTIVITY"]["VALUE"])) {
    $arSelect = array("ID", "NAME", "IBLOCK_ID", "PROPERTY_MAP", "PROPERTY_ADDRESS", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "PREVIEW_TEXT");
    $arFilter = array(
        "ID" => $arResult["PROPERTIES"]["ACTIVITY"]["VALUE"],
        "ACTIVE" => "Y"
    );

    $res = CIBlockElement::GetList(["ID" => $arResult["PROPERTIES"]["ACTIVITY"]["VALUE"]], $arFilter, false, false, $arSelect);
    $counter = 0;
    while ($arItem = $res->GetNext(false, false)) {
        $coordinates = explode(",", $arItem["PROPERTY_MAP_VALUE"]);
        $lon = $coordinates[1];
        $lat = $coordinates[0];

        $placeMark = [
            "LON" => $coordinates[1],
            "LAT" => $coordinates[0],
            "TEXT" => $arItem["NAME"]
        ];
        $arResult["MAP_DATA"]["PLACEMARKS"][] = $placeMark;

        $arResult["MAP_DATA"]["yandex_lon"] += $placeMark["LON"];
        $arResult["MAP_DATA"]["yandex_lat"] += $placeMark["LAT"];


        $arResult["ROUTE"][] = [
            "ID" => $arItem["ID"],
            "NAME" => $arItem["NAME"],
            "DETAIL_PAGE_URL" => $arItem["DETAIL_PAGE_URL"],
            "PREVIEW_TEXT" => $arItem["PREVIEW_TEXT"],
            "TIME" => $arResult["PROPERTIES"]["ACTIVITY_TIME"]["VALUE"][$counter],
            "MAP" => $arItem["PROPERTY_MAP_VALUE"],
            "ADDRESS" => $arItem["PROPERTY_ADDRESS_VALUE"],
            "PICTURE" => CFile::ResizeImageGet(
                $arItem["PREVIEW_PICTURE"],
                array(
                    'width' => 510,
                    'height' => 392
                ),
                BX_RESIZE_IMAGE_PROPORTIONAL,
                true
            )
        ];
        $counter++;
    }

    if (!empty($arResult["MAP_DATA"]["PLACEMARKS"])) {
        $arResult["MAP_DATA"]["yandex_lon"] = $arResult["MAP_DATA"]["yandex_lon"] / count($arResult["MAP_DATA"]["PLACEMARKS"]);
        $arResult["MAP_DATA"]["yandex_lat"] = $arResult["MAP_DATA"]["yandex_lat"] / count($arResult["MAP_DATA"]["PLACEMARKS"]);
    }
}

