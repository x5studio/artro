<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var CBitrixComponent $component */

$arSort = [
    $arParams["SORT_BY1"] => $arParams["SORT_ORDER1"],
    $arParams["SORT_BY2"] => $arParams["SORT_ORDER2"],
];
$arSelect = [
    "ID",
    "NAME",
    "DETAIL_PAGE_URL",
    "PREVIEW_PICTURE",
    "ACTIVE_FROM"
];
$arFilter = [
    "IBLOCK_ID" => $arResult["IBLOCK_ID"],
    "ACTIVE" => "Y",
    "CHECK_PERMISSIONS" => "Y",
];

$arNavParams = [
    "nPageSize" => 1,
    "nElementID" => $arResult["ID"],
];
$arItems = [];
$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);
$rsElement->SetUrlTemplates($arParams["DETAIL_URL"]);
while ($obElement = $rsElement->GetNextElement()) {
    $arItem = $obElement->GetFields();;
    $arItems[] = [
        "NAME" => $arItem["NAME"],
        "URL" => $arItem["DETAIL_PAGE_URL"],
        "DISPLAY_ACTIVE_FROM" => CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat())),
        "PICTURE" => CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            array(
                'width' => 510,
                'height' => 392
            ),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        )
    ];
}

if (count($arItems) == 3) {
    $arResult["TO_RIGHT"] = $arItems[0];
    $arResult["TO_LEFT"] = $arItems[2];
} elseif (count($arItems) == 2) {
    if ($arItems[0]["ID"] != $arResult["ID"]) {
        $arResult["TO_RIGHT"] = $arItems[0];
    } else {
        $arResult["TO_LEFT"] = $arItems[1];
    }
}