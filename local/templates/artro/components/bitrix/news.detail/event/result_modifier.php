<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var CBitrixComponent $component */
$arResult["SCHEDULE"] = [];
foreach ($arResult["PROPERTIES"] as $property) {
    if ((50000 <= $property["SORT"]) && ($property["SORT"] <= 51000) && $property["VALUE"]) {
        $arResult["SCHEDULE"][] = [
            'TITLE' => $property["DESCRIPTION"],
            'TEXT' => $property["~VALUE"]["TEXT"],
        ];
    }
}