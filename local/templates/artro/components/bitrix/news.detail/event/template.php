<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$needLeftBorder = false;

$picture = $arResult["DETAIL_PICTURE"];
if (intval($picture["ID"]) == 0) {
    $picture = reset($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"]);
    if (intval($picture) == 0) {
        $picture = $arResult["PREVIEW_PICTURE"];
    }
}

$heroPicture = [
    "ORIGINAL" => $picture,
    "JPG" => CFile::ResizeImageGet(
        $picture,
        array(
            'width' => 1903,
            'height' => 646
        ),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    ),
    "WEBP" => NULL,
];


if (CModule::IncludeModule("delight.webpconverter")) {
    $heroPicture["WEBP"] = DelightWebpConverter::ConvertImage($heroPicture["JPG"]["src"], $heroPicture["JPG"]["src"] . ".webp");
    if ($heroPicture["WEBP"]) {
        $heroPicture["WEBP"] = str_replace(" ", "%20", $heroPicture["WEBP"]);
    }
}

?>

<?php $this->SetViewTarget("element-hero-content"); ?>
<div class="text-white text-uppercase fz-14 fz-md-18 fw-700 pt-30 mb-20 mb-md-5">
    <?= $arResult["DISPLAY_PROPERTIES"]["TYPE"]["DISPLAY_VALUE"] ?>
</div>

<h1 class="text-white ff-montserrat fz-20 fz-md-45 mb-20"><?= $arResult["NAME"] ?></h1>

<div class="text-white fz-14 fz-md-16 mb-25 typography">
    <?= $arResult["PROPERTIES"]["DESCRIPTION_UNDER_TITLE"]["~VALUE"]["TEXT"] ?>
</div>

<?php if ($arResult["PROPERTIES"]["AGE_RESTRICTIONS"]["VALUE"]): ?>
    <div class="mb-30 mb-md-40">
        <div class="btn btn-lg btn-outline-white bg-transparent no-hover">
            <?= $arResult["PROPERTIES"]["AGE_RESTRICTIONS"]["VALUE"] ?>
        </div>
    </div>
<?php endif ?>

<div class="text-white">
    <a id="js-share"
       class="ie-hidden"
       href="javascript:void(0)"
       data-title="<?= $arResult["NAME"] ?>"
       data-result="Спасибо, что поделились этой страницей!"
    >
        <span class="icon-share fz-20 ml-md-25 mr-15"></span><span class="fz-16">Поделиться</span>
    </a>
    <div id="js-share-result"></div>
</div>
<?php $this->EndViewTarget(); ?>

<?php $this->SetViewTarget("element-hero-picture"); ?>
<picture class="hero__banner">
    <?php if ($heroPicture["WEBP"]): ?>
        <source srcset="<?= $heroPicture["WEBP"] ?>" type="image/webp">
    <?php endif ?>
    <source srcset="<?= $heroPicture["JPG"]["src"] ?>" type="image/jpg">
    <img src="<?= $heroPicture["JPG"]["src"] ?>" alt="">
</picture>
<?php $this->EndViewTarget(); ?>

<div class="container pt-35 pt-md-60 mb-40">
    <div class="row mt-md-20">
        <div class="col-lg-6">
            <?php if ($arResult["PROPERTIES"]["TITLE_ABOVE_DETAIL_TEXT"]["VALUE"]): ?>
                <h2 class="fz-20 fz-md-22 mb-25"><?= $arResult["PROPERTIES"]["TITLE_ABOVE_DETAIL_TEXT"]["VALUE"] ?></h2>
            <?php endif ?>
            <div class="mb-60 mb-lg-0 typography">
                <?= $arResult["DETAIL_TEXT"] ?>
            </div>
        </div>
        <div class="col-lg-6">
            <?php if ($arResult["PROPERTIES"]["MORE_PHOTO"]): ?>
                <div class="mx-n15 mx-md-60 mx-lg-0">
                    <div class="swiper slider-simple slider-events-detail mb-60">
                        <div class="swiper-wrapper">
                            <?php foreach ($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $photoId): ?>
                                <?php
                                $photo = CFile::ResizeImageGet(
                                    $photoId,
                                    array(
                                        'width' => 618,
                                        'height' => 417
                                    ),
                                    BX_RESIZE_IMAGE_PROPORTIONAL,
                                    true
                                );
                                ?>
                                <div class="swiper-slide swiper-lazy" data-background="<?= $photo["src"] ?>"></div>
                            <?php endforeach ?>
                        </div>
                    </div>

                    <div class="d-flex justify-content-between align-items-center py-5">
                        <div class="d-none d-md-block text-left">
                            <a href="javascript:void(0);"
                               class="slider-simple-prev btn btn-slider btn-lg btn-outline-red fw-400"
                            >
                                <span class="icon-arrow fz-20 align-text-top rotate-180"></span>
                            </a>
                        </div>
                        <div class="flex-grow-1 text-center">
                            <div class="slider-simple-pagination"></div>
                        </div>
                        <div class="d-none d-md-block text-right">
                            <a href="javascript:void(0);"
                               class="slider-simple-next btn btn-slider btn-lg btn-outline-red fw-400"
                            >
                                <span class="icon-arrow fz-20 align-text-top"></span>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>


<?php if (!empty($arResult["SCHEDULE"])): ?>
    <div class="container mb-50">
        <h2 class="fz-20 fz-md-32 fw-700 mb-15">Расписание</h2>
        <div class="line mb-20 mb-md-35"></div>
        <?php
        $firstItem = reset($arResult["SCHEDULE"]);
        unset($arResult["SCHEDULE"][0]);
        ?>
        <div class="mb-55">
            <div class="d-flex flex-column flex-lg-row justify-content-between align-items-lg-center mb-30">
                <div class="order-1 order-lg-0">
                    <h3 class="fz-22 fw-700 mb-0">
                        <?= $firstItem["TITLE"] ?>
                    </h3>
                </div>
                <?php if ($arResult["PROPERTIES"]["EVENT_PROGRAM_PDF"]["VALUE"]): ?>
                    <div class="order-0 order-lg-1">
                        <a href="<?= $arResult["DISPLAY_PROPERTIES"]["EVENT_PROGRAM_PDF"]["FILE_VALUE"]["SRC"] ?>"
                           class="d-block d-md-inline-block btn btn-lg btn-secondary mb-20 mb-lg-0 fz-12 fz-md-14"
                           download=""
                        >
                            Программа мероприятия в pdf
                        </a>
                    </div>
                <?php endif ?>
            </div>
            <div class="text-title typography">
                <?= $firstItem["TEXT"] ?>
            </div>
        </div>

        <?php foreach ($arResult["SCHEDULE"] as $scheduleItem): ?>
            <div class="mb-45">
                <div class="d-flex flex-column flex-lg-row justify-content-between align-items-lg-center mb-30">
                    <div class="order-1 order-lg-0">
                        <h3 class="fz-22 fw-700 mb-0">
                            <?= $scheduleItem["TITLE"] ?>
                        </h3>
                    </div>
                    <div class="order-0 order-lg-1">
                        <span class="btn btn-lg btn-link invisible">&nbsp;</span>
                    </div>
                </div>
                <div class="text-title typography">
                    <?= $scheduleItem["TEXT"] ?>
                </div>
            </div>
        <?php endforeach ?>

        <div class="line mt-10"></div>
    </div>
<?php endif ?>


<div class="container mb-55">
    <?php if ($arResult["PROPERTIES"]["MAP"]["VALUE"]): ?>
        <h2 class="fz-20 fz-md-22 fw-700 mb-45">Место проведения</h2>

        <div class="mb-50">
            <?php
            $coordinates = explode(",", $arResult["PROPERTIES"]["MAP"]["VALUE"]);
            $lon = $coordinates[1];
            $lat = $coordinates[0];
            ?>
            <?php
            $APPLICATION->IncludeComponent(
                "bitrix:map.yandex.view",
                "detail-page-map",
                [
                    "MAP_DATA" => serialize([
                        "yandex_lon" => $lon,
                        "yandex_lat" => $lat,
                        "yandex_scale" => 16,
                        "PLACEMARKS" => [
                            [
                                "LON" => $lon,
                                "LAT" => $lat,
                                "TEXT" => $arResult["PROPERTIES"]["ADDRESS"]["VALUE"]
                            ]
                        ]
                    ]),
                    "MAP_WIDTH" => "100%",
                    "MAP_HEIGHT" => "527px",
                    "CONTROLS" => [
                        "SMALLZOOM",
                    ],
                    "OPTIONS" => [
                        "ENABLE_SCROLL_ZOOM",
                        "ENABLE_DBLCLICK_ZOOM",
                        "ENABLE_DRAGGING"
                    ],
                    "MAP_ID" => "map_detail_page",
                ],
                $component
            ); ?>
        </div>
    <?php endif ?>

    <?php if ($arResult["PROPERTIES"]["TITLE_UNDER_MAP"]["VALUE"]): ?>
        <h3 class="fz-20 fz-md-22 fw-700 mb-35"><?= $arResult["PROPERTIES"]["TITLE_UNDER_MAP"]["VALUE"] ?></h3>
    <?php endif ?>

    <?php if ($arResult["PROPERTIES"]["DESCRIPTION_UNDER_MAP"]["VALUE"]): ?>
        <div class="text-title mb-40 typography">
            <?= $arResult["PROPERTIES"]["DESCRIPTION_UNDER_MAP"]["~VALUE"]["TEXT"] ?>
        </div>
        <div class="line"></div>
    <?php endif ?>
</div>
