<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$needLeftBorder = false;
?>

<?php $this->SetViewTarget("element-hero-content"); ?>
<div class="d-inline-flex align-items-center">
    <div class="py-5 pr-20 fz-14 text-white border-right border-white text-nowrap"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></div>
    <div class="py-5 pl-20 fz-14 text-white border-left border-white text-nowrap ie-hidden">
        <a id="js-share"
           class="ie-hidden"
           href="javascript:void(0)"
           data-title="<?= $arResult["NAME"] ?>"
           data-result="Спасибо, что поделились этой страницей!"
        >
            <span class="icon-share fz-16 align-middle mr-10"></span>
            Поделиться
        </a>
    </div>
</div>
<div id="js-share-result" class="text-white"></div>
<?php $this->EndViewTarget(); ?>


<div class="container container_news typography">
    <h2><?= $arResult["NAME"] ?></h2>
    <?= $arResult["DETAIL_TEXT"] ?>
</div>

<div class="container">
    <div class="line mb-30"></div>

    <div class="mb-45 mb-md-35">

        <div class="d-flex justify-content-between px-20 px-md-0 text-title fw-600">
            <?php if (is_array($arResult["TO_LEFT"])): ?>
                <a href="<?= $arResult["TO_LEFT"]["URL"] ?>" class="d-flex flex-column flex-md-row align-items-center">
                    <div class="mr-0 mr-md-30 mb-10 mb-md-0">
                        <div class="btn btn-slider btn-lg btn-outline-red">
                            <span class="icon-arrow fz-20 align-text-top rotate-180 fw-400"></span>
                        </div>
                    </div>
                    <div class="text-center text-md-left">
                        Предыдущий <br class="d-md-none"> материал
                    </div>
                </a>
            <?php endif ?>

            <?php if (is_array($arResult["TO_RIGHT"])): ?>
                <a href="<?= $arResult["TO_RIGHT"]["URL"] ?>"
                   class="d-flex flex-column-reverse flex-md-row align-items-center"
                >
                    <div class="text-center text-md-left">
                        Следующий <br class="d-md-none"> материал
                    </div>
                    <div class="mr-0 ml-md-30 mb-10 mb-md-0">
                        <div class="btn btn-slider btn-lg btn-outline-red">
                            <span class="icon-arrow fz-20 align-text-top fw-400"></span>
                        </div>
                    </div>
                </a>
            <?php endif ?>


        </div>

    </div>

    <div>
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="row">
                    <? if ($arResult["TO_LEFT"]): ?>
                        <div class="col-md-6 text-title">
                            <a class="d-block"
                               href="<?= $arResult["TO_LEFT"]["URL"] ?>"
                               aria-label="<?= htmlspecialchars($arResult["TO_LEFT"]["NAME"]) ?>"
                            >
                                <div class="aspect-ratio aspect-ratio_77 mb-20">
                                    <img class="rounded lazyload"
                                         src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                         data-src="<?= $arResult["TO_LEFT"]["PICTURE"]["src"] ?>"
                                         alt="<?= htmlspecialchars($arResult["TO_LEFT"]["NAME"]) ?>"
                                    >
                                </div>
                                <div class="ff-montserrat fz-16 fz-md-18 fz-xl-20 fw-700 mb-15 mb-lg-10">
                                    <?= $arResult["TO_LEFT"]["NAME"] ?>
                                </div>
                                <div class="text-basic">
                                    <?= $arResult["TO_LEFT"]["DISPLAY_ACTIVE_FROM"] ?>
                                </div>
                            </a>
                        </div>
                    <? endif ?>

                    <? if ($arResult["TO_RIGHT"]): ?>
                        <div class="col-md-6 text-title d-none d-md-block">
                            <a class="d-block"
                               href="<?= $arResult["TO_RIGHT"]["URL"] ?>"
                               aria-label="<?= htmlspecialchars($arResult["TO_RIGHT"]["NAME"]) ?>"
                            >
                                <div class="aspect-ratio aspect-ratio_77 mb-20">
                                    <img class="rounded lazyload"
                                         src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                         data-src="<?= $arResult["TO_RIGHT"]["PICTURE"]["src"] ?>"
                                         alt="<?= htmlspecialchars($arResult["TO_RIGHT"]["NAME"]) ?>"
                                    >
                                </div>
                                <div class="ff-montserrat fz-16 fz-md-18 fz-xl-20 fw-700 mb-15 mb-lg-10">
                                    <?= $arResult["TO_RIGHT"]["NAME"] ?>
                                </div>
                                <div class="text-basic">
                                    <?= $arResult["TO_RIGHT"]["DISPLAY_ACTIVE_FROM"] ?>
                                </div>
                            </a>
                        </div>
                    <? endif ?>
                </div>
            </div>
        </div>

    </div>


</div>

