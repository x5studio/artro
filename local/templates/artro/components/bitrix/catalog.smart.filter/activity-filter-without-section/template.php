<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<div class="col-md-4 col-lg-3 mb-15 mb-md-40">
    <div class="sidebar-filter d-md-block text-title collapse" id="collapse-sidebar-filter">
        <a class="sidebar-filter__close d-inline-block d-md-none" data-toggle="collapse"
           href="#collapse-sidebar-filter" role="button" aria-expanded="true"
           aria-controls="collapse-sidebar-filter" aria-label="Скрыть фильтр"
        >
            <span class="icon-close"></span>
        </a>
        <form name="<?= $arResult["FILTER_NAME"] . "_form" ?>" action="<?= $arResult["FORM_ACTION"] ?>"
              method="get" class="smartfilter"
        >
            <?php foreach ($arResult["HIDDEN"] as $arItem): ?>
                <input type="hidden" name="<?= $arItem["CONTROL_NAME"] ?>" id="<?= $arItem["CONTROL_ID"] ?>"
                       value="<?= $arItem["HTML_VALUE"] ?>"
                />
            <?php endforeach; ?>
            <div class="mb-15">

                <?php foreach ($arResult["ITEMS"] as $key => $arItem) :
                    if (empty($arItem["VALUES"]) || isset($arItem["PRICE"])) {
                        continue;
                    }
                    ?>
                    <div class="sidebar-filter__section filter-section">
                        <a class="filter-section__title" data-toggle="collapse"
                           href="#filter-section-<?= $arItem["ID"] ?>"
                           role="button" aria-expanded="true" aria-controls="filter-section-<?= $arItem["ID"] ?>"
                        >
                            <?= $arItem["NAME"] ?> <span class="filter-section__title-icon icon-chevron"></span>
                        </a>

                        <div class="filter-section__body collapse show" id="filter-section-<?= $arItem["ID"] ?>">
                            <div class="filter-section__inner overflow-auto scrollbar">

                                <?php
                                $arCur = current($arItem["VALUES"]);
                                switch ($arItem["DISPLAY_TYPE"]):
                                    default:
                                        ?>
                                        <?php foreach ($arItem["VALUES"] as $val => $ar): ?>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox"
                                                   class="custom-control-input"
                                                   value="<?= $ar["HTML_VALUE"] ?>"
                                                   name="<?= $ar["CONTROL_NAME"] ?>"
                                                   id="<?= $ar["CONTROL_ID"] ?>"
                                                <?= $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                                   onclick="smartFilter.click(this)"
                                            />

                                            <label data-role="label_<?= $ar["CONTROL_ID"] ?>"
                                                   class="custom-control-label pl-15 fz-14 fz-md-16 mb-10 <?= $ar["DISABLED"] ? 'disabled' : '' ?>"
                                                   for="<?= $ar["CONTROL_ID"] ?>"
                                            >
                                                <?= $ar["VALUE"] ?>
                                                <?php if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])): ?>
                                                    (
                                                    <span data-role="count_<?= $ar["CONTROL_ID"] ?>"><?= $ar["ELEMENT_COUNT"] ?></span>)
                                                <?php endif; ?>
                                            </label>
                                        </div>
                                    <?php endforeach; ?>
                                    <?php endswitch; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>

            <div class="d-flex flex-md-column">
                <div class="flex-grow-1 mb-10">
                    <button href="#" class="d-block btn btn-lg btn-outline-red fz-18 fw-600 w-100"
                            type="submit"
                            id="set_filter"
                            name="set_filter"
                            value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"

                    >Показать
                    </button>
                </div>

                <div class="flex-grow-1 text-title">
                    <button class="d-block btn btn-lg  text-underline  fz-16 w-100"
                            type="submit"
                            id="del_filter"
                            name="del_filter"
                            value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
                    >Сбросить
                    </button>
                </div>

                <div class="bx-filter-popup-result hidden"
                     id="modef"
                    <?php if (!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"'; ?>
                >
                    <?= GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">' . intval($arResult["ELEMENT_COUNT"]) . '</span>')); ?>
                    <span class="arrow"></span>
                    <br/>
                    <a href="<?= $arResult["FILTER_URL"] ?>"
                       target=""
                    ><?= GetMessage("CT_BCSF_FILTER_SHOW") ?></a>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    var smartFilter = new JCSmartFilter('<?= CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>