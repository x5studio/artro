<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
$APPLICATION->RestartBuffer();
unset($arResult["COMBO"]);
$arResult["FILTER_URL"] = str_replace("/filter/clear/", "/", $arResult["FILTER_URL"]);
$arResult["FILTER_AJAX_URL"] = str_replace("/filter/clear/", "/", $arResult["FILTER_AJAX_URL"]);
$arResult["SEF_SET_FILTER_URL"] = str_replace("/filter/clear/", "/", $arResult["SEF_SET_FILTER_URL"]);
$arResult["SEF_DEL_FILTER_URL"] = str_replace("/filter/clear/", "/", $arResult["SEF_DEL_FILTER_URL"]);

$ajaxUrl = $arResult["FILTER_AJAX_URL"];

$parsed = parse_url($ajaxUrl);
$query = $parsed['query'];
parse_str($query, $params);

$params["catalog-ajax"] = "Y";
unset($params['bxajaxid']);

$arResult["CATALOG_AJAX_URL"] = $parsed["path"] . "?" . http_build_query($params);;

echo CUtil::PHPToJSObject($arResult, true);
