<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$tagProperty = $arResult["ITEMS_BY_CODE"]["TAGS"];
$typeProperty = $arResult["ITEMS_BY_CODE"]["TYPE"];
$dateProperty = $arResult["ITEMS_BY_CODE"]["DATE"];
$isWeekendProperty = $arResult["ITEMS_BY_CODE"]["WEEKEND"];

?>
<div class="container pt-35">
    <h1 class="ff-montserrat mb-25 fz-32 fz-md-45">Галерея анонсов <?= (new DateTime())->format("Y") ?></h1>

    <form name="<?= $arResult["FILTER_NAME"] . "_form" ?>"
          action="<?= $arResult["FORM_ACTION"] ?>"
          method="get"
          class="smart-filter-form"
    >

        <?php foreach ($arResult["HIDDEN"] as $arItem): ?>
            <input type="hidden" name="<?= $arItem["CONTROL_NAME"] ?>" id="<?= $arItem["CONTROL_ID"] ?>"
                   value="<?= $arItem["HTML_VALUE"] ?>"
            />
        <?php endforeach; ?>

        <?php //region Дата ?>
        <div class="d-flex flex-wrap mb-30 mb-md-20 align-items-center">
            <div class="mt-n5 ml-5 mb-30 mb-md-0 order-md-1">
                <a class="events__calendar-btn btn btn-lg btn-outline-secondary"
                   id="js-daterangepicker-action"
                   aria-label="Открыть календарь"
                >
                    <span class="icon-calendar text-red"></span>
                </a>

                <input class="events__daterangepicker"
                       type="text"
                       id="js-daterangepicker"
                       aria-hidden="true"
                >

                <input name="<?= $dateProperty["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                       type="hidden"
                       id="js-event-min-date"
                       value="<?= $dateProperty["VALUES"]["MIN"]["HTML_VALUE"] ?>"
                       onchange="smartFilter.keyup(this)"
                >

                <input name="<?= $dateProperty["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                       type="hidden"
                       id="js-event-max-date"
                       value="<?= $dateProperty["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                       onchange="smartFilter.keyup(this)"
                >
            </div>

            <div class="w-100 d-md-none"></div>

            <div class="d-inline-block text-title text-uppercase fw-700 fz-14 fz-md-18 mr-30 mr-md-35 mb-10">
                <a class="text-red" id="js-clear-date" href="javascript:void(0)">Все</a>
            </div>

            <div class="d-inline-block text-title text-uppercase fw-700 fz-14 fz-md-18 mr-30 mr-md-35 mb-10">
                <a class="" id="js-set-date-today" href="javascript:void(0)">Сегодня</a>
            </div>

            <div class="d-inline-block text-title text-uppercase fw-700 fz-14 fz-md-18 mr-30 mr-md-35 mb-10">
                <a class="" id="js-set-date-tomorrow" href="javascript:void(0)">Завтра</a>
            </div>

            <?php foreach ($isWeekendProperty["VALUES"] as $val => $ar): ?>
                <div class="d-inline-block text-title text-uppercase fw-700 fz-14 fz-md-18 mr-30 mr-md-35 mb-10">
                    <input type="checkbox"
                           value="<?= $ar["HTML_VALUE"] ?>"
                           name="<?= $ar["CONTROL_NAME"] ?>"
                           id="<?= $ar["CONTROL_ID"] ?>"
                           class="event-checkbox hidden js-weekend-checkbox"
                        <?= $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                           onclick="smartFilter.clickToWeekends(this)"
                    />

                    <label data-role="label_<?= $ar["CONTROL_ID"] ?>"
                           class="mb-0 event-checkbox-label-text <?= $ar["CHECKED"] ? 'text-red' : '' ?>"
                           for="<?= $ar["CONTROL_ID"] ?>"
                           onclick="smartFilter.clickToWeekends(this)"

                    >Выходные</label>
                </div>
            <?php endforeach; ?>


        </div>
        <?php //endregion ?>

        <?php //region Типы?>
        <?php
        $arCur = current($typeProperty["VALUES"]);
        $checkedAll = empty(array_column($typeProperty["VALUES"], "CHECKED"));
        ?>
        <div class="mb-25 mb-md-35">
            <div class="accordion">
                <div class="accordion__title d-block d-md-none text-title text-uppercase fw-700 fz-14 fz-md-18 mb-5"
                     data-toggle="collapse"
                     data-target="#events-collapse-menu"
                     aria-expanded="false"
                     aria-controls="events-collapse-menu"
                     role="navigation"
                >
                    Мероприятия
                </div>

                <div class="collapse hide d-md-block" id="events-collapse-menu">
                    <div class="d-block d-md-inline-block text-title text-uppercase fw-700 fz-14 fz-md-18 mb-10 mr-30 pt-20 pt-md-0">
                        <input type="checkbox"
                               class="event-checkbox hidden"
                               value=""
                               name="<?= $arCur["CONTROL_NAME_ALT"] ?>"
                               id="<?= "all_" . $arCur["CONTROL_ID"] ?>"
                               onclick="smartFilter.click(this)"
                            <?= $checkedAll ? 'checked="checked"' : '' ?>

                        />
                        <label class="event-checkbox-label-text <?= $checkedAll ? 'text-red' : '' ?>"
                               for="<?= "all_" . $arCur["CONTROL_ID"] ?>"
                        >Все мероприятия</label>
                    </div>
                    <?php foreach ($typeProperty["VALUES"] as $val => $ar): ?>
                        <div class="d-block d-md-inline-block text-title text-uppercase fw-700 fz-14 fz-md-18 mb-10 mr-30">
                            <input type="checkbox"
                                   value="<?= $ar["HTML_VALUE"] ?>"
                                   name="<?= $ar["CONTROL_NAME"] ?>"
                                   id="<?= $ar["CONTROL_ID"] ?>"
                                   class="event-checkbox hidden"
                                <?= $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                <?= $ar["DISABLED"] ? 'disabled' : '' ?>
                                   onclick="smartFilter.click(this)"
                            />
                            <label data-role="label_<?= $ar["CONTROL_ID"] ?>"
                                   class="event-checkbox-label-text <?= $ar["CHECKED"] ? 'text-red' : '' ?>"
                                   for="<?= $ar["CONTROL_ID"] ?>"
                            >
                                <?= $ar["VALUE"]; ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="accordion__line d-md-none"></div>
            </div>
        </div>
        <?php //endregion?>

        <?php //region Теги?>
        <?php
        $arCur = current($tagProperty["VALUES"]);
        $checkedAll = empty(array_column($tagProperty["VALUES"], "CHECKED"));
        ?>
        <div class="mb-35">
            <div class="d-inline-block mr-1 mb-5">
                <input type="checkbox"
                       class="event-checkbox hidden"
                       value=""
                       name="<?= $arCur["CONTROL_NAME_ALT"] ?>"
                       id="<?= "all_" . $arCur["CONTROL_ID"] ?>"
                       onclick="smartFilter.click(this)"
                    <?= $checkedAll ? 'checked="checked"' : '' ?>

                />
                <label class="event-checkbox-label-btn btn btn-md-lg <?= $checkedAll ? 'btn-red' : 'btn-outline-secondary' ?>"
                       for="<?= "all_" . $arCur["CONTROL_ID"] ?>"
                >Все теги</label>
            </div>

            <?php foreach ($tagProperty["VALUES"] as $val => $ar): ?>
                <div class="d-inline-block mr-1 mb-5">
                    <input type="checkbox"
                           value="<?= $ar["HTML_VALUE"] ?>"
                           name="<?= $ar["CONTROL_NAME"] ?>"
                           id="<?= $ar["CONTROL_ID"] ?>"
                           class="event-checkbox hidden"
                        <?= $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                        <?= $ar["DISABLED"] ? 'disabled' : '' ?>
                           onclick="smartFilter.click(this)"
                    />
                    <label data-role="label_<?= $ar["CONTROL_ID"] ?>"
                           class="event-checkbox-label-btn btn btn-md-lg <?= $ar["CHECKED"] ? 'btn-red' : 'btn-outline-secondary' ?>"
                           for="<?= $ar["CONTROL_ID"] ?>"
                    >
                        <?= $ar["VALUE"]; ?>
                    </label>
                </div>
            <?php endforeach; ?>
        </div>
        <?php //endregion?>

        <div class="smart-filter-parameters-box-container hidden">
            <input class="btn btn-primary"
                   type="submit"
                   id="set_filter"
                   name="set_filter"
                   value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
            />

            <input class="btn btn-link"
                   type="submit"
                   id="del_filter"
                   name="del_filter"
                   value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
            />

            <div id="modef" class="hidden">
                <?= GetMessage("CT_BCSF_FILTER_COUNT", ["#ELEMENT_COUNT#" => '<span id="modef_num">' . intval($arResult["ELEMENT_COUNT"]) . '</span>']); ?>
                <span class="arrow"></span>

                <a href="<?= $arResult["FILTER_URL"] ?>" target=""><?= GetMessage("CT_BCSF_FILTER_SHOW") ?></a>
            </div>
        </div>
    </form>

</div>

<script type="text/javascript">
    var smartFilter = new JCSmartFilter('<?= CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>