<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult["ITEMS_BY_CODE"] = [];

foreach ($arResult["ITEMS"] as $arItem) {
    if ($arItem["CODE"]) {
        $arResult["ITEMS_BY_CODE"][$arItem["CODE"]] = $arItem;
    }
}