<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $eventsPrefilter;
$eventsPrefilter = [
    "<=PROPERTY_DATE" => new \Bitrix\Main\Type\DateTime()
];
?>
<?php
$APPLICATION->IncludeComponent("fbit:header.hero", ".default", [
    "BREADCRUMBS" => "Y",
    "TYPE" => "image",
    "SIZE" => "hero_small",
    "IMAGE" => [
        "WEBP" => "/img/events/bg-events.jpg",
        "JPEG" => "/img/events/bg-events.webp",
    ],
    "DISABLE_DIVIDER" => "N",
    "ADD_TITLE_CLASS" => "d-none",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => 0,
]);
?>

<div class="events  mb-60">
    <?php
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.smart.filter",
        "events",
        array(
            "PREFILTER_NAME" => 'eventsPrefilter',
            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "SECTION_ID" => 0,
            "FILTER_NAME" => $arParams["FILTER_NAME"],
            "HIDE_NOT_AVAILABLE" => "N",
            "SHOW_ALL_WO_SECTION" => "Y",
            "INCLUDE_SUBSECTIONS" => "Y",
            "CACHE_TYPE" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "Y",
            "SAVE_IN_SESSION" => "N",
            "PRICE_CODE" => $arParams["FILTER_PRICE_CODE"],
            "XML_EXPORT" => "N",
            "SECTION_TITLE" => "-",
            "SECTION_DESCRIPTION" => "-",
            "CONVERT_CURRENCY" => "N",
            "CURRENCY_ID" => "RUB",

            "DISPLAY_ELEMENT_COUNT" => "N",
            "AJAX_MODE" => "N",
            "INSTANT_RELOAD" => "N",
            "SEF_MODE" => $arParams["SEF_MODE"],
            "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
            "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"]
        ),
        false
    );
    ?>

    <?php $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "events",
        [
            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "PARENT_SECTION" => $arResult["VARIABLES"]["SECTION_ID"],
            "PARENT_SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
            "FIELD_CODE" => [
                "PROPERTY_VIDEO.DETAIL_PAGE_URL"
            ],
            "SORT_BY1" => "PROPERTY_DATE",
            "SORT_ORDER1" => "ASC",

            "SORT_BY2" => $arParams["ELEMENT_SORT_FIELD2"],
            "SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "Y",
            "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
            "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
            "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
            "FILTER_NAME" => $arParams["FILTER_NAME"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "SET_TITLE" => "N",
            "MESSAGE_404" => $arParams["~MESSAGE_404"],
            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
            "SHOW_404" => $arParams["SHOW_404"],
            "NEWS_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
            "PAGER_SHOW_ALWAYS" => "N"
        ],
        $component
    ); ?>

</div>

<div class="container">
    <?php $APPLICATION->ShowViewContent("pagination") ?>
</div>


<div class="py-60 mb-60"></div>