<a href="#" class="footer__social-link d-inline-block mr-15">
    <span class="icon-social-instagram"></span>
</a>
<a href="#" class="footer__social-link d-inline-block mr-15">
    <span class="icon-social-vkontakte"></span>
</a>
<a href="#" class="footer__social-link d-inline-block mr-15">
    <span class="icon-social-facebook"></span>
</a>
<a href="#" class="footer__social-link d-inline-block">
    <span class="icon-social-odnoklassniki"></span>
</a>