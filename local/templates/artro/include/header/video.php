<video class="hero__banner" preload="auto" autoplay loop muted poster="/img/__content/video/bg-rnd-poster.jpg">
    <source type="video/webm" src="/img/__content/video/bg-rnd-video.webm">
    <source type="video/mp4" src="/img/__content/video/bg-rnd-video.mp4">
</video>