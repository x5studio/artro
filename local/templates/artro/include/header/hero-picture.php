<picture class="hero__banner">
    <source srcset="/img/__content/bg-hero.webp" type="image/webp">
    <source srcset="/img/__content/bg-hero.jpg" type="image/jpg">
    <img src="/img/__content/bg-hero.jpg" alt="">
</picture>