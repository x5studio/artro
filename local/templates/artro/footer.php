<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>

</main>


<div class="scroll-top ie-hidden">
    <a href="javascript:void(0)" class="scroll-top__inner text-reset js-scroll-to" data-target="#scroll-top-target"
       data-offset="0" aria-label="Прокрутить страницу вверх"
    >
        <div class="scroll-top__icon"></div>
        <div class="scroll-top__text">Вверх</div>
    </a>
</div>

<footer class="footer">
    <div class="divider bg-yellow"></div>
    <div class="footer__inner">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-4 pr-md-30">
                    <div class="mb-10 mb-md-25 pt-lg-5">
                        <a href="/">
                            <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/footer/logo.php"); ?>
                        </a>
                    </div>
                    <div class="fz-12 fz-md-14 mb-15">
                        <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/footer/text1.php"); ?>
                    </div>
                    <div class="fz-14 fz-md-16 mb-15 fw-600">
                        <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/footer/phone.php"); ?>
                    </div>
                    <div class="fz-12 fz-md-14 mb-10">
                        <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/footer/address.php"); ?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="accrdion">
                        <h4 class="footer__title d-none d-md-block fz-20 fw-700 mb-5 mb-lg-25 ff-montserrat">
                            Меню
                        </h4>
                        <div class="footer__title accordion__title d-block d-md-none fz-16 fw-700 mb-0 py-10 ff-montserrat"
                             data-toggle="collapse" data-target="#footer-collapse-menu" aria-expanded="false"
                             aria-controls="footer-collapse-menu" role="navigation"
                        >
                            Меню
                        </div>
                        <div class="collapse hide d-md-block" id="footer-collapse-menu">
                            <?php $APPLICATION->IncludeComponent("bitrix:menu", "footer-collapse-menu", [
                                "ROOT_MENU_TYPE" => "footer",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_TIME" => "36000000",
                                "MENU_CACHE_USE_GROUPS" => "N",
                                "MENU_THEME" => "site",
                                "CACHE_SELECTED_ITEMS" => "N",
                                "MENU_CACHE_GET_VARS" => [],
                                "MAX_LEVEL" => "1",
                                "CHILD_MENU_TYPE" => "",
                                "USE_EXT" => "N",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "Y",
                            ],
                                false
                            ); ?>
                        </div>
                        <div class="accordion__line d-md-none"></div>
                    </div>

                </div>
                <div class="col-md-6 col-lg-4">

                    <div class="footer__social mt-md-n35 mt-lg-0 mb-25 mb-md-0">
                        <div class="accordion">
                            <h4 class="footer__title d-none d-md-block fz-20 fw-700 mb-5 mb-lg-25 ff-montserrat">
                                Мы в соц-сетях
                            </h4>
                            <div class="footer__title accordion__title d-block d-md-none fz-16 fw-700 mb-0 py-10 ff-montserrat"
                                 data-toggle="collapse" data-target="#footer-collapse-social" aria-expanded="false"
                                 aria-controls="footer-collapse-social" role="navigation"
                            >
                                Мы в соц-сетях
                            </div>
                            <div class="collapse hide d-md-block" id="footer-collapse-social">
                                <div class="pt-15 pb-25 pt-mb-0 pb-mb-0">
                                    <div class="mb-10 mb-md-20 mb-lg-30">
                                        <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/footer/social.php"); ?>
                                    </div>
                                    <div class="fz-14 mb-10">
                                        Cайт агентства
                                    </div>
                                    <div class="mb-10">
                                        <span class="icon-arrow d-inline-block align-middle mr-10"></span>
                                        <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/footer/link1.php"); ?>
                                    </div>
                                    <div class="fz-14 mb-10">
                                        MICE портал
                                    </div>
                                    <div class="">
                                        <span class="icon-arrow d-inline-block align-middle mr-10"></span>
                                        <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/footer/link2.php"); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion__line d-md-none"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-12">
                    <div class="pt-md-25 pt-lg-45">
                        <div class="fz-12 mb-20">
                            <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/footer/oferta.php"); ?>
                        </div>
                        <div class="fz-12">
                            <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/footer/copy.php"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom">
    </div>
</footer>
</body>
</html>