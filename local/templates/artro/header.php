<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

global $APPLICATION, $USER;
CJSCore::Init(['core', 'ajax']);

$assets = \Bitrix\Main\Page\Asset::getInstance();

$assets->addJs('/assets/lib/lazysizes/lazysizes.min.js');
$assets->addJs('/assets/lib/jquery/jquery-3.6.0.min.js');
$assets->addJs('/assets/lib/jquery/jquery.cookie.js');
$assets->addJs('/assets/lib/bootstrap-4.6.1/dist/js/bootstrap.min.js');

$assets->addJs('/assets/lib/swiper/swiper-bundle.min.js');
$assets->addJs('/assets/lib/d3/dist/d3.min.js');
$assets->addJs("/assets/lib/jcf/js/jcf.js");
$assets->addJs("/assets/lib/jcf/js/jcf.select.js");
$assets->addJs("/assets/lib/daterangepicker/moment.min.js");
$assets->addJs("/assets/lib/daterangepicker/daterangepicker.js");
$assets->addJs('//translate.google.com/translate_a/element.js?cb=TranslateInit"');
$assets->addJs('/assets/js/google-translate.js');
$assets->addJs('/assets/js/script.js');

$assets->addCss('/assets/lib/bootstrap-4.6.1/dist/css/bootstrap.min.css');
$assets->addCss('/assets/lib/icomoon-v1.0/style.css');
$assets->addCss('/assets/lib/swiper/swiper-bundle.min.css');
$assets->addCss('/assets/lib/daterangepicker/daterangepicker.css');
$assets->addCss('/assets/css/style.css');

$assets->addString('<meta charset="UTF-8">');
$assets->addString('<meta name="viewport" content="width=device-width, initial-scale=1" />');
$assets->addString('<link rel="icon" type="image/x-icon" href="/favicon.ico">');
$assets->addString('<link rel="icon" type="image/png" href="/img/favicon/favicon.png">');
$assets->addString('<link rel="apple-touch-icon" href="/img/favicon/apple-touch-icon.png">');
$assets->addString('<link rel="preload" as="font" href="/assets/fonts/open-sans/open-sans-v27-latin_cyrillic-regular.woff2" type="font/woff2" crossorigin>');
$assets->addString('<link rel="preload" as="font" href="/assets/fonts/open-sans/open-sans-v27-latin_cyrillic-600.woff2" type="font/woff2" crossorigin>');
$assets->addString('<link rel="preload" as="font" href="/assets/fonts/open-sans/open-sans-v27-latin_cyrillic-700.woff2" type="font/woff2" crossorigin>');
$assets->addString('<link rel="preload" as="font" href="/assets/fonts/montserrat/montserrat-v18-latin_cyrillic-500.woff2" type="font/woff2" crossorigin>');
$assets->addString('<link rel="preload" as="font" href="/assets/fonts/montserrat/montserrat-v18-latin_cyrillic-600.woff2" type="font/woff2" crossorigin>');
$assets->addString('<link rel="preload" as="font" href="/assets/fonts/montserrat/montserrat-v18-latin_cyrillic-700.woff2" type="font/woff2" crossorigin>');
$assets->addString('<link rel="preload" as="font" href="/assets/lib/icomoon-v1.0/fonts/icomoon.woff" type="font/woff" crossorigin>');

$heroType = 'video';
if (defined("HERO_TYPE_PICTURE")) {
    $heroType = 'picture';
}

?><!DOCTYPE html>
<html>
<head>
    <?php $APPLICATION->ShowHead(); ?>
    <title><?php $APPLICATION->ShowTitle(); ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
</head>
<body class="body">
<header class="header fixed-top w-100" id="header-collapse-parent">
    <div class="header__blur"></div>
    <div id="panel">
        <?php $APPLICATION->ShowPanel(); ?>
    </div>
    <div class="container">
        <div class="header__top">
            <div class="header__logo">
                <a href="/" aria-label="Вольный Дон">
                    <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/header/logo.php"); ?>
                </a>
            </div>
            <div class="header__date">
                <?= FormatDate("d F Y / H:i") ?>
            </div>
            <div class="header__search header-search collapse" id="collapse-header-search"
                 data-parent="#header-collapse-parent"
            >
                <div class="header-search__inner">
                    <form action="/search/">
                        <label for="site-search" class="d-none">Поиск по сайту</label>
                        <input id="site-search"
                               type="text"
                               name="q"
                               class="header-search__input form-control form-control-lg"
                               autocomplete="off"
                        >
                        <input type="submit" value="" class="header-search__button btn btn-link">
                    </form>

                    <div class="header-search__results header-search-results">
                        <div class="header-search-results__body">
                        </div>
                    </div>
                </div>
            </div>
            <div class="header__search-trigger header-search-trigger">
                <div class="header-search-trigger__inner">
                    <a href="javascript:void(0)"
                       class="header-search-trigger__link" aria-label="Перейти к поиску"
                       data-toggle="collapse" data-target="#collapse-header-search" aria-expanded="false"
                       aria-controls="collapse-header-search" role="button"
                    >
                        <span class="header-search-trigger__icon icon-search"></span>
                    </a>
                </div>
            </div>
            <div class="header__lang header-lang">
                <div class="header-lang__inner" data-toggle="collapse" data-target="#collapse-lang"
                     aria-expanded="false" aria-controls="collapse-lang" role="navigation"
                >
                    <div class="header-lang__items">
                        <a href="javascript:void(0)"
                           data-google-lang="ru"
                           class="header-lang__item js-active-lang header-lang__item-active"
                        ></a>

                        <div class="header-lang__hidden collapse"
                             id="collapse-lang"
                             data-parent="#header-collapse-parent"
                        >
                            <a href="javascript:void(0);" data-google-lang="ru" class="header-lang__item">Ru</a>
                            <a href="javascript:void(0);" data-google-lang="fr" class="header-lang__item">Fr</a>
                            <a href="javascript:void(0);" data-google-lang="en" class="header-lang__item">En</a>
                            <a href="javascript:void(0);" data-google-lang="tr" class="header-lang__item">Tr</a>
                            <a href="javascript:void(0);" data-google-lang="de" class="header-lang__item">Gr</a>
                        </div>
                    </div>
                    <div class="header-lang__toggle">
                        <span class="header-lang__toggle-icon icon-chevron rotate-90 rotate-transition"></span>
                    </div>
                </div>
            </div>
            <div class="header__hight-contrast">
                <a href="#" aria-label="Версия для слабовидящих" title="Версия для слабовидящих">
                    <span class="icon-eye"></span>
                </a>
            </div>
            <div class="header__burger-top">
                <a href="javascript:void(0)" class="menu-icon" aria-label="Открыть меню" data-toggle="collapse"
                   data-target="#collapse-duplicate-menu" aria-expanded="false"
                   aria-controls="collapse-duplicate-menu"
                   role="button"
                >
                    <span class="menu-icon__line"></span>
                    <span class="menu-icon__line"></span>
                    <span class="menu-icon__line"></span>
                </a>
            </div>
            <div class="header__delimiter-one"></div>
            <div class="header__delimiter-two"></div>
        </div>
        <div class="header__bottom">
            <div class="header__burger-bottom">
                <a href="javascript:void(0)" class="menu-icon" data-toggle="collapse"
                   data-target="#collapse-secondary-menu" aria-expanded="false"
                   aria-controls="collapse-secondary-menu"
                   role="button"
                >
                    <span class="menu-icon__line"></span>
                    <span class="menu-icon__line"></span>
                    <span class="menu-icon__line"></span>
                </a>
            </div>
            <div class="header__menu-wrap">
                <?php $APPLICATION->IncludeComponent("bitrix:menu", "header", [
                    "ROOT_MENU_TYPE" => "header",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_THEME" => "site",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => [],
                    "MAX_LEVEL" => "4",
                    "CHILD_MENU_TYPE" => "headersub",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                ],
                    false
                ); ?>
                <?php $APPLICATION->IncludeComponent("bitrix:menu", "header-secondary", [
                    "ROOT_MENU_TYPE" => "headersecondary",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_THEME" => "site",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => [],
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                ],
                    false
                ); ?>
            </div>
        </div>
    </div>
    <div class="header__duplicate-menu duplicate-menu collapse"
         id="collapse-duplicate-menu"
         data-parent="#header-collapse-parent"
    >
        <div class="container">
            <div class="duplicate-menu__inner scrollbar">
                <div class="row mx-0">
                    <div class="col-md-6 mb-25 mb-md-0">
                        <?php $APPLICATION->IncludeComponent("bitrix:menu", "duplicate-header", [
                            "ROOT_MENU_TYPE" => "header",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "36000000",
                            "MENU_CACHE_USE_GROUPS" => "N",
                            "MENU_THEME" => "site",
                            "CACHE_SELECTED_ITEMS" => "N",
                            "MENU_CACHE_GET_VARS" => [],
                            "MAX_LEVEL" => "4",
                            "CHILD_MENU_TYPE" => "headersub",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                        ],
                            false
                        ); ?>

                    </div>
                    <div class="col-md-6">
                        <?php $APPLICATION->IncludeComponent("bitrix:menu", "duplicate-header-secondary", [
                            "ROOT_MENU_TYPE" => "headersecondary",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "36000000",
                            "MENU_CACHE_USE_GROUPS" => "N",
                            "MENU_THEME" => "site",
                            "CACHE_SELECTED_ITEMS" => "N",
                            "MENU_CACHE_GET_VARS" => [],
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                        ],
                            false
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="scroll-top-target"></div>

<main class="main">
<? /*
    <div class="hero <?php $APPLICATION->ShowProperty('HERO_SIZE', ""); ?>">
        <div class="hero__backdrop"></div>
        <?php if ($heroType == "video"): ?>
            <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/header/video.php"); ?>
        <?php else: ?>
            <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/header/hero-picture.php"); ?>
        <?php endif ?>

        <div class="hero__wrap">
            <div class="hero__breadcrumb">
                <?php if (!defined("DISABLE_BREADCRUMBS")): ?>
                    <div class="container">
                        <?php $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", [
                            "START_FROM" => "0",
                            "PATH" => "",
                            "SITE_ID" => "-"
                        ]); ?>
                    </div>
                <?php endif ?>
            </div>

            <div class="hero__body">
                <div class="container">
                    <h1 class="text-white ff-montserrat">
                        <?php $APPLICATION->ShowTitle(false, false); ?>
                    </h1>
                    <div class="text-white fz-14">
                        <?php $APPLICATION->ShowProperty("DESCRIPTION"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="divider bg-yellow"></div>
    </div>
    <?php
    if (!defined("DISABLE_CONTAINER")) {
        echo '<div class="container">';
    }
    ?>
*/ ?>