<?php

namespace FBit;

class Conf
{
    /**
     * ID инфоблока "Чем заняться"
     */
    public const  ID_IBLOCK_THINGS_TO_DO = 1;

    /**
     * ID инфоблока "Где остановиться"
     */
    public const ID_IBLOCK_WHERE_TO_STAY = 2;

    /**
     * ID инфоблока "Где поесть"
     */
    public const ID_IBLOCK_WHERE_TO_EAT = 3;

    /**
     * ID инфоблока "Маршруты"
     */
    public const ID_IBLOCK_ROUTES = 4;

    /**
     * ID инфоблока "Новости"
     */
    public const ID_IBLOCK_NEWS = 5;

    /**
     * ID инфоблока "Чем заняться на главной"
     */
    public const ID_IBLOCK_MAIN_THINGS_TO_DO = 6;

    /**
     * ID инфоблока "Дайджест Вольного Дона"
     */
    public const ID_IBLOCK_MAIN_DIGEST = 7;

    /**
     * ID инфоблока "О регионе"
     */
    public const ID_IBLOCK_MAIN_ABOUT = 8;

    /**
     * ID инфоблока "Вас может заинтересовать"
     */
    public const ID_IBLOCK_MAY_BE_INTERESTED = 9;

    /**
     * ID инфоблока "Статьи"
     */
    public const ID_IBLOCK_ARTICLES = 10;

    /**
     * ID инфоблока "События"
     */
    public const ID_IBLOCK_EVENTS = 11;

    /**
     * ID инфоблока "Путеводитель"
     */
    public const ID_IBLOCK_GUIDE = 12;
}