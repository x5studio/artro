<?php

namespace FBit;


class Seasons
{
    const SEASON_WINTER = 'winter';

    const SEASON_SPRING = 'spring';

    const SEASON_SUMMER = 'summer';

    const SEASON_AUTUMN = 'autumn';

    /**
     * Seasons.
     *
     * @var array
     */
    public $seasons = array(
        self::SEASON_WINTER,
        self::SEASON_SPRING,
        self::SEASON_SUMMER,
        self::SEASON_AUTUMN,
    );

    /**
     * Month/Season map.
     *
     * @var array
     */
    public $monthRange = array(
        0 => array(12, 1, 2),
        1 => array(3, 4, 5),
        2 => array(6, 7, 8),
        3 => array(9, 10, 11),
    );

    /**
     * Parse input date and return numeric month.
     *
     * @param string
     *
     * @return int
     */
    public function getMonth(?\DateTime $date): int
    {
        if (is_null($date)) {
            return intval((new \DateTime())->format("m"));
        } else {
            return intval($date->format("m"));
        }
    }

    /**
     * Parse date, return season.
     *
     * @param string
     * @return string
     */
    public function get($date = null)
    {
        return $this->seasons[(($this->getMonth($date) % 12) / 3)];
    }

    /**
     * Get months numbers that belong to the season.
     *
     * @param string $season
     * @return array
     */
    public function monthRange($season)
    {
        if (!in_array(ucfirst($season), $this->seasons)) {
            throw new \Exception($season . ' is not a season.');
        }

        return $this->monthRange[array_search(ucfirst($season), $this->seasons)];
    }
}