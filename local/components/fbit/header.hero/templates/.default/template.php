<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */
?>

<div class="hero <?= $arParams["SIZE"] ?>">
    <div class="hero__backdrop"></div>

    <?php if ($arParams["TYPE"] == "video"): ?>
        <video class="hero__banner" preload="auto" autoplay loop muted poster="<?= $arParams["VIDEO"]["POSTER"] ?>">
            <source type="video/webm" src="<?= $arParams["VIDEO"]["WEBM"] ?>">
            <source type="video/mp4" src="<?= $arParams["VIDEO"]["MP4"] ?>">
        </video>
    <?php else: ?>
        <picture class="hero__banner">
            <source srcset="<?= $arParams["IMAGE"]["WEBP"] ?>" type="image/webp">
            <source srcset="<?= $arParams["IMAGE"]["JPEG"] ?>" type="image/jpg">
            <img src="<?= $arParams["IMAGE"]["WEBP"] ?>" alt="">
        </picture>
    <?php endif ?>

    <div class="hero__wrap">
        <div class="hero__breadcrumb">
            <?php if ($arParams["BREADCRUMBS"] == "Y"): ?>
                <div class="container">
                    <?php $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", [
                        "START_FROM" => "0",
                        "PATH" => "",
                        "SITE_ID" => "-"
                    ]); ?>
                </div>
            <?php endif ?>
        </div>

        <div class="hero__body">
            <div class="container">
                <h1 class="text-white ff-montserrat <?= $arParams["ADD_TITLE_CLASS"] ?>">
                    <?php $APPLICATION->ShowTitle(false, false); ?>
                </h1>
                <div class="text-white fz-14">
                    <?php $APPLICATION->ShowProperty("PAGE_DESCRIPTION"); ?>
                </div>
            </div>
        </div>
    </div>
    <?php if($arParams["DISABLE_DIVIDER"] != "Y"): ?>
        <div class="divider bg-yellow"></div>
    <?php endif ?>
</div>

