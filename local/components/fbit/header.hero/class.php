<?php

use \Bitrix\Main;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Error;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 */
class HeaderHeroComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($params)
    {
        $params["CACHE_TYPE"] = "N";
        $params["CACHE_TIME"] = 0;

        if (!isset($params["BREADCRUMBS"])) {
            $params["BREADCRUMBS"] = "Y";
        }

        if (!isset($params["SIZE"])) {
            $params["SIZE"] = "hero_small";
        }

        if (!isset($params["TYPE"])) {
            $params["TYPE"] = "image";
        }

        if ($params["TYPE"] == "video") {
            if (!isset($params["VIDEO"])) {
                $params["VIDEO"] = [
                    "POSTER" => "/img/__content/video/bg-rnd-poster.jpg",
                    "WEBM" => "/img/__content/video/bg-rnd-video.webm",
                    "MP4" => "/img/__content/video/bg-rnd-video.mp4",
                ];
            }
        } else if ($params["TYPE"] == "image") {
            if (!isset($params["IMAGE"])) {
                $params["IMAGE"] = [
                    "JPEG" => "/img/__content/bg-hero.jpg",
                    "WEBP" => "/img/__content/bg-hero.webp",
                ];
            }
        }

        return $params;
    }

    public function executeComponent()
    {
        $this->includeComponentTemplate();
    }
}