<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="d-flex align-items-center justify-content-end">
    <span class="d-none d-md-inline-block fw-700 fz-16 mr-10 mr-lg-15">Сортировать по:</span>
    <div class="flex-grow-1 flex-md-grow-0 text-center text-md-left">
        <select class="form-control js-order-control" name="order-by" aria-label="Сортировать по">
            <?php foreach ($arParams["SORT"] as $sortCode => $sort): ?>
                <option value="sort-<?= $sortCode ?>"
                        data-href="<?= $APPLICATION->GetCurPageParam("sort={$sortCode}", ["sort"]) ?>"
                    <?php if ($sort["SELECTED"]): ?>
                        selected
                    <?php endif ?>
                ><?= $sort["NAME"] ?>
                </option>
            <? endforeach ?>
        </select>
    </div>
</div>
