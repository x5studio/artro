<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => Loc::getMessage("NEWS_SORT_ORDER_COMPONENT_NAME"),
	"DESCRIPTION" => Loc::getMessage("NEWS_SORT_ORDER_COMPONENT_DESCRIPTION"),
	"PATH" => array(
        "ID" => "content",
        "CHILD" => array(
            "ID" => "news",
            "NAME" => Loc::getMessage("NEWS_SORT_ORDER_PATH_NAME"),
            "SORT" => 10,
            "CHILD" => array(
                "ID" => "news_cmpx",
            ),
        ),
	),
);