<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class NewsListSort extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }

    public function executeComponent()
    {
        $arResult = &$this->arResult;
        $arParams = &$this->arParams;

        $arResult["sort"] = $this->getValue("SORT", "sort");

        $this->includeComponentTemplate();

        return $arResult;
    }

    public function getValue($paramCode, $requestCode)
    {
        $arResult = &$this->arResult;
        $arParams = &$this->arParams;

        $requestValue = $this->request->getQuery($requestCode);
        $defaultValue = null;
        $selected = null;
        foreach ($arParams[$paramCode] as $code => &$value) {
            if ($value["DEFAULT"] === true) {
                $defaultValue = &$value;
            }
            if ($requestValue == $code) {
                $value["SELECTED"] = true;
                $selected = $value;
                break;
            }
        }
        unset($value);

        if (is_null($selected)) {
            $defaultValue["SELECTED"] = true;
            $selected = $defaultValue;
        }

        return $selected;
    }
}
